<?php

  Class Orders{

  
    
    
       private $db_connection;
    
      /* Constructor class
       *
       */
       
      public function __construct($db_connection){
    
      $this->db_connection = $db_connection;
    
    
      }
    
    
      /* Returns the total number of orders placed.
       */
    
      public function totalOrders(){
    
    
        
        $query_stmt = $this->db_connection->prepare("SELECT id FROM orders ");
        $query_stmt->execute();
        $returned_row = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
    
        
    
        return count($returned_row);
        
    
    
    
      }
    
      /* Returns the total number of orders placed between a certain start and end time in seconds
       */
    
      public function getOrders($start_time,$end_time=''){
    
        $end_time = (empty($end_time) ? date('U') : $end_time);
        
        $query_stmt = $this->db_connection->prepare("SELECT id FROM orders WHERE timestamp >= :start_time AND  timestamp <= :end_time");
        //$query_stmt->execute(array('start_time'=>$start_time,'end_time'=>$end_time));
        $query_stmt->bindParam(':start_time',intval($start_time), PDO::PARAM_INT);
        $query_stmt->bindParam(':end_time',intval($end_time), PDO::PARAM_INT);
        $query_stmt->execute();
        $returned_row = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
    
        
    
        return count($returned_row);
        
    
    
    
      }
    
      /* Returns the total number of orders with state 'pending' or 0 
       */
    
      public function getPackableOrders(){
    
       
        $query_stmt = $this->db_connection->prepare("SELECT * FROM orders WHERE status = 0");
        
        $query_stmt->execute();
        $returned_row = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

        
        
        return $returned_row;
        
    
    
    
      }
      
      /* Returns the total number of orders with state 'awaiting dispatch' or 1 
       */
    
      public function getPackedOrders(){
    

        
        $query_stmt = $this->db_connection->prepare("SELECT * FROM orders WHERE status = 1");
        
        $query_stmt->execute();
        $returned_row = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

        
        
        return $returned_row;
        
    
    
    
      }

      
      /* Returns the total number of orders with state 'dispatched' or 2 
       */
    
      public function getSentOrders(){
    

        
        $query_stmt = $this->db_connection->prepare("SELECT * FROM orders WHERE status = 2");
        
        $query_stmt->execute();
        $returned_row = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

        
        
        return $returned_row;
        
    
    
    
      }
 
    
    
      /* Retrieves a users order history
       *
       * 
       * 
       */
    
       
    
      public function getOrderHistory($uid=''){
    
      $uid= (empty($uid) ? $_SESSION['uid'] : $uid);

        $query_stmt = $this->db_connection->prepare("SELECT * FROM orders WHERE customer_id = :uid  ORDER BY timestamp DESC");
        $query_stmt->execute(array('uid'=>$uid));
        $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $returned_data;
    
      }
    
    
    
      /* Retrieves items from a order
       *
       * 
       * 
       */
    
       
    
      public function getProducts($order_id){
    
        $query_stmt = $this->db_connection->prepare("SELECT * FROM order_items WHERE order_id = :order_id");
        $query_stmt->execute(array('order_id'=>$order_id));
        $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $returned_data;
    
      }


      /** Generates the html for a users order history.
       *
       * Used in the users order_history view and the admins view of every users records.
       * $external optionally allows the links to images to be mde to work
       *  TODO - Is this better off in the UI class?
       * 
       */

    public function orderHistoryTable($past_orders,$external=false){
////////////////////////////////////////////////

      $tdata ='';

      global $product;
      global $conf;
      foreach($past_orders as $key => $order){
      
      
        // Each order needs to be looked up in the order items table
      
        $order_date = date('d/m/Y H:i:s',$order['timestamp']);
      
        $order_items = $this->getProducts($order['id']);
      
        $no_of_order_items = count($order_items);

        

        switch ($order['status']){

          case 0:
          $order_status = 'processing';
          $order_glyph = 'fa-circle-o';
          $status_color = 'red';
          break;
          
          case 1:
          $order_status = 'awaiting dispatch';
          $order_glyph = 'fa-adjust';
          $status_color = 'green';
          break;
          
          case 2:
          $order_status = 'dispatched';
          $order_glyph = 'fa-circle';
          $status_color = 'blue';
          break;
          

        }
        $status = "Order Status: <i class='fa $order_glyph'></i> <b> $order_status </b>";
      
        $total = calculatePrice($order['total']);
      
        $tdata .= "<div  class='butt_after float_left'><h2><i class='fa fa-credit-card'></i> Order: ".
                    str_pad($order['id'],7,0,STR_PAD_LEFT)
                    ."</h2><h6 class=black><i class='fa fa-clock-o'></i> $order_date</h6>"
                    ."<h3 class=$status_color>$status</h3>";
        $tdata .= ($external==false ? "<table border='1' class='oddeven'>" : "<table border='0' width='60%'>");
        
        //$tdata .= "<tr><th colspan='3'> Order no: ". str_pad($order['id'],7,0,STR_PAD_LEFT) ." - </th><th> $order_date </th></tr>";
      
        foreach($order_items as $key => $item ){
      
          $product_name = $product->lookup($item['product_id'])['name'];
      
          $price = calculatePrice($item['price']*$item['qty']);

          $img_src = ($external== false ? './cache/product_images/' : $conf['site_uri'].'cache/product_images/');

          $img_src .= $item['product_id'] .'.jpeg';

          $img = $product->getImage($item['product_id'],$product_name);
          
          $tdata .= "
          <tr>
            <td>
            $img
            </td><td>
              
              $product_name
              
            </td>
            <td>
            ".
              $item['qty']
              ."
            </td>
            <td>
            ".
              $price
              ."
            </td>
            
      
          ";
      
          
      
        }
          
            // Add a row with the order total (from the db)
      
        $tdata .= "<tr><th colspan='3'> Order Total </th><td>$total</td></tr>";
        
      
        $tdata .= '</table>';
        $tdata .='</div>';
      }
      
      return $tdata;


////////////////////////////////////////////////
    }
  
    /** Returns the details of a single order
     *
     */


    public function getOrderDetails($order_id){

        $query_stmt = $this->db_connection->prepare("SELECT * FROM orders WHERE id = :order_id ");
        $query_stmt->execute(array('order_id'=>$order_id));
        $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
        
        return $returned_data[0];
    
    


      }
  
      
      
    /** Update order status
     */
  
  
    public function updateStatus($order_id){
  
          
      $query_stmt = $this->db_connection->prepare("UPDATE orders SET status = status + 1 WHERE id = :order_id");
      $query_stmt->execute(array('order_id'=>$order_id));

      $order_detail = $this->getOrderDetails($order_id);

      $order_status = $order_detail['status'];

      if($order_status >= 2){

        // Email customer to inform that order has been dispatched

          $this->sendOrderEmail($order_id,$type='dispatch');

      }


    }
  
  
  
    /**
     * Place an order
     *
     *
     */
  
    public function placeOrder(){
  
      // Need the $product object in scope for lookups
  
      global $product;
      global $shopping_basket;
      global $userinfo;
      global $user;
  
      // We only allow logged in users to place orders so check this first and redirect as required
  
      if(!isset($_SESSION['uid'])){
  
        // TODO - move the natural language to 'lang'
  
        display_feedback('To place an order, you will need to log in or create an account','Login required','login','notify');
        break;
      }
  
      // We only allow users who have an address on file to place orders so check this first and redirect as required
  
      if(empty($user->getAddress($_SESSION['uid']))){
  
        // TODO - move the natural language to 'lang'
  
        display_feedback("To place an order, you will need to add an address to your account, you can do this in the 'My Account' section on the ment",'Address required','account','notify');
        break;
      }
  
      /* IF there are items inthe basket then place them into an order
       *
       * If there are no items in the basket then the assumption should be that the users browser
       * has suggested a historic link from a previous visit, or that the user has manually entered or bookmarked the order
       * page. We gracefully and redirect to the products page after notifying the user as to the error to allow them to update bookmarks
       * as required.
       */
  
      if(isset($_SESSION['basket_items'])){
  
        $timestamp = date('U');
  
        $total = $shopping_basket->totalPrice()['basket_total']*100;
  
        $query_stmt = $this->db_connection->prepare("INSERT INTO orders (customer_id,timestamp,total) VALUES (:user,:timestamp,:total)");
        $query_stmt->execute(array('user'=>$_SESSION['uid'],'timestamp'=>$timestamp,'total'=>$total));
  
        /* Add the items for the order into the 'order items' db
         *
         * NOTE:
         * We need the order no that we just created, and this was autoincremented in the db
         * so we don't have it stored in the running session yet so must
         * retrieve it based on other unique info that we do know about the order:
         *
         * - the timestamp we created
         * - the customer id
         * 
         * Not all databases will report the last inserted row (MySQL being one of then)
         * so doing this will negate this shortcoming
         */
        
  
        $query_stmt = $this->db_connection->prepare("SELECT id FROM orders WHERE timestamp = :timestamp AND customer_id = :user");
        $query_stmt->execute(array('user'=>$_SESSION['uid'],'timestamp'=>$timestamp));
        $order_id = $query_stmt->fetch(PDO::FETCH_ASSOC)['id'];
  
  
        /* Now add each basket_item into the db
         * Store the price so that the record kept is accurate of this sale
         * if we were to not store the price and then draw this out of the db
         * for order history then eventually all of the
         * records would be wrong as prices were changed.
         */
  
        
        $query = "";
  
        foreach($_SESSION['basket_items'] as $product_id => $qty){
    
          
  
    
          $query_stmt = $this->db_connection->prepare("INSERT INTO order_items (product_id,order_id,qty,price)
                                                      VALUES (:product_id,:order_id,:qty,:price)");
  
          $price = $product->lookup($product_id)['price'];
          
    
                                                       ;
          $query_stmt->execute(array('order_id' =>$order_id,
                                          'qty' =>$qty,
                                        'price' =>$price,
                                    'product_id'=>$product_id));

          /* Update the product qty in the products database table
           */

           $this->adjustStock($product_id,$qty);

          /* Call the stock checker to ensure notification emails are sent
           */

           $this->stockCheck($product_id);
  
        }
  
        // Now clear the basket so that the customer can place more orders
  
  
        $shopping_basket->clear();

        // Send a confirmation email

        $this->sendOrderEmail($order_id,$type='confirm');
  
  
          
      }
      else{
  
          // No items in basket. nothing to order.
          // let's go somewhere else instead
  
          display_feedback("Your basket appears to be empty. You'll need to add products to your basket in to order them.",'Empty basket');
  
  
      }
  
        
  
    }



    /**
     * Adjust stock qty as product is ordered.
     *
     *
     *
     *
     */

     protected function adjustStock($product_id,$qty_ordered){


      $query_stmt = $this->db_connection->prepare("UPDATE products SET stock_qty = stock_qty - :qty_ordered WHERE id = :product_id");
      $query_stmt->execute(array('qty_ordered'=>$qty_ordered,'product_id'=>$product_id));


      /* Check the stock level and respond accordingly
       */

       
    
      return true;





     }


  
    /**
     * Handles the sending of notifications to the user(s) iddentified in config to receive
     * low stock notification emails.
     *
     * 
     */
  
  
    protected function stockCheck($product_id){
  
      // check stock level against threshold
  
        $query_stmt = $this->db_connection->prepare("SELECT stock_qty,reorder_qty FROM products WHERE id = :product_id");
        $query_stmt->execute(array('product_id'=>$product_id));
        $returned_row = $query_stmt->fetch(PDO::FETCH_ASSOC);
        if($returned_row['stock_qty']<=$returned_row['reorder_qty']){

            $this->sendStockNotification($product_id);


        }

  
    }
  


  
    /**
     * Send order dispatch notification email
     */

    protected function sendOrderEmail($order_id,$type='confirm'){

        global $user;
        global $conf;

          $order_info = $this->getOrderDetails($order_id);

          // orderHistoryTable expects an array of arrays we have just the one array so far so quickly wrap that into another array
          
          $order_summary = $this->orderHistoryTable(['0'=>$order_info],$external=true);

          // Get the recipient info

          $recipient = $user->info($order_info['customer_id']);

          $recipient_email = $recipient['email'];

          $recipient_name = $recipient['username'];


          if($type=='confirm'){

            $subject = "Your order ($order_id) is now confirmed ";
            $link = $conf['site_uri'] . '?p=order_history';
            $message = "
              Hi $recipient_name<br><br>
              Your order (number:$order_id) has now been confirmed.
              <hr>
              $order_summary<br><br>We'll email you again when we send your goods out. Shouldn't take us long.<br>
              But you can check your order's status at any time by visiting <a href='$link'>$link</a>
              If there is anything else in the meantime, don't hesitate to to get intouch
              <br><br>Kind Regards<br><br>
              ".$conf['core']['company_name'];
  

          }

          if($type=='dispatch'){
  
            $subject = "Your order ($order_id) has been dispatched ";
  
            $message = "
              Hi $recipient_name<br><br>
              Your order (number:$order_id) has now been dispatched.
              <hr>
              $order_summary<br><br>We hope that you enjoy your goods and look forward to you shopping with us again soon.
              <br><br>Kind Regards<br><br>
              ".$conf['core']['company_name'];

          }
          
          if(sendEmail($recipient_email,$subject,$message)){
 
              return true;

          }
          else{
  
            display_feedback("An error occured with your request, please try again");
            return false;
            
          }

    }






  
    /**
     * Send low stock notification email
     */

    protected function sendStockNotification($product_id){


        // $conf contains the manager email to which we are sending
        global $conf;
        global $product;


          $product_info = $product->lookup($product_id);
          $product_name = $product_info['name'];
          $product_stock_level = $product_info['stock_qty'];
          $product_reorder_level = $product_info['reorder_qty'];
          

          // Get the recipient
          $recipient = $conf['core']['manager_email'];

          // Set the headers
          $headers = $conf['core']['email_headers'];
  
          // prepare the email subject
          // note - dpone encode it here as this happens later - a week to find this! d'oh!
          //$subject = encodeSubject("Low Stock Notification ($product_name)");
          $subject = "Low Stock Notification ($product_name)";

                 

          $link = $conf['site_uri'] .'?p=inventory';
    
  
          // TODO Move the email body into  the db as config
          
          $message = "
          [$product_id] $product_name is now LOW in stock having dropped below the reorder quantity specified of <b>$product_reorder_level</b>.
          <hr>
          <br>Product ID: <b>[$product_id]</b> $product_name</b>  Qty In Stock: <b>$product_stock_level</b>
          <hr>
          <br>$link<br><br> Use the link above to view stock levels<br><br>Kind Regards<br><br>
          ".$conf['core']['company_name'];
      

          if(sendEmail($recipient,$subject,$message)){
 
              return true;
          }
          else{
  
            display_feedback("An error occured with your request, please try again");
            return false;
            
          }
  

      



    }





  }
