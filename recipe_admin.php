<?php
  
  // Values for form
  
  $recipe_name = '';
  $recipe_serves = '';
  $recipe_prep = '';
  $recipe_cook = '';
  $recipe_others = '';
  $recipe_method = '';
  $recipe_action = 'add_recipe';
  $show_form = false;

  /* Get all recipes 
   */

  $product_data = $product->getAllFromCategory();
  $recipes = $product->getAllRecipes();
  $no_of_recipes = count($recipes);
  $options = '';


  $ingredient_shortlist = array();

  // If we are editing a recipe we want to catch this here so that we can populate the options below with the current values

  if(isset($_POST['a'])){

    if($_POST['a']=='edit_recipe'){

      $recipe_id = $_POST['recipe_id'];
      
      $ingredients = $product->findFromRecipes($recipe_id);

      // Create an array, to use in a moment when we make the options for the recipe form
      // that is constructed similarly to the shopping basket session array
      // as in, it has the product id as the key and the qty as the corresponding value
      
      foreach($ingredients as $ingredient){
        $product_id = $ingredient['product_id'];
        $ingredient_shortlist["$product_id"] = $ingredient['qty_required'];

      }
  
      $show_form=true;
      $recipe_action = 'save_recipe';

    }
  }



  foreach($product_data as $key => $product_info){

    $id = $product_info['id'];
    $qty='';

    
    if(array_key_exists($id,$ingredient_shortlist)){

      $qty = $ingredient_shortlist["$id"];

    }

    $product_image = $product->getImage($product_info['id'],$product_info['name']);

    $options .= "<tr><td><img src='$product_image'</td><td>". $product_info['name'] ."</td> <td>
                 <input name='ingredients[". $product_info['id'] ."]' type='number' value='$qty'></td></tr> ";

  }


  /* List all recipes or edit one that has been chosen
  */
  
  
  if(isset($_POST['a'])){

    /* Save and existing recipe
     */

    if($_POST['a']=='new_form'){

      $show_form = true;
    

    }
    elseif(($_POST['a']=='add_recipe')||($_POST['a']=='save_recipe')){
  
      // Set these so that the inputted values go back into the displayed form for correction
  
      $recipe_name = sanitise($_POST['name']);
      $recipe_serves = sanitise($_POST['serves']);
      $recipe_prep_time = sanitise($_POST['prep_time']);
      $recipe_cook_time = sanitise($_POST['cook_time']);
      $recipe_method = sanitise($_POST['method']);
  
      /* Check the conditions that apply to the subitted form values
       *
       * TODO - MOVE ALL OF THESE STRINGS INTO THE LANG FILE
       */
  
        // If any of these statements are true, then an error exists in the data
  
        switch (true){
  
          // Name not set
        
          case (empty($recipe_name)):
            $errors['recipe_name_feedback'] = 'You must specify a name for the recipe';
  
          // Serves not set
  
          case (empty($_POST['serves'])):
            $errors['serves'] = 'You have to specify the number of people that this recipe serves';
  
          // Cook time not set
  
          case (empty($_POST['cook_time'])):
            $errors['cook_time'] = 'You have to set the cooking time';
  
          // Ingredients not set
  
          case (empty($_POST['ingredients'])):
            $errors['ingredients'] = 'The recipe needs to contain at least one ingredient that is sold in the shop';
  
          // Method not set
  
          case (empty($recipe_method)):
            $errors['method'] = 'You need to write the method';
  
        }
  
        // If any of these statements are false, then an error exists in the data
  
        switch(false){
  
          //// Name not letters only
  
          //case (ctype_alpha($recipe_name)):
            //$errors['name']= 'Name can only contains letters and spaces';
            
          // Serves not a number
  
          case (is_numeric($_POST['serves'])):
            $errors['serves'] = 'The number of people that the recipe serves must be a whole number, at least 1';
          
          // Prep time not set
  
          case (isset($_POST['prep_time'])):
            $errors['prep_time'] = 'Preparation time must be specified';
  
          // prep time not a number
  
          case (is_numeric($_POST['prep_time'])):
            $errors['prep_time'] = 'Preparation time must be a whole number of minutes';
  
  
          // Cook time not a number
  
          case (is_numeric($_POST['cook_time'])):
            $errors['cook_time'] = 'Cook time must be a whole number of minutes';

          // Ingredients is not an array

          case (is_array($_POST['ingredients'])):
          $errors['ingredients'] = 'The recipe needs to contain at least one ingredient that is sold in the shop';
          
        }
  
  
      // If we set not errors above then we can proceed with adding / updating the recipe
  
      if(empty($errors)){

        $ingredients = $_POST['ingredients'];

        if($_POST['a'] == 'add_recipe'){
      

          $product->addRecipe($recipe_name,$recipe_serves,$recipe_prep_time,$recipe_cook_time,$ingredients,$recipe_method);

          // update these as we display the count on the page

          $recipes = $product->getAllRecipes();

          $no_of_recipes = count($recipes);

        }

        if($_POST['a'] == 'save_recipe'){

          $recipe_id = $_POST['recipe_id'];
        
          echo "updating". $recipe_id;

          $product->updateRecipe($recipe_id,$recipe_name,$recipe_serves,$recipe_prep_time,$recipe_cook_time,$ingredients,$recipe_method);

        }
        $show_form = false;

      }
      else{
        
        $show_form=true;

      }
  


    }

    /* Edit an existin recipe
     */

    elseif($_POST['a']=='edit_recipe'){

      $recipe_id = $_POST['recipe_id'];

      $recipe_info = $product->getRecipe($recipe_id);

      $recipe_name = $recipe_info['name'];
      $recipe_serves = $recipe_info['serves'];
      $recipe_prep_time = $recipe_info['prep_time'];
      $recipe_cook_time = $recipe_info['cook_time'];
      $recipe_method = $recipe_info['method'];



    }

  }

  /* no action selected so list all recipes
   *
   */

  if($show_form==false){

    $all_recipes = $product->getAllRecipes();

    $tdata = "<table class='oddeven'>
              <tr>
                <th>
                  Recipe
                </th>
                <th>
                  Serves
                </th>
                <th>
                  Prep Time (minutes)
                </th>
                <th>
                  Cook Time (minutes)
                </th>
                <th>
                  Products in recipe
                </th>
              </tr>
              ";


    foreach ($all_recipes as $key => $recipe_data){

      $num_of_products = $product->findFromRecipes($recipe_data['id']);

      $tdata .= "
                  <tr>
                    <td>
                      ".$recipe_data['name']."
                    </td>
                    <td>
                      ".$recipe_data['serves']."
                    </td>
                    <td>
                      ".$recipe_data['prep_time']."
                    </td>
                    <td>
                      ".$recipe_data['cook_time']."
                    </td>
                    <td>
                      ".count($num_of_products)."
                    </td>
                    <td>
                    <form action='./?p=recipe_admin' method='post'>
                      <input type='hidden' name='recipe_id' value='". $recipe_data['id'] ."'/>
                      <button type='submit' name='a' value='edit_recipe'> <i class='fa fa-eye'></i> Edit</button>
                      </form>
                    </td>
                  </tr>

                ";

    }

      $tdata .= "</table>";

  }

// form feedback values determined from $errors
// TODO - MOVE ALL OF THESE STRINGS INTO THE LANG FILE


$recipe_name_feedback =  (isset($errors['name']) ? $errors ['name'] : 'The name of the recipe');
$recipe_serves_feedback =  (isset($errors['serves']) ? $errors ['serves'] : 'The number of portions the recipe yields');
$recipe_prep_time_feedback =  (isset($errors['prep_time']) ? $errors ['prep_time'] : 'The preparation time of the recipe in minutes');
$recipe_cook_time_feedback =  (isset($errors['cook_time']) ? $errors ['cook_time'] : 'The cook time of the recipe in minutes');
$recipe_method_feedback =  (isset($errors['method']) ? $errors ['method'] : "Describe the process of making the dish.
                                                                             Be sure to include any other ingredients required
                                                                             beyond the one's available on the site and be sure to
                                                                             include all the measurements and times!");
// fomr input class values for feedback display

$recipe_name_class =  (isset($errors['name']) ? 'input_error': '');
$recipe_serves_class =  (isset($errors['serves']) ? 'input_error': '');
$recipe_prep_time_class =  (isset($errors['prep_time']) ? 'input_error': '');
$recipe_cook_time_class =  (isset($errors['cook_time']) ? 'input_error': '');
$recipe_method_class =  (isset($errors['method']) ? 'input_error': '');

$button_label = ucwords(str_replace("_"," ",$recipe_action));




