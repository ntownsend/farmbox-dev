<?php

// Set the user level for this page.

$user->authorise(4);

$show_button=true;
$show_form=false;

$product_id_field = '';


  
  if(isset($_POST['a'])){



    // Handle calls to add an item to stock

      if($_POST['a']==('add_stock'||'edit_stock')){
  
        if(isset($_POST['product_name'])){
      
        
          if(empty($_POST['product_name'])){
      
            $error['product_name']['err'] = 'Value must be filled in';
      
          }else{
      
            $product_name = sanitise($_POST['product_name']);
          
          }
      
          if(isset($_POST['description'])){
        
        
          
            if(empty($_POST['description'])){
        
              $error['description']['err'] = 'Value must be filled in';
        
            }else{
        
              $description = sanitise($_POST['description']);
            
            }
        
            // The form should pass values for all of these,
            // however if not then we can set them to 0 and edit them later.
            
            $category = (is_numeric($_POST['category']) ? $_POST['category'] : 0);
            $price = (is_numeric($_POST['price']) ? $_POST['price'] : 0);
            $qty = (is_numeric($_POST['qty']) ? $_POST['qty'] : 0);
            $reorder_qty = (is_numeric($_POST['reorder_qty']) ? $_POST['reorder_qty'] : 0);
        
            if(empty($error)){
        
              // Add product to the db

              if($_POST['a']=='add_stock'){
        
                $product->add($product_name,$description,$qty,$reorder_qty,$price,$category);
                
              }

              // update the product in the db
              
              else{

                $product_id=$_POST['product_id'];
  
                $product->update($product_id,$product_name,$description,$qty,$reorder_qty,$price,$category);

              }

            page_redirect('inventory');

              
            }
            
            // TODO - handle errors here
          
          }
      
        }

      }
 

      // Add or Edit, we need the form and not the button


      if($_POST['a']=='add_new'){

        $page_title = 'Add';
        $page_action = 'add_stock';
        $button_label = 'Add item';
        $product_info = [
                          'id'=>'',
                          'name'=>'',
                          'description'=>'',
                          'stock_qty'=>'0',
                          'reorder_qty'=>'1',
                          'price'=>'0'
                          
                        ];

      }



      if($_POST['a']=='edit'){

        

        $page_title='Edit';
        $page_action = 'edit_stock';
        $button_label = 'Update item';
      
        if(isset($_POST['product_id'])){
      
          if(is_numeric($_POST['product_id'])){

            $product_info = $product->lookup($_POST['product_id']);

            $product_id_field = "<input type='hidden' name='product_id' value='". $product_info['id'] ."' /> ";
  
          }
        }

      }


    if($_POST['a']==('add_new'||'edit')){

      $show_form = true;
      $show_button = false;

        $group_options = '';
    
        foreach($product->listCategories() as $key => $value){
      
          $selected = ($product_info['id']== $value['id'] ? 'selected' : '');
          //$selected ='';
          $group_options .= "<option value='".$value['id']."' $selected>". $value['name'] ."</option>";
    
        }

   
  

    }


    
  }
  else{
    
    
    
    // these are the sort columns we will accept
    // this is important as we are building these int the prepared pdo statement in User.inc
    // as PDO doesn't play nicely when you try and bind params on the order by condition
    $order_by = array('id','name','category','stock_qty','supplier','price','reorder_qty');
    
    // include the view choices bar
    
    include"./view_choices.php";
    
    
    $allitems = $product->getAllFromCategory($selected_category,$sort_by,$sort_direction,$start_row,$row_limit);
    
    
    $tdata = "
           
            <table class='oddeven'>
              <tr>
                <th>Category <a class='yellow' href='./?p=inventory&amp;sort=category&amp;dir=". $alt_dir['category'] ."'> <i class='fa fa-sort'></i></th>
                <th>Product Code <a class='yellow' href='./?p=inventory&amp;sort=id&amp;dir=". $alt_dir['id'] ."'> <i class='fa fa-sort'></i></th>
                <th>Name <a class='yellow' href='./?p=inventory&amp;sort=name&amp;dir=". $alt_dir['name'] ."'> <i class='fa fa-sort'></i></th>
                <th>Description</th>
                <th class='center'>QTY <a class='yellow' href='./?p=inventory&amp;sort=stock_qty&amp;dir=". $alt_dir['stock_qty'] ."'> <i class='fa fa-sort'></i></th>
                <th class='center'>Reorder Thresh. <a class='yellow' href='./?p=inventory&amp;sort=reorder_qty&amp;dir=". $alt_dir['reorder_qty'] ."'> <i class='fa fa-sort'></i></th>
                <th>Supplier <a class='yellow' href='./?p=inventory&amp;sort=supplier&amp;dir=". $alt_dir['supplier'] ."'> <i class='fa fa-sort'></i></th>
                <th class='center'>Stock Value</th>
                <th class='center'>Price <a class='yellow' href='./?p=inventory&amp;sort=price&amp;dir=". $alt_dir['price'] ."'> <i class='fa fa-sort'></i></th>
                <th>Action</th>
              </tr>
          

            ";
    
    
    foreach($allitems as $key => $value){
      
      $value['category_name'] = $product->getCategoryName($value['category']);
      
      $tdata .= "
                <tr>
                <td>".
                  $value['category_name']
                ."</td>
                <td>".
                  $value['id']
                ."</td>
            
                <td>".
                  $value['name']
                ."</td>
            
                <td title='".$value['description'] ."'>".
                   substr($value['description'],0,35) ."&#8230;"
                ."</td>
            
                <td>".
                  $value['stock_qty']
                ."</td>
            
                <td>".
                  $value['reorder_qty']
                ."</td>
            
                <td>".
                  $value['supplier']
                ."</td>
            
                <td>".
                  calculatePrice($value['stock_qty']*$value['price'])
                ."</td>
                <td>".
                  calculatePrice($value['price'])
                ."</td>
                  <td>
                    <form action='./?p=inventory' method='post'>
                    <input type='hidden' name='product_id' value='".$value['id']."'/>
                      <button class='small' type='submit' name='a' value='edit' >Edit</button>
                    </form>
                  </td>
              </tr>
              ";
            
    }

    $tdata .= '</table>';
    
  }
