<?php
/* This gets called via ajax/jquery so needs to have some of the classes included.
 *
 * TODO - make an includes file to ensure nothing is ever missed.
 */

session_start();

 // Load configuration
  require('./config.php');
  

  // Load Classes Required to set up the rest of the application
  // (n.b. Database MUST be first!)

  require("./inc/Class.Database.inc.php");
  require("./inc/Class.UI.inc.php");

  // Load Language file 

  $lang_file = "./lang/".$conf['core']['language'].".php";

  if(!file_exists($lang_file)){

    error_debug("ERROR: FATAL: Cannot load lang file");
  }
  else{

    require($lang_file);

  }

  // Load Classes (n.b. Database must be first!)
  $databaseOb = db_connect();


    require("./inc/Class.User.inc.php");

  $user = new User($databaseOb);
    $userinfo = $user->info($_SESSION['uid']);
    $user->updateLastActive($_SESSION['uid']);
  
  require("./inc/Class.Chat.inc.php");
  $chat = new Chat($databaseOb);
      $timestamp = date('d/m/Y H:i:s');


$default = "
      
        <div class='message green'>
      
          <h6>
            <i class='fa fa-clock-o'></i> $timestamp 
          </h6>
          
          <i class='fa fa-comment'></i> Hi, welcome to Abe & Cloe live chat how can I help you today
      
        </div>
  
      ";

  // If we haven't been connected to chat in the last 10 seconds then send a connected message


      $now = date('U');
      $last = $chat->lastConnected($userinfo['id']);
    if(($now-$last) > 10 ){

        $chat->sendMessage('CONNECTED',0);
        echo $default;
  
    }
// On each page load we amend the database to keep track of who is connected to chat

  $chat->updateConnected($_SESSION['uid']);

  
// CHat Handler



  // If a message has been submitted then send it.

  if(isset($_POST['text'])){

    $chat->sendMessage($_POST['text'], $_POST['replying_to']);

  }


  // Retrieve the most recent message
    
  if(isset($_GET['getmessage'])){
    
      
    $prev_message = $_SESSION['chat']['last_received'];

    //$prev_message =0;
    
    $message = $chat->getLastMessage();
    
    $timestamp = date('d/m/Y H:i:s');
  
    
   

    //if(!isset($_SESSION['chat']['last_received'])){


      //echo $default;


    //}
    //else{

      // Check we have not received the message already and that the message has some content

      if(($message['id']>$_SESSION['chat']['last_received'])&&(!empty($message))){

        // If the messagae is from the logged in user then we style it appropriately

        if($message['msg_from']==$_SESSION['uid']) {

          $class = '';
          $name = '(' .$userinfo['username'] .')';
          $name = '(me)';
          $icon ='user';

        }
        else{

          // This styles a reply to the logged in user

          $class = 'green';
          $name = ($userinfo['group']==0 ? '(Abe & Cloe)' : '(me)');
          $icon ='comment';
        }

        // If the logged in user is a member of staff and the message is not from a customer
        
        if(($userinfo['group']!=0)&&($message['msg_from']!=0)){
        
          //$name = $user->lookup($message['from']);
          $name = '(' .$user->lookup($message['msg_from']) .')';
          $class = '';
          $icon = 'user';

        }

      if(($userinfo['group']!=0)&&($message['msg_from']==0)){
        
          //$name = $user->lookup($message['from']);
          $name = '(Abe & Cloe Staff)';
          $class = '';
          $icon = 'wechat';

        }

        
        echo "
      
        <div class='message $class'>
      
          <h6>
            <i class='fa fa-clock-o'></i> ". date('d/m/Y - H:i:s',$message['timestamp']) ."
          </h6>
          
          <i class='fa fa-$icon'></i> <b>". $name .'</b> : '.  $message['message'] ."
      
        </div>
  
      ";
      }
    //}

    $_SESSION['chat']['last_received'] = (isset($message['id']) ? $message['id'] : 0 );


  }
