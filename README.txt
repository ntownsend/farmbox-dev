# Feature List
#
# Key
# + implemented
# - to implement
# ~ wishlist



# Core Functionality

- User registration
- Password recovery
- login handler
- permissions handler
- Internationalisation (different languages) - en_gb da_dk

# Store functionality

- Browse products
- shopping cart
- save shopping cart
- order history
- re-order mechanism
- repeat order mechanism
- discount mechanism
- add special offers


# Recipes

- browse recipes
- order ingredients for recipe
- upload recipes
- rate recipes
- search recipes by ingredient

# Chat

- chat with receptionist onlnine

# Location service

- gmaps integration

# Admin 

- stock management
- stock audit
- notifications
- customer account admin



Sites Used as reference

CODING


STYLE / LAYOUT

noise backgrounds - http://www.noisetexturegenerator.com/
favicon - http://www.favicon.cc/?action=icon&file_id=338173

OTHER SIMILAR BUSINESSES

http://stockboxgrocers.com/food/
