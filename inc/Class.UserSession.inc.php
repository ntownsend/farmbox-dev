<?php

/* Class.UserSession.php
 *
 * Handles user authentication and authorisation.
 *
 * REFERENCE:
 *
 * Password hashing and salting - https://crackstation.net/hashing-security.htm
 * TODO: Implement the slow hash technique mentioned in the linked article above
 */
 
class UserSession{

  public $_uid;


  public function __construct($_username){


  $_uid = $user->GetUID($_username);

  // Regenerate the session id
    session_regenerate_id();

    $_SESSION['uid'] = $_uid;
    echo "Creating user session";
    session_write_close();
    
  }

  private $user_id;

  /* Check that the user exists and that the password matches that stored.
   * Error is reported on failure of either step,
   */

  function authenticateUser($username,$password){
    
      if(User::Exists($username)) {

        validatePassword($password);

      }
  

    }

  public function userLogout(){
    session_unset();
    session_destroy();
    page_redirect();
  }


  /* Creates a password hash
   */

  
  function createPassword($password){

    /* We use the default algo and cost values here
     *
     */
 
    $hashed_password = password_hash($password, PASSWORD_DEFAULT);

    return $hashed_password;
  }

  /** Validate a password
   *
   *
   * @return bool $user_authenticated
   */
  
  function validatePassword($_password,$_hash){
      
      
      // Check that the password is correct, returns a boolean
      $user_authenticated = password_verify($_password,$_hash);
      
      if ($check) {
      
      // passwords matched! show account dashboard or something
      
      } else {
      
      // passwords didn’t match, show an error

      
      
      }


  }

}
