<?php

Class Product{


  /** Handles the manipualtion of 'products' and associate data
   * 
   * Contents
   *
   *  __construct()   :   The magic constructor
   * countOf()        :   Returns the total no of products, either all or those in stock
   * 
   *
   */

  protected $db_connection;


  // TODO - Are we using these?
  protected $product_id;
  protected $product_name;
  protected $product_price;
  protected $product_image;
  protected $product_stock;



  public function __construct($db_connection){

    $this->db_connection = $db_connection;





  }





  /**
   * Add product to inventory
   * TODO - image upload
   * DONE - grab from google images
   *
   */

  public function add($name,$description,$stock_qty,$reorder_qty,$price,$category){

    $query_stmt = $this->db_connection->prepare("INSERT INTO products (name,description,stock_qty,reorder_qty,price,category)
                                                 VALUES (:name,:description,:stock_qty,:reorder_qty,:price,:category)");
    $query_stmt->execute(array(
                              'name'=>ucwords($name),
                              'description'=>$description,
                              'stock_qty'=>$stock_qty,
                              'reorder_qty'=>$reorder_qty,
                              'price'=>$price,
                              'category'=>$category
                               )
                        );
        



  }



  /**
   * Add product to inventory
   * TODO - image upload
   * DONE - grab from google images
   *
   */

  public function update($product_id,$name,$description,$stock_qty,$reorder_qty,$price,$category){

    $query_stmt = $this->db_connection->prepare("UPDATE products
                                                 SET name=:name, description=:description,stock_qty=:stock_qty,
                                                 reorder_qty=:reorder_qty,price=:price,category=:category
                                                 WHERE id = :product_id
                                                 ");
    $query_stmt->execute(array(
                              'name'=>ucwords($name),
                              'product_id'=>$product_id,
                              'description'=>$description,
                              'stock_qty'=>$stock_qty,
                              'reorder_qty'=>$reorder_qty,
                              'price'=>$price,
                              'category'=>$category
                               )
                        );
        



  }


  /**
   * Add recipe to inventory
   * TODO - image upload / grab from google images
   *
   */

  public function addRecipe($name,$serves,$prep_time,$cook_time,$ingredients,$method){


    // See the note further down about the last insert id
    
    
    $query_stmt = $this->db_connection->prepare("INSERT INTO recipes (name,serves,prep_time,cook_time,method)
                                                 VALUES (:name,:serves,:prep_time,:cook_time,:method)");


    $query_transaction = $this->db_connection->beginTransaction();

    $query_stmt->execute(array(
                              'name'=>ucwords($name),
                              'serves'=>$serves,
                              'prep_time'=>$prep_time,
                              'cook_time'=>$cook_time,
                              'method'=>$method
                               )
                        );
    // Here we need the last insert id from PDO as we don't store anything identifiable in the last db call
    // see (http://www.php.net/manual/en/pdo.lastinsertid.php)
        
    $recipe_id = $this->db_connection->lastInsertId();
    $this->db_connection->commit();


    foreach($ingredients as $id => $qty){
      if($qty>0){
        $query_stmt = $this->db_connection->prepare("INSERT INTO recipe_items (recipe_id,product_id,qty_required)
                                                 VALUES (:recipe_id,:product_id,:qty)");
        $query_stmt->execute(array(
                              'product_id'=>$id,
                              'recipe_id'=>$recipe_id,
                              'qty'=>$qty)
                          );
      }
    }
  
  }




  /**
   * Update recipe in inventory
   * TODO - image upload / grab from google images
   *
   */

  public function updateRecipe($recipe_id,$name,$serves,$prep_time,$cook_time,$ingredients,$method){


    // See the note further down about the last insert id
    
    
    $query_stmt = $this->db_connection->prepare("UPDATE recipes
                                                 SET name=:name,serves=:serves,prep_time=:prep_time,cook_time=:cook_time,method=:method
                                                 WHERE id = :recipe_id
                                                 ");


    

    $query_stmt->execute(array(
                              'recipe_id'=>$recipe_id,
                              'name'=>ucwords($name),
                              'serves'=>$serves,
                              'prep_time'=>$prep_time,
                              'cook_time'=>$cook_time,
                              'method'=>$method
                               )
                        );
    // Here we need the last insert id from PDO as we don't store anything identiiable in the last db call
    // see (http://www.php.net/manual/en/pdo.lastinsertid.php)
        
    //printR($ingredients);

    // Fist delete all the ingredients for this recipe, as this is far simpler and less error prone than
    // trying to calculate and upate values.

    $query_stmt = $this->db_connection->prepare("DELETE FROM recipe_items WHERE recipe_id = :recipe_id");
    $query_stmt->execute(array('recipe_id'=>$recipe_id));


    
    foreach($ingredients as $id => $qty){
      if($qty>0){

        // now add the ingredients again. just like new :-)
      
        
        $query_stmt = $this->db_connection->prepare("INSERT INTO recipe_items (recipe_id,product_id,qty_required)
                                                     VALUES (:recipe_id,:product_id,:qty)
                                                    ");
        $query_stmt->execute(array(
                              'product_id'=>$id,
                              'recipe_id'=>$recipe_id,
                              'qty'=>$qty)
                          );

      }
    }
  
  }





  /** Retrieves an image from the internet
   * using the google image search api
   *
   * n.b only creative commons images are returned
   * and those marked not for commercial use are not retrieved
   * 
   */
   


  public function googleImage($q){
    $q = urlencode($q);
    // include user ip as per Google Terms of Use
    // Clean it up and validate it first though

    $ip =  filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP);
    
    $jsonurl = "https://ajax.googleapis.com/ajax/services/search/images?";
    
    // Which version of the API are we requesting
    $jsonurl .= "v=1.0";

    // set image size to request
    $jsonurl .= "&imgsz=small";

    // enable safe search
    $jsonurl .= "&safe=active";

    // Add our search term
    $jsonurl .= "&q=".$q;

    // include the user ip in the request as per the guidelines
    $jsonurl .= "&userip=".$ip;

    // Only show Creative Commons Licensed images that are designated for:
    // Reuse, AND commercial use!
    // see - https://support.google.com/websearch/answer/29508
    // see - http://googleajaxsearchapi.blogspot.co.uk/2010/04/restricting-by-licenses-now-available.html
    $jsonurl .= "&as_rights=(cc_publicdomain|cc_attribute|cc_sharealike).-(cc_nonderived)";


    // retrieve from google
    $result = json_decode(file_get_contents($jsonurl), true);

    // Use the tbUrl (thumbnal url) to ensure that we aren't loading huge images
    // some large images seem to creep through into the results.
    // it may be better, ultimately, to make use of the full javascript api here
    // instead of this simple REST interface.
    
    $img = $result['responseData']['results']['0']['tbUrl'];
    return $img;
 }



  /** Looks for a prdouct image in the sites cache, if one can't be found then
   *  a google image search is carried out and the first result returned to be used.
   * the third boolean flag allows for recipe images to be retrieved using this method
   * also allwoing th egoogleimage retrieval to be used too!
   * 
   */


  public function getImage($product_id,$product_name,$for_product=true){

    // We need $conf for the image directory

    global $conf;

    $image_class ='';
    $google_branding_text = '';
    $image_dir = $conf['core']['product_image_dir'];
    $image_text = $product_name; 
    $overlay_title = '';

    if($for_product==false){
      $image_dir = $conf['core']['recipe_image_dir'];
    }

    if(file_exists($image_dir.$product_id.".jpeg")){
      
      $img_url = $image_dir.$product_id.'.jpeg';
      $image_text .= ' ['.$conf['core']['company_name'] .']';
    }
    else{

      // No image found so either return a hash symbol as the url or if enabled in config
      // use the google image search api to retrieve and image.

      $img_url = '#';

      if($conf['core']['use_google_image']==true){
        $img_url = $this->googleImage($product_name);
        
        // We place our own google branding on top of the image to ensure we
        // are comliant with the google ajax api guidelines for image search
        // see ()

        $image_class='google_image';
        $google_branding_text = "<i class='fa fa-google'></i>";
        $image_text  .= ' [google images]';
        $overlay_title = "title='Powered by Google Images'";
      }

      

      

      
        
    }
  
  return "<div $overlay_title class='$image_class'>$google_branding_text</div>
          <img class='' src='$img_url' title='$image_text' alt='[image] $image_text' />
  ";
  //return $img_url;

  }



  /**
   * Find specified product in recipes
   *
   *
   */

  public function findInRecipes($pid){

    $query_stmt = $this->db_connection->prepare("SELECT recipe_id FROM recipe_items WHERE product_id = :pid");
    $query_stmt->execute(array('pid'=>$pid));
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

    return $returned_data;
    



  }

  /**
   * List product categories
   *
   */

  public function listCategories(){

    $query_stmt = $this->db_connection->prepare("SELECT * FROM product_categories");
    $query_stmt->execute();
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
    //return $query_stmt->fetchAll(PDO::FETCH_ASSOC);

  
    return $returned_data;
    



  }

  /**
   * Find products from a specific recipes
   *
   *
   */

  public function findFromRecipes($recipe_id){

    $query_stmt = $this->db_connection->prepare("SELECT * FROM recipe_items WHERE recipe_id = :recipe_id ");
    $query_stmt->execute(array('recipe_id'=>$recipe_id));
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

    return $returned_data;
    



  }


  /** Returns all recipes
   *
   */

   public function getAllRecipes(){

    
    $query_stmt = $this->db_connection->prepare("SELECT * FROM recipes");
    $query_stmt->execute();
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

    return $returned_data;
    

   }

  /** Returns info about a single recipe
   *
   */

   public function getRecipe($recipe_id){

    
    $query_stmt = $this->db_connection->prepare("SELECT * FROM recipes WHERE id = :recipe_id");
    $query_stmt->execute(array('recipe_id'=>$recipe_id));
    $returned_row = $query_stmt->fetch(PDO::FETCH_ASSOC);

    return $returned_row;
    

   }

  

  /** Get all category names
   *
   */

  public function getCategoryName($cat_id){


    $query_stmt = $this->db_connection->prepare("SELECT name FROM product_categories WHERE id = :cat_id");
    $query_stmt->execute(array('cat_id'=>$cat_id));
    $returned_data = $query_stmt->fetch(PDO::FETCH_ASSOC);

   return $returned_data['name'];


  }


  /** Lookup product price from the database
   *
   * @param   id    The product id to lookup
   *
   * @return  $returned_row['price']  The price of the product;
   *
   */

  public function priceLookup($id,$calculated=true){

    $query_stmt = $this->db_connection->prepare("SELECT price FROM products WHERE id = :id");
    $query_stmt->execute(array('id'=>$id));
    $returned_row = $query_stmt->fetch(PDO::FETCH_ASSOC);

    $output = (($calculated==true) ? calculatePrice($returned_row['price']) : $returned_row['price']);
    return $output;
  }


/** Returns no of products in database that are in stock
 *  This is used for the pagination
 * @param   (bool)  $instock  Whether to include items that are not instock
 * @param   (bool)  $category  Which product category to count
 */

  public function countOf($instock=false,$category='all'){

    $query = "SELECT * FROM products ";

    $query .= (($instock==true || $category != 'all') ? ' WHERE ' : '');

    // If we're looking for only in stock
    $query .= ($instock==true ? "  stock_qty > 0 " : '');

    $query .= (($instock==true && $category != 'all') ? ' AND ' : '');

    $query .= ($category !='all' ? " category = :category " : '');

    $query_stmt = $this->db_connection->prepare($query);

    if($category != 'all'){
      $query_stmt->bindParam(':category', intval($category), PDO::PARAM_INT);


    }


    
    $query_stmt->execute();
    
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

    

  return count($returned_data);

  }



  /** Builds search suggestions from the product name and descriptions and
   * recipe names and methods
   *
   */

  public function makeSearchSuggestions(){

    //$query_stmt = $this->db_connection->prepare("SELECT name,description FROM products");
    //$query_stmt->execute();
    //$returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

    $returned_product_data = $this->getAllFromCategory();
    $returned_recipe_data = $this->getAllRecipes();

    

    $datalist = "<datalist id='search'>"; 
    $words = array();
    
    /* Iterate over product data first
     */

  
    foreach($returned_product_data as $returned_row)

    {

      // Foreach product, split the name into individual words

      $name_words = explode(" ",$returned_row['name']);
      $description_words = explode(" ",$returned_row['description']);
      $name_words = array_merge($name_words,$description_words);
      //$all_words = array_merge($all_words,$name_words);
      //printR($name_words);

      // build an array of words with a count of their occurence

      // These are common words we will ignore
      $discount_words = [
                          'a','or','cooking','everything','description','the','these','and','&','it','with','served'
                          ];

      foreach($name_words as $id => $word){
        $word = strtolower($word);
        // remove common punctuation characters
        $word = str_replace([',','.','!',':',';','_','-'],"",$word);
  
        if(!empty($word)){
          // Skip the discount_words
          if(!in_array($word,$discount_words)){
            
            // if the word hasn't already been added, add it to the output array
            if(!in_array($word,$words)){
  
  
                $words["$word"] = $word;
               
  
                // Build the datalist
                // see (http://www.w3schools.com/tags/tag_datalist.asp)
  
                $datalist .= "<option value='$word'>";
  
            }
          }
        }
      }
    }

    $datalist .= "</datalist>";
  
    return $datalist;

  }




  /**
   * Searches all tables...
   *
   * TODO ... and pages
   * for the search terms provide
   *
   */
  
  public function searchAll($search_query){

    // First search products

    // n.b. use UPPER in SQL to ensure that comparison catches all results
    // see ( http://use-the-index-luke.com/sql/where-clause/functions/case-insensitive-search)
      
    $query_stmt = $this->db_connection->prepare("SELECT * FROM products WHERE UPPER(name) LIKE UPPER(:search_query)
                                                  OR UPPER(description) LIKE UPPER(:search_query)");
    $query_stmt->execute(array('search_query'=>'%'. $search_query .'%'));
    $returned_search_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

    

    
    // Then search recipes
    
    $query_stmt = $this->db_connection->prepare("SELECT * FROM recipes WHERE UPPER(name) LIKE UPPER(:search_query)
                                                  OR UPPER(method) LIKE UPPER(:search_query)");
    $query_stmt->execute(array('search_query'=> '%'. $search_query .'%' ));
    $returned_recipe_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);



    // Also find recipes containing the products that match the search results
        
    foreach($returned_search_data as $product_info){
    //printR($product_info);

         $product_id = $product_info['id'];
       
      $returned_recipe["product_id"] = $this->findInRecipes($product_id);

    }
    
    return ['product_data'=>$returned_search_data,'recipe_data'=>$returned_recipe_data,'product_recipes'=>$returned_recipes];
  

  }





  /**
   * Retrieves all products of the given category
   * allowing for limiting the number of rows retrieved and specifying where to
   * begin retrieval to allow for pagination of results
   *
   *  @param  (int)category         The category id no to refine lookup with
   *  @param  (int)sort             The column by which to sort the data
   *  @param  (string)dir           The direction to sort in (default=desc)
   *  @param  (int)start            Which row to start retriving from (default=0)
   *  @param  (int)limit            How many rows to retrieve (default=30)
   * 
   * @return (array)returned_data   The resulting rows from the db or false
   */

  public function getAllFromCategory($category='all',$sort='id',$dir='desc',$start=0,$limit='all'){

    // Only allow DESC or ASC to be passed to the db call as we are not binding this as a parameter to PDO.

    $dir = ($dir == 'asc' ? 'ASC' : 'DESC');

  
    
    $limit = (empty($limit) ? all : $limit);
    
    $limit = ($limit == 'all' ? $this->countOf(false,$category) : $limit);
    // If category is set to anything other than the default of 'all ' then append a WHERE clause to the
    // SQL statement
  
    $where = ($category == 'all' ? '' : 'WHERE category = :category');

    // Construct our query

    $query = "SELECT * FROM products $where ORDER BY `$sort` $dir LIMIT :start, :limit";

    $query_stmt = $this->db_connection->prepare($query);

    // To bind the parameters we must cast them as integers AS WELL as using the global
    // This took a week to figure out!

    $query_stmt->bindParam(':start', intval($start), PDO::PARAM_INT);
    $query_stmt->bindParam(':limit', intval($limit), PDO::PARAM_INT);

    // Only bind this paramter if the category was set otherwise an exception will be thrown

    if($category != 'all'){
      
      $query_stmt->bindParam(':category', intval($category), PDO::PARAM_INT);

    } 
        
    // Complete the db request and return data as an array.

    $query_stmt->execute();
    
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

    return $returned_data;


  }









  /**
   * Returns information about a product
   *
   *  @input product_id   The id of the product to return info about
   *  @input product_id   A specific field to return, if not set then all are returned as an array from PDO
   *
   */

  public function lookup($product_id){

  
    $_stmt_query = $this->db_connection->prepare("SELECT id,name,stock_qty,description,price,category,reorder_qty FROM products WHERE id = :product_id");
    $_stmt_query->execute(array('product_id'=>$product_id));
    $returned_data = $_stmt_query->fetch(PDO::FETCH_ASSOC);
    

  return $returned_data;

  
  

  }


}
