  <?php

    $grandtotal =0;
    $tdata ='';
    $delivery = 4.00;
    $discount =0;
    $dis_minus=0;
    $finaltotal=0;
    $disabled='';

  // Check for a page action call

  if(isset($_POST['a'])){

    switch($_POST['a']){

   //  basket actions

   case 'update_basket':
        $shopping_basket->update();
       // $page='basket';
        break;

      // Clear basket

      case 'clear_basket':
        $shopping_basket->clear();
        page_redirect('products');
        break;

      case 'save_basket':
        $shopping_basket->save();
        $page = 'savedbaskets';
        break;


    }


  }

  // Process basket items

  if(!empty($_SESSION['basket_items'])){



    foreach($_SESSION['basket_items'] as $item => $qty){


  
      $price = $product->priceLookup($item);
      $subtotal = number_format(($price*$qty),2);

      $basket_totals = $shopping_basket->totalPrice();

      

      $product_name = $product->lookup($item);

      $disabled = (isset($_SESSION['promo_code']) ? 'disabled' : '');

      $tdata .= "
        <tr>
          <td>
            $item
          </td>
          <td>
            ".$product_name['name']."
          </td>
          <td>
            &pound;$price
          </td>
          <td><input  name='basket_items[".$product_name['id']."]' min='0' max='".$product_name['stock_qty']."' type='number' value='$qty'>

          </td>
          <td>
            &pound;$subtotal
          </td>
        </tr>
      ";

    }


    // Finally add the bottom row of the table with the delivery cost, and totals.

    $tdata .= "
      <tr>
        <td colspan='4' class='right'> Sub-total
        </td>
        <td>
          &pound;".$basket_totals['basket_subtotal']."
        </td>
      </tr>
    ";



    
    $tdata .= "
      <tr>
        <td colspan='2'>
        Discount
        </td>
        <td>".$basket_totals['discount_percentage']." %</td>
        <td>".(isset($_SESSION['promo_code']) ? $_SESSION['promo_code'] : '')."</td>
        <td class='red'>
          -&pound;".$basket_totals['discount_amount']."
        </td>
      </tr>
    ";

    $tdata .= "
      <tr>
        <td colspan='4' class='right'><i class='fa fa-truck'></i> Delivery
        </td>
        <td>
          &pound;".$basket_totals['delivery']."
        </td>
      </tr>
    ";



    $tdata .= "
      <tr>
        <td colspan='4'>Grand total
        </td>
        <td>
          &pound;".$basket_totals['basket_total'] ."
        </td>
      </tr>
    ";




  }
  else{

    // If the basket is empty, there is no point in displaying it to the user,
    //

    display_feedback("Your basket is empty, you'll need to add some products in order to view the basket",'Basket empty!','products','info');


    }
