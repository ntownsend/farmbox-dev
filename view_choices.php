
<?php

  // Replace empty values with sensible defaults


 $default_sort = (($page=='products')||($page=='inventory')  ? 'name' : 'id');
 $sort              = (!empty($_GET['sort'])     ? $_GET['sort']       : $default_sort  );
 $sort_direction    = (!empty($_GET['dir'])      ? $_GET['dir']        : 'asc');
 $start_row         = (!empty($_GET['start'])    ? $_GET['start']      : 0     );
 $row_limit         = (!empty($_GET['num'])      ? $_GET['num']        : 60    );
 $selected_category = (!empty($_GET['cat'])      ? $_GET['cat']        : 'all'    );

    // If therr is no sort by specified then sort by id

    $sort_by = (in_array($sort,$order_by) == true ? $sort : 'id');

    // Determine th eopposite direction for links etc

    foreach($order_by as $value){

        $alt_dir["$value"] = 'asc';

        if($sort==$value){

          $alt_dir["$value"] = ($sort_direction=='asc' ? 'desc' : 'asc' );

        }


      }

    

    // Retrieve categories from db and build the <option>s for the search refinement


    if(($page=='products')||($page=='inventory'))
    {
          // Set total number of items for the pagination totals and links

    $total_no_of_items = $product->countOf($instock=false,$selected_category);

   //   $categories = $product->listCategories();
   //   echo ' -- '. count($categories) .' - '. $categories['0']['id'];
      //printR($categories);
      $category_choices ='';

      foreach($product->listCategories() as $key => $category){
        // Check to see if this is the one currently selected.
  
  
        $selected = ($selected_category == $category['id']  ?   'selected' : '');

  
        $category_choices .= "<option value='". $category['id'] ."' $selected > ". $category['name'] . "</option>";


      }


    }elseif($page=='users'){

  // Set total number of items for the pagination totals and links

  $this_category = (($selected_category=='all') ? false : $selected_category);

  $total_no_of_items = $user->numOfUsers(false,false,$this_category);



      $groups = $user->getGroups();
      $category_choices ='';
      foreach($groups as $key => $category){
  
        // Check to see if this is the one currently selected.
    
    
        $selected = ($selected_category == $category['id']  ?   'selected' : '');
    
        $category_choices .= "<option value='". $category['id'] ."' $selected > ". $category['group_name'] . "</option>";
  
      }
  
  

    }

    // build the options for the num of results selected
    //
    //

    $options = ['all','4','8','16','24'];

    $num_choices = '';

    foreach($options as $number){

        $selected = ($row_limit == $number ? 'selected' : '');

        $num_choices .= "<option val='$number' $selected>$number</option>";



      }



    $options = $order_by;
    $sort_choices = '';

      foreach($options as $sorter){

        $selected = ($sort == $sorter ? 'selected' : '');

        $sort_choices .= "<option val='$sorter' $selected>$sorter</option>";



      }


    $options = ['asc','desc'];
    $dir_choices = '';

      foreach($options as $direction){

        $selected = ($sort_direction == $direction ? 'selected' : '');

        $dir_choices .= "<option val='$direction' $selected>$direction</option>";



      }


  // Setup the pagination links ready for the template

  // If the row limit is all, then we need to replace it with a number value
  // i.e. the total number of items
  $row_limit = $row_limit == 'all' ? $total_no_of_items : $row_limit;

  // build the request string based on the current values
  $request_string ='&sort='. $sort .'&num='. $row_limit .'&dir='. $sort_direction .'&cat='. $selected_category ;

  // The previous and next links are calculated  by checking the current start row, minus the limit us not less than
  // or equal to zero.
  
  $prev_start = (($start_row-$row_limit)<=0 ? 0 : intval($start_row-$row_limit) ) ;
  $next_start = ($start_row+$row_limit);
  
  $prev_link = (($start_row != 0 ) ? "<a href='./?p=$page&start=$prev_start$request_string'><i class='fa fa-chevron-circle-left'></i> prev</a>" : '');
  $next_link = ($total_no_of_items > ($start_row+$row_limit) ? "<a href='?p=$page&start=$next_start$request_string'>next <i class='fa fa-chevron-circle-right'></i></a>": '');

  // The 'extent' indicates how far through the current data set we currently  are

  $extent = ((($row_limit+$start_row)>$total_no_of_items) ? $total_no_of_items : $row_limit+$start_row);

  $num_wayfinder = "Showing results ". ($start_row+1) ." to $extent of  $total_no_of_items  $prev_link  $next_link ";

// Now build the html to represent the  choices for the user
  
$view_choices = "


  <div class='layout_chooser'>
  <form name='view_choices' action='./' method='get'>
    <input type='hidden' name='p' value='$page' />
    Showing 
    <select name='cat'>
      <option>all</option>
      $category_choices
    </select>
 
    Displaying
    <select name='num'>
      $num_choices
    </select>
    results per page
- 
    Sorted by
    <select name='sort' >
      $sort_choices
    </select>
    Direction 
    <select name='dir' >
      <option></option>
      $dir_choices
    </select>
    
    <button class='small' type='submit'> Go <i class='fa fa-chevron-right'></i></button>

  </form>

  </div>
<div class='numbar' >
  $num_wayfinder
</div>
";
