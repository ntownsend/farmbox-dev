<?php


  /**
  *
  *
  *
  */


  Class Message{

  
  
  
     private $db_connection;
  
    /* Constructor class
     *
     *
     */
     
    public function __construct($db_connection){
  
    $this->db_connection = $db_connection;
  
  
    }

    /** Add a message to the database
     */
  

     public function  add($name,$email,$phone,$message_text){

      // add message to db

      $timestamp = date('U');

      $stmt_query = $this->db_connection->prepare("INSERT INTO contact_messages (name,email,phone,message_text,received)
                                                   VALUES (:name,:email,:phone,:message_text,:timestamp) ");

      $stmt_query->execute(array('name'=>$name,
                                'email'=>$email,
                                'phone'=>$phone,
                         'message_text'=>$message_text,
                            'timestamp'=>$timestamp
                                ));


    }


    /** Mark a message as replied
     */

     public function markReplied($msg_id,$reply_msg){

      $timestamp = date('U');
      $query_stmt = $this->db_connection->prepare("UPDATE contact_messages SET replied_date=:timestamp,replied_by=:user,reply_msg=:reply_msg  WHERE id = :msg_id");
      $query_stmt->execute(array('msg_id'=>$msg_id,'timestamp'=>$timestamp,'user'=>$_SESSION['uid'],'reply_msg'=>$reply_msg));


     }

    /** Delete a message from the db
     */

     public function delete($msg_id){

      $query_stmt = $this->db_connection->prepare("DELETE FROM contact_messages WHERE id = :msg_id");
      $query_stmt->execute(array('msg_id'=>$msg_id));


     }

  
    /* Returns the total number of orders placed.
     */
  
    public function total(){
  
  
      
      $query_stmt = $this->db_connection->prepare("SELECT id FROM contact_messages ");
      $query_stmt->execute();
      $returned_row = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
  
      
  
      return count($returned_row);
      
  
  
  
    }

    /** Returns the total no of messages that are not marked as read
     *
     */

    public function awaitingResponse(){
      
      $query_stmt = $this->db_connection->prepare("SELECT * FROM contact_messages WHERE replied_date=0 ORDER BY received DESC");
      $query_stmt->execute();
      $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

      return $returned_data;

    }

    /** Returns the total no of messages that are not marked as read
     *
     */

    public function getReplied(){
      
      $query_stmt = $this->db_connection->prepare("SELECT * FROM contact_messages WHERE replied_date>0 ORDER BY received DESC");
      $query_stmt->execute();
      $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

      return $returned_data;

    }

    /**
     * Returns all messages a user has sent via the live chat function sorted so that the most recent are first
     * TODO - Allow for a limit to be specified
     * 
     */


    public function chatHistory($user_id){
  
  
      
      $query_stmt = $this->db_connection->prepare("SELECT * FROM chat_messages WHERE  msg_from = :user_id ORDER BY timestamp desc");
      $query_stmt->execute(array('user_id'=>$user_id));
      $returned_row = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
  
      
  
      return $returned_row;
      
  
  
  
    }

    /** retrive the contact message history for a specified user
     *
     *
     *
     */

    public function contactHistory($user_id){
  
  
      
      $query_stmt = $this->db_connection->prepare("SELECT * FROM contact_messages WHERE  msg_from = :user_id");
      $query_stmt->execute(array('user_id'=>$user_id));
      $returned_row = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
  
      
  
      return $returned_row;
      
  
  
  
    }
  
  
  }
