<?php


  // Start the page render timer
  // to be displayed (by default)
  // in the footer

  $timer_start = microtime(TRUE);

  // Start a session here as we rely on it throughout.
  session_start();

  // Load configuration
  require('./config.php');


  // Load Classes Required to set up the rest of the application
  // (n.b. Database MUST be first!)

  require("./inc/Class.Database.inc.php");
  require("./inc/Class.UI.inc.php");

  // Load Language file 

  $lang_file = "./lang/".$conf['core']['language'].".php";

  if(!file_exists($lang_file)){

    error_debug("ERROR: FATAL: Cannot load lang file");
  }
  else{

    require($lang_file);

  }

  // Load Classes (n.b. Database must be first!)

  require("./inc/Class.Product.inc.php");
  require("./inc/Class.User.inc.php");
  require("./inc/Class.UserSession.inc.php");
  require("./inc/Class.Basket.inc.php");
  require("./inc/Class.Orders.inc.php");
  require("./inc/Class.Message.inc.php");

  $databaseOb = db_connect();

  /* Actions for logged in user
   */
    $user = new User($databaseOb);

  if(userLoggedIn()==true){
  
    $userinfo = $user->info($_SESSION['uid']);
    $user->updateLastActive($_SESSION['uid']);
  }
   /* Instantiate the applications objects
   */

  $shopping_basket = new Basket($databaseOb);
  $product = new Product($databaseOb);
  $orders = new Orders($databaseOb);
  $message = new Message($databaseOb);

  /* Handle requested events before rendering any pages
   *
   *
   */



// order
    
  if(isset($_POST['order'])&&(($_POST['order']=='Place order')||($_POST['order'] =='confirm'))){

      $page='order';

    }


// Add to basket action.
// Keep this here to allow this to be called from anywhere

  if(isset($_POST['a'])){

    if($_POST['a']=='add_to_basket'){

        $shopping_basket->add($_POST['product_code'],$_POST['qty']);

 
    }



  }
  

  /* Determine and render Page Output
   *
   *
   *
   */

    // Page header

// Keep all of our output in memory until we are finished processing
// Do this using ob_start  so that we can modify page headers
// and let page_redirect() function handle redirection at any stage.

 ob_start();

  include "html/header.html";

  if(userLoggedIn()==false){

    if(!isset($_GET['p'])){
      include './html/promo_banner.html';
 
      if(!isset($page)){
        $page='products';
      }
    }
  }

   // If the$page variable has not beenn set by anything already then
   // get the page variable from the GET request.
   // Sanitise and then load pages accordingly
   

    if(!isset($page)){

      if(isset($_GET['p'])){

        $page=strip_tags(trim($_GET['p']));

      }else{

      $page= 'products';

      }

    }

    // Load the page templater

    if(file_exists("./". $page .".php")){

      include "./" . $page .".php";

    }

    // Load the page or form template

    if(file_exists("./html/". $page .".html")){

      include "./html/" . $page .".html";

    }
    elseif(file_exists("./html/forms/". $page .".html")){

      include "./html/forms/" . $page .".html";

    }
    else{

      // This is consistent with the 404 error behaviour set in .htaccess

      page_redirect('404');
      include "html/layout.html";

    }

    include "html/footer.html";

  // Now output the ob we started. If we got this far we are rednring the page we have built

  ob_flush();

?>
