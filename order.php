<?php
// Set the user level for this page.

//$user->authorise(0);

  // check that this page was called by the form in the basket
  
  if((isset($_POST['order']))&&($_POST['order']=='confirm')){
    
    if(isset($_SESSION['basket_items'])){

    
      $orders->placeOrder();
      display_feedback("In production the payment gateway would be incorporated here. Successful payment would place the order.",
                       'Order Confirmed','order_history','notify');
    
    }

  }
  else{

    $tdata ='';

    // Display the order items again in a table, non editable,

    foreach($_SESSION['basket_items'] as $item => $qty)
    {
            $price = $product->priceLookup($item);
      $subtotal = number_format(($price*$qty),2);

      $basket_totals = $shopping_basket->totalPrice();

      

      $product_name = $product->lookup($item);//,'name');
      $tdata .= "
        <tr>
          <td>
            $item
          </td>
          <td>
            ".$product_name['name']."
          </td>
          <td>
            &pound;$price
          </td>
          <td>$qty

          </td>
          <td>
            &pound;$subtotal
          </td>
        </tr>
      ";

    }

    $tdata .= "
      <tr>
        <td colspan='4' class='right'> Sub-total
        </td>
        <td>
          &pound;".$basket_totals['basket_subtotal']."
        </td>
      </tr>
    ";



    
    $tdata .= "
      <tr>
        <td colspan='2'>
        Discount
        </td>
        <td>".$basket_totals['discount_percentage']." %</td>
        <td>".(isset($_SESSION['promo_code']) ? $_SESSION['promo_code'] : '')."</td>
        <td class='red'>
          -&pound;".$basket_totals['discount_amount']."
        </td>
      </tr>
    ";

    $tdata .= "
      <tr>
        <td colspan='4' class='right'><i class='fa fa-truck'></i> Delivery
        </td>
        <td>
          &pound;".$basket_totals['delivery']."
        </td>
      </tr>
    ";



    $tdata .= "
      <tr>
        <td colspan='4'>Grand total
        </td>
        <td>
          &pound;".$basket_totals['basket_total'] ."
        </td>
      </tr>
    ";




    }

  
