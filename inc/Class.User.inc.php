<?php

/** Class.User.inc.php
* Provides all User related features.
*
*
* Contents
*
* + constructor
* + authenticate
* + exists
* + create
* + info
* + logout
* 
*/

Class User {

  protected	$_uid;
  protected	$_username;
  protected	$_password_hash;
  protected	$_email;
  protected $_user_level;
  private $db_connection;

  /** MAGIC constructor class
   *
   *
   *
   */

  public function __construct($db_connection){

  $this->db_connection = $db_connection;
  

  }



 /** Creates (registers) a new user
  *
  *
  *
  *
  */

  public function createNewUser($username,$password,$email,$send_email=true,$autoreset=false,$group=0){
    
    $timestamp = date('U');
   
    $query_stmt = $this->db_connection->prepare("INSERT INTO users (username,password,email,`group`,joined)
                                                  VALUES  (:username,:password,:email,:group,:timestamp)");
    $query_stmt->execute(array('username' => $username, 'password' => $password, 'email' => $email,'group' => $group,'timestamp'=>$timestamp));
    $uid = $this->GetUID($username);

    // Add an empty row in the customer table that stores delivery details
    // we get the customer to fill this in later to streamline the signup process


    $query_stmt = $this->db_connection->prepare("INSERT INTO customers (user_id) values  (:user_id)");
    $query_stmt->execute(array('user_id' => $uid));

    // Add an empty row in the chat DB
    
    $query_stmt = $this->db_connection->prepare("INSERT INTO chat (user_id,timestamp) values  (:user_id,0)");
    $query_stmt->execute(array('user_id' => $uid));

    
    

    $_SESSION['did_register_email'] = $email;
    session_write_close();
    if($send_email==true){
  
      $this->sendVerificationEmail($uid,$username,$email);
     

      // Autoreset is used when an admin user adds users.
      // instead of setting and then having to distribute a password
      // a rest link is sent straight to the employee who is then prompted to create a new password.
    } 

    if($autoreset==true){

      if(!$this->sendResetLink($email)){
          error_debug("Email error");

          }
          else
          {
                    display_feedback("email success");
                    error_log("email success");

          }
          ;


      }
      
  

  }

  /** Returns the number of users in the database
   *
   * Optionally it will return only those who have activated their accounts
   * and / or only customer type accounts or users in a particular group
   */


  public function numOfUsers($unverified=false,$customers=false,$ingroup=false){

    $query = "SELECT * FROM users WHERE";

    $query .= ($unverified==true ? " last_authenticated = 0" : ' last_authenticated > 0');

    $query .= ($customers ==true ? " AND `group` = 0 " : '');

    

    $query_stmt = $this->db_connection->prepare($query);

    if(is_numeric($ingroup)){

      $query .=  " AND `group` = :ingroup ";
      $query_stmt->execute(array('ingroup'=>$ingroup));
    }else{
    
      $query_stmt->execute();
    }
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
  
    return count($returned_data);

  }

/** Returns the number of active users in the database
 */

public function getActiveUsers(){

  $three_minutes_ago = date('U')-180;

  $query_stmt = $this->db_connection->prepare("SELECT id,username,last_active from users WHERE last_active > :time");
  $query_stmt->execute(array('time'=>$three_minutes_ago));
  $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
  
  return $returned_data;


}

/** Returns information about all customer users
 *
 */

  public function listCustomers(){


    $query_stmt = $this->db_connection->prepare("SELECT * FROM users WHERE `group`=0");
    $query_stmt->execute();
  $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

  return $returned_data;

  }



  



 /** List all users 
  *
  * Retrieves a partial number of records based on the pagination info received.
  *
  *
  *
  */


  public function listAll($sort='username',$dir='asc',$start=0,$limit=30,$group='all'){

    $dir = ($dir == 'asc' ? 'ASC' : 'DESC');



    $where = ($group == 'all' ? '' : 'WHERE `group` = :group');

  //  $query = "SELECT id,username,group,email,last_authenticated,last_active FROM users";
  
    $query = "SELECT * FROM users $where ORDER BY `$sort` $dir LIMIT :start, :limit";


    //$query_stmt = $this->db_connection->prepare("SELECT * FROM users ORDER BY :sort desc");
    $query_stmt = $this->db_connection->prepare($query);
    
    


    // Use bindParam here instead of array() so we can specify the input types individually as required
    // as the default stance of PDO is that input is type PARAM_STRING, the manual warns against using
    // int values without being specific about it
    
    $query_stmt->bindParam(':start', intval($start), PDO::PARAM_INT);
    $query_stmt->bindParam(':limit', intval($limit), PDO::PARAM_INT);



    if($group != 'all'){
      
      $query_stmt->bindParam(':group', intval($group), PDO::PARAM_INT);

    } 
      

    $query_stmt->execute();

    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

    // Add the group names into the returned array
    
    foreach($returned_data as $key => $value){
      $returned_data["$key"]['group_name'] = $this->groupName($value['group']);
    }


    //$returned_data_sorted = arsort

    return $returned_data;
  }



 /** Retrieves full infpormation about a user account
  *
  *
  *
  */

  public function info($uid){

    
    $query_stmt = $this->db_connection->prepare("SELECT * FROM users WHERE id = :uid");
    $query_stmt->execute(array('uid' => $uid));
    $returned_data =$query_stmt->fetch();
    // Blank the password field out, just in case
    $returned_data['password'] ='';
    if(isset($returned_data['group'])){
      $returned_data['group_name'] = $this->groupName($returned_data['group']);
    }
    else
    {
      echo "NO GROUP";
    }
    
    $returned_data['address_details'] = $this->getAddress($uid);
  
    
    return $returned_data;

  }
  
 /** Makes the user id into an account no
  * Performed as a method to allow for future changes
  *
  *
  */

  public function makeAccountNo($id){

      return str_pad( $id,7,0,STR_PAD_LEFT);


  }

  
 /** Lookup a specific user
  * 
  *
  */



  public function lookup($uid){


        $query_stmt = $this->db_connection->prepare("SELECT username FROM users WHERE id = :uid");
        $query_stmt->execute(array('uid' => $uid));
        $returned_data =$query_stmt->fetch(PDO::FETCH_ASSOC);
        

        return $returned_data['username'];

  }


 /** Get the groupname of a specified user
  *
  *
  *
  *
  *
  */


  protected function groupName($gid){

    $query_stmt = $this->db_connection->prepare("SELECT name FROM user_groups WHERE  id = $gid");
    $query_stmt->execute(array('gid'=>$gid));
    $returned_row = $query_stmt->fetch(PDO::FETCH_ASSOC);
    return $returned_row['name'];

      }



  /* Log a user out
   *
   */

  public function logout(){
    session_unset();
    session_destroy();
    page_redirect('login');
  }



  /* Sends (or resends a verification email to a user)
   *
   *
   *
   *
   *
   */

   
  private function sendVerificationEmail($_uid,$_username,$_email,$change=false){

    global $conf;
    //global $db_connection;
    // Generate our string

    $_time = date('U');
    $_hash = md5("$_email$_time");

    //Uses suggestion for processing from
    //http://stackoverflow.com/questions/3794959/easiest-way-for-php-email-verification-link

    $subject = 'Confirm your registration!';
    
    $link = $conf['site_uri'] .'?p=verify&hash='. $_hash;
    
    if($change == false){
      $message = "Hi " . $_username . " To complete your sign up with ". $conf['core']['company_name']
    ." visit this link to verify your account

     $link";

      $feedback =' ';
      $feedback_link = '';
     
    }else{

      $message = "Hi " . $_username . " Thanks for updating you email address with ". $conf['core']['company_name']
    ." please visit this link to verify the updated address.

     $link";

      $feedback = 'The next time you sign out, you won\'t be able to sign back in until your verify the new email address.';
      $feedback_link = 'account';

    }
     //$headers .= 'Abe & Cloe <noreply@thejubster.co.uk>' . "\r\n";


      if(sendEmail($_email,$subject,$message)){
  
        $_query_stmt = $this->db_connection->prepare("INSERT INTO verifications (hash,user) values  (:hash,:user)");
        $_query_stmt->execute(array('user' => $_uid,'hash' => $_hash));
      

      display_feedback($feedback,'Almost done',$feedback_link,notify);
      return true;
      
      }
      else{
        display_feedback("We seem to be having some trouble completing that request. Please try again, and if the error persists please <a href='./?p=contact'> get in touch</a>",'General error');
      return false;

      }
  

    }

/* Email a password reset link to a user
 */


  public function sendResetLink($email){

    

    global $conf;

      // Check that the email address we were passed exists
      $email = sanitise($email);
      $uid = $this->Exists($email);
      if($uid!=false){
        $_time = date('U');
        $_hash = md5("$email$_time");

        $username = $this->info($uid);
        $username = $username['username'];

        $subject = '&#x2605; ['. $conf['core']['company_name'] .'] Reset your account';
        $link = $conf['site_uri'] .'?p=reset_account&hash='. $_hash;

        // TODO - Move the email headers section out into config - we only need it once and we email several tomes
        // TODO - Make a send an email finction too,

        // Required for html mail.
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: Abe & Cloe ' . "\r\n";
        $headers .= 'Reply-To: Abe & Cloe <noreply@thejubster.co.uk> ' . "\r\n";

        // TODO Move the email body into  the db as config
        
        $message = "Hi <b>" . $username . "</b> <br><br>A password reset request has been received for your ". $conf['core']['company_name'] ." account.<br><br>
        If you are expecting this email then please visit this link to set a new password for your account.<br><br>$link<br><br> If not, you may safely ignore it.<br><br>Kind Regards<br><br>
        ".$conf['core']['company_name'];
        

        if(mail($email,$subject,$message,$headers)){
          $_query_stmt = $this->db_connection->prepare("INSERT INTO account_resets (hash,user) values  (:hash,:user)");
          $_query_stmt->execute(array('user' => $uid,'hash' => $_hash));

          display_feedback("An email has been sent to $email. Please follow the instructions in the email to reset your account.",
          "Password reset",'','notify');
        }
        else{

          display_feedback("An error occured with your request, please try again");
          
      	}

      }
      else{

        display_feedback("That email address wasn't found in our records. Please check and try again.",
        "Email address not found",'resetaccount');

      }

      

  }

  /* Verify a reset password hash
   */

  public function verifyReset($hash){

   
      $query_stmt = $this->db_connection->prepare("SELECT user from account_resets WHERE hash = :hash");
      $query_stmt->execute(array('hash'=>$hash));
      $returned_row = $query_stmt->fetch(PDO::FETCH_ASSOC);

      if(empty($returned_row)){

        return false;
        break;

      }
      $uid = $returned_row['user'];


    return $uid;

    page_redirect('pwreset');



  }

  /** Change a users password
   */


public function changePassword($uid,$passwordhash){

  $query_stmt = $this->db_connection->prepare("UPDATE users SET password = :passwordhash WHERE id = :uid");
  $query_stmt->execute(array('uid'=>$uid,'passwordhash'=>$passwordhash));
  //$returned_row = $query_stmt->fetch();

  return true;

}


/** Retrieve a users address details from the db
 *
 *
 *
 */

  public function getAddress($user_id){

    $query_stmt = $this->db_connection->prepare("SELECT * FROM customers WHERE user_id = :user_id");
    $query_stmt->execute(array('user_id'=>$user_id));
    $returned_data = $query_stmt->fetch(PDO::FETCH_ASSOC);
    
    return $returned_data;



  }



/** Change a users address details in the customers table
 *
 *
 *
 */

public function changeAddress($user_id,$house,$address,$country,$postcode){


    $user_id = $_SESSION['uid'];
  
    $query_stmt = $this->db_connection->prepare("UPDATE customers SET house=:house, address=:address, country=:country, postcode=:postcode WHERE user_id = :user_id");
                                                 
    $query_stmt->execute(array(
                              'user_id'=>$user_id,
                              'house'=>$house,
                              'address'=>$address,
                              'country'=>$country,
                              'postcode'=>$postcode
                               ));
    page_redirect('account');
}


public function changeEmail($uid,$email){

  $query_stmt = $this->db_connection->prepare("UPDATE users SET email = :email, last_authenticated = 0 WHERE id = :uid");
  $query_stmt->execute(array('uid'=>$uid,'email'=>$email));
    $username = $this->lookup($uid);
    $this->sendVerificationEmail($uid,$username,$email,true);

  return true;

}

  /** Clears all password resest hashes for the given user id
   *
   *  This function is called by sendReset prior to emailing and storing a hash
   * to ensure that there is only ever one reset hash per user in the database
   */

public function clearReset($uid){

      

      $_query_stmt = $this->db_connection->prepare('DELETE FROM account_resets WHERE user = :uid');
      $_query_stmt->execute(array('uid' => $uid));
      //$_returned_row = $_query_stmt->fetch();

}




  /* Verify a user based on a hash code that they have been sent
   * 
   * Returns the user id if the hash exists and is valid or false if not.
   */ 

  public function verifyEmail($hash){

    global $db_connection;

    // Look for an entry that contains the hash we were passed

    $query_stmt = $this->db_connection->prepare('SELECT user FROM verifications WHERE hash = :hash');
    $query_stmt->execute(array('hash' => $hash));
    $returned_row = $query_stmt->fetch();

    // Grab the uid so we can update this user
    
    $uid = $returned_row['user'];
  
    if(!empty($uid)){

      $this->updateLastAuth($uid);
      
      $query_stmt = $this->db_connection->prepare('DELETE FROM verifications WHERE hash = :hash');
      $query_stmt->execute(array('hash' => $hash));
      //$returned_row = $query_stmt->fetch();
       
      return $uid;

    }
    else{

      return false;

    }

    



  }


  /* Checks the database to see if a username is in use
   *
   *
   *
   *
   *
   * 
   */

  public function Exists($value){


    global $db_connection;

    // have we been given an email address or username

    if(strpos($value,'@')==true){

        $query_stmt = $this->db_connection->prepare('SELECT id FROM users WHERE email = :email');
        $query_stmt->execute(array('email' => $value));
        

    }else{

        $query_stmt = $this->db_connection->prepare('SELECT id FROM users WHERE username = :username');
        $query_stmt->execute(array('username' => $value));
        
    
    }
    
    $returned_row = $query_stmt->fetch();

    // return the user id if found or false on nothing found

    $exists = ($returned_row ? $returned_row['id'] : false);

    return $exists;

  }

  /** Checks to see if an email address is already in the db
   *
   *  @input string $_email
   *  @return bool $_exists
   */

  public function emailExists($email){

  
    global $db_connection;
          
  
      
    $query_stmt = $this->db_connection->prepare('SELECT email FROM users WHERE email = :email');
    $query_stmt->execute(array('email' => $email));
    $returned_row = $query_stmt->fetch();
  
    $exists = ($returned_row ? true : false);
  
      
    return $exists;

  }

  /** Retrieves the users id based on their username
   * The users id is what is used to track them, the
   * username is therefore changeable
   *
   */
  
  public function GetUID($username){


        
    try {
        $_query_stmt = $this->db_connection->prepare('SELECT id FROM users WHERE username = :username');
        $_query_stmt->execute(array('username' => $username));
        $_returned_row = $_query_stmt->fetch();

        $_user_exists = $_returned_row['id'];

    } catch(PDOException $e) {
        error_debug('ERROR: ' . $e->getMessage());
    }

  return $_user_exists;

  }


  
  /* Authenticate User
   *
   */



  public function Authenticate($username,$password){

  $user_authenticated = false;

      if($this->Exists($username)!=false){

        $query_stmt = $this->db_connection->prepare('SELECT * FROM users WHERE username = :username');
        $query_stmt->execute(array('username' => $username));
        $returned_row = $query_stmt->fetch();
        $db_password_hash = $returned_row['password'];
         
        $user_exists = ($returned_row ? false : true);

        if($returned_row['last_authenticated']=='0'){

          // The user exists but their account has the last)authenticated field set to 0 which indicates a non-verified user
          display_feedback('You must verify your account before you can login.
          You need to visit the link that you were emailed when you signed up.','Could not log you in');
        }
        // Use PHP 5.5's new password_verify() function to check the hash password against the hash of the
        // password provided
        elseif(password_verify($password,$db_password_hash)){
          
          $user_authenticated = $returned_row['id'];
          $this->updateLastAuth($username);
        }
      }
      else{
        systemLog("Login error");
        $page = 'login';
      }

    return $user_authenticated;
  }

  /** Returns a list of all the account types
   *
   *  if $names is TRUE (default) the group names are looked up and added to the returned data
   */


  public function getGroups($names=TRUE){

    $query_stmt = $this->db_connection->prepare("SELECT * FROM user_groups");
    $query_stmt->execute();
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);


    // Add the group names to the returned results
    if($names==TRUE){
      
      for($i=0;$i<count($returned_data);$i++){

        $returned_data["$i"]['group_name'] = $this->getGroupName($returned_data["$i"]['id']);

      }



    return $returned_data;


    
    }
  }

  /** Lookup and return the name of a group
   *
   */
  

  private function getGroupName($gid){

    $query_stmt = $this->db_connection->prepare("SELECT name FROM user_groups WHERE id = :gid");
    $query_stmt->execute(array('gid'=>$gid));
    $returned_data = $query_stmt->fetch(PDO::FETCH_ASSOC);
    
    return $returned_data['name'];


  }


  /* Update last_authenticated entry in user db
   *
   *
   *
   */


  public function updateLastActive($_uid){

    $_timestamp = date('U');
    try{
      $_query_stmt = $this->db_connection->prepare("UPDATE users SET last_active=:timestamp WHERE id=:uid");
      $_query_stmt->execute(array('uid'=>$_uid,'timestamp'=>$_timestamp));
    }
    catch(PDOException $e) {
            error_debug('ERROR: ' . $e->getMessage());
        }

  }

  /* Update last_authenticated entry in user db
   *
   *
   *
   */


  private function updateLastAuth($uid){

    $timestamp = date('U');
    $query_stmt = $this->db_connection->prepare("UPDATE users SET last_authenticated=? WHERE id=?");
    $query_stmt->execute(array($timestamp,$uid));
    
  }

  /* Returns the users authorisation level
   */

  public function authorise($level){

      // TODO - we should have this value stored in the object!

      global $userinfo;
      global $page;

    // If we aren't logged in we are certainlly not authorised'

    if(!isset($_SESSION['uid'])){

         // We can't redirect here as the output has already started when this is called
        page_redirect('login');
         
    }



      $authorised =  ($userinfo['group'] >= $level ? true : false);

      
      
      if(!$authorised){

                /* Log the authorisation failure
                 */

                error_log("AUTHORISATION FAILURE : user: ". $userinfo['username'] . "(". $userinfo['id'] .
                ") Attempting to access page: $page User Group:". $userinfo['group_name'] ."(".
                $userinfo['group'] .")"
                 );


    

        page_redirect('login');
    
       
      }

  }

  /** Updates a user role
   *  to new_group. Used in the user management page
   */

   public function updateRole($uid,$new_group){

    $query_stmt = $this->db_connection->prepare("UPDATE users SET `group` = :new_group WHERE id = :uid");
    $query_stmt->execute(array(
                          'new_group'=>$new_group,
                          'uid'=>$uid,
                          ));


   }




}

