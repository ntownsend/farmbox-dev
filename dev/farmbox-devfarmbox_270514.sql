-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 27, 2014 at 08:57 AM
-- Server version: 5.5.35-1-log
-- PHP Version: 5.5.9-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `farmbox`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_resets`
--

DROP TABLE IF EXISTS `account_resets`;
CREATE TABLE IF NOT EXISTS `account_resets` (
  `hash` varchar(32) COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `account_resets`
--

INSERT INTO `account_resets` (`hash`, `created`, `user`) VALUES
('1d127e5346ef852dc42a38f4df9ea446', '2014-05-06 22:48:11', 27),
('38469cbd33887747e74eb7e937e2f3cf', '2014-05-06 22:43:02', 17),
('393071b14b75a2c97f1846b569637f92', '2014-05-06 22:45:50', 17),
('5634ea3d546c89276928bc595bd899fc', '2014-05-06 22:20:02', 17),
('6ae2b6ed0c359cc5d6a7132d3cec915b', '2014-05-06 22:46:45', 27),
('7dca9952d51af02aa12af895aec1f6c5', '2014-05-06 22:42:55', 17),
('98bcfc0969f61c8e65a4a8d32fa29a9b', '2014-05-06 22:15:54', 17),
('9eabf90ec73b061bc2b082987bb76766', '2014-05-06 22:18:21', 17),
('c222b6651ac2ee52334a65e75252d484', '2014-05-06 22:46:14', 17),
('d39669000f0e35c25d408436938c3863', '2014-05-06 22:16:55', 17),
('f49bd35e90c3a78e9e846cf43025b673', '2014-05-06 18:48:21', 18);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
CREATE TABLE IF NOT EXISTS `chat` (
  `timestamp` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`timestamp`, `user_id`) VALUES
(1401135977, 17),
(1401177454, 18),
(0, 19),
(0, 21),
(1401136309, 22),
(0, 23),
(0, 49),
(0, 50);

-- --------------------------------------------------------

--
-- Table structure for table `chat_messages`
--

DROP TABLE IF EXISTS `chat_messages`;
CREATE TABLE IF NOT EXISTS `chat_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_to` int(11) NOT NULL,
  `msg_from` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `message` mediumtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=255 ;

--
-- Dumping data for table `chat_messages`
--

INSERT INTO `chat_messages` (`id`, `msg_to`, `msg_from`, `timestamp`, `message`) VALUES
(201, 0, 18, 1399391697, 'imelda here'),
(202, 18, 0, 1399391706, 'Hi there imelda :-)'),
(203, 0, 18, 1399391719, 'hi there Daddy'),
(204, 18, 0, 1399391725, 'Well hello'),
(205, 0, 18, 1399391743, 'we think you\\re the best!!!!....ever!'),
(206, 18, 0, 1399391747, ':-D'),
(207, 0, 18, 1399391754, 'XXX'),
(208, 0, 18, 1399391762, 'It looks great'),
(209, 18, 0, 1399391767, '*blush*'),
(210, 0, 18, 1399391778, ' i am impressed, not that I should be'),
(211, 0, 18, 1399402926, 'ello'),
(212, 18, 0, 1399402934, 'HI Hank'),
(213, 0, 18, 1399854784, 'fdgdfg'),
(214, 18, 0, 1399854791, 'dshhf'),
(215, 24, 0, 1399854798, 'ghdgf'),
(216, 18, 0, 1400235656, 'err'),
(217, 18, 0, 1400758224, 'What happens when a really large and lengthy message gets submitted to the database? Does the whole thing come crashing down around our ears or can the system cope? Here comes some  words: Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'),
(218, 0, 18, 1400758434, 'What happens when a really large and lengthy message gets submitted to the database? Does the whole thing come crashing down around our ears or can the system cope? Here comes some  words: Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'),
(219, 18, 0, 1401056699, ''),
(220, 18, 0, 1401056711, ''),
(221, 18, 0, 1401116539, 'fsdfsdf'),
(222, 18, 0, 1401117091, 'zfzdf'),
(223, 18, 0, 1401117450, 'asdf'),
(224, 18, 0, 1401117709, 'faslidljksldfj'),
(225, 0, 18, 1401117798, 'ewfdscs'),
(226, 24, 0, 1401117809, 'to jeff'),
(227, 18, 0, 1401117820, 'asdasdasdasdasd'),
(228, 0, 18, 1401117875, 'dsadsfdsaafasdfadfsad'),
(229, 0, 18, 1401118268, 'er...... hello?'),
(230, 18, 0, 1401118279, 'Hi there hank'),
(231, 0, 18, 1401118291, 'whew. That was weird'),
(232, 18, 0, 1401120813, '`asdasdasD'),
(233, 0, 18, 1401120818, 'SDFASDFD'),
(234, 24, 0, 1401120827, 'ASDASDASDASDASDASDASD'),
(235, 0, 18, 1401123514, 'CONNECTED'),
(236, 0, 18, 1401123621, 'CONNECTED'),
(237, 0, 18, 1401123654, 'Hello there'),
(238, 18, 0, 1401123662, 'Hi there hank'),
(239, 24, 0, 1401123984, 'asdasdasdasdasdasd'),
(240, 0, 18, 1401123987, 'adfsdfadsf'),
(241, 18, 0, 1401123995, 'asdqweasdqweasdqwesad'),
(242, 0, 18, 1401124221, 'CONNECTED'),
(243, 0, 0, 1401124295, 'CONNECTED'),
(244, 0, 18, 1401124322, 'CONNECTED'),
(245, 0, 18, 1401125124, 'fsdfasd'),
(246, 0, 18, 1401125129, 'a'),
(247, 18, 0, 1401125131, '`dfsdf'),
(248, 0, 18, 1401135698, 'CONNECTED'),
(249, 0, 0, 1401135751, 'CONNECTED'),
(250, 18, 0, 1401135769, 'This message is from ADMIN'),
(251, 18, 0, 1401135931, 'This is another message from admin'),
(252, 18, 0, 1401135949, 'This is a message from the manager'),
(253, 0, 18, 1401135990, 'CONNECTED'),
(254, 18, 0, 1401136020, 'Hey hank, see you are talking to yourself again!');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text COLLATE utf8_bin NOT NULL,
  `value` varchar(1024) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `key`, `value`) VALUES
(1, 'business_name', 'Abe & Cloe.'),
(2, 'shop_location', 'Foxton, UK'),
(3, 'style', 'default'),
(4, 'strapline', 'Healthy Organic Food Delivered Directly To Your Door'),
(5, 'delivery_cost', '4.50'),
(6, 'free_delivery_threshold_price', '20.00');

-- --------------------------------------------------------

--
-- Table structure for table `contact_messages`
--

DROP TABLE IF EXISTS `contact_messages`;
CREATE TABLE IF NOT EXISTS `contact_messages` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `email` varchar(256) COLLATE utf8_bin NOT NULL,
  `phone` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `message_text` text COLLATE utf8_bin NOT NULL,
  `received` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `replied` tinyint(1) NOT NULL,
  `replied_by` int(11) NOT NULL,
  `replied_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `contact_messages`
--

INSERT INTO `contact_messages` (`id`, `name`, `email`, `phone`, `message_text`, `received`, `replied`, `replied_by`, `replied_date`) VALUES
(0, 'nick', 'nick@thejubster.co.uk', NULL, '0', '2014-05-05 16:23:39', 0, 0, '0000-00-00 00:00:00'),
(0, 'hank', 'nick@thejubster.co.uk', NULL, 'Blah blah blah blah bkag\r\n  ', '2014-05-05 16:24:36', 0, 0, '0000-00-00 00:00:00'),
(0, 'hank', 'hank@thejubster.co.uk', NULL, 'Some message that is likely pretty boring.      \r\n  ', '2014-05-06 19:18:44', 0, 0, '0000-00-00 00:00:00'),
(0, 'hank', 'hank@thejubster.co.uk', NULL, '      \r\n  ', '2014-05-11 09:36:44', 0, 0, '0000-00-00 00:00:00'),
(0, 'hank', 'hank@thejubster.co.uk', NULL, '      \r\n  ', '2014-05-11 09:37:20', 0, 0, '0000-00-00 00:00:00'),
(0, 'manager', 'manager@thejubster.co.uk', NULL, 'dasdasd', '2014-05-11 09:38:37', 0, 0, '0000-00-00 00:00:00'),
(0, 'pernille frandsen', 'pernillehbfrandsen@gmail.com', NULL, 'Awesome T ;-)', '2014-05-23 21:08:18', 0, 0, '0000-00-00 00:00:00'),
(0, 'Billybob', 'billybob@thejubster', NULL, 'Hi there,\r\n\r\nthis is a message from billy bob. Just to see how this is all working out.\r\n\r\nCheers\r\n', '2014-05-24 11:04:10', 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `postcode` varchar(10) CHARACTER SET latin1 NOT NULL,
  `address` varchar(256) CHARACTER SET latin1 NOT NULL,
  `house` varchar(50) CHARACTER SET latin1 NOT NULL,
  `county` varchar(50) CHARACTER SET latin1 NOT NULL,
  `country` varchar(50) CHARACTER SET latin1 NOT NULL,
  `telephone` varchar(20) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=36 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `postcode`, `address`, `house`, `county`, `country`, `telephone`) VALUES
(3, 17, 'OO11OO', 'Rack 15\r\nLevel 2\r\nDatacentre', '42', '', 'uk', ''),
(4, 18, 'HA12NK', 'HanksSt\r\nHanksville\r\nHankshire', 'Flat B', '', 'uk', ''),
(5, 19, 'BA85PC', 'Babs Lane,\r\nBabston', 'Babs Palace', '', 'uk', ''),
(6, 20, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(7, 21, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(8, 22, 'CA5HBAG', 'Managers Mansion,\r\nRichville\r\nDoshtown\r\n', '1', '', 'uk', ''),
(9, 23, 'CB9FN', 'Someplace,\r\nSometown\r\nSomeshire', '23', '', 'uk', ''),
(10, 24, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(11, 25, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(12, 26, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(13, 27, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(14, 28, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(15, 29, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(16, 30, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(17, 31, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(18, 32, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(19, 34, 'JM35Pc', 'James St,\r\nJamestown\r\nCounty James', '400', '', 'uk', ''),
(20, 35, '', '', '', '', '', ''),
(21, 36, 'id3q1', 'Droptable Ware\r\nNamequals\r\nUserswhere\r\n', '23', '', 'uk', ''),
(22, 37, '', '', '', '', '', ''),
(23, 38, '', '', '', '', '', ''),
(24, 39, '', '', '', '', '', ''),
(25, 40, '', '', '', '', '', ''),
(26, 41, '', '', '', '', '', ''),
(27, 42, '', '', '', '', '', ''),
(28, 43, '', '', '', '', '', ''),
(29, 44, '', '', '', '', '', ''),
(30, 45, '', '', '', '', '', ''),
(31, 46, '', '', '', '', '', ''),
(32, 47, '', '', '', '', '', ''),
(33, 48, '', '', '', '', '', ''),
(34, 49, '', '', '', '', '', ''),
(35, 50, 'cb24 3dp', 'fews lane', 'the retreat', '', 'uk', '');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_actions`
--

DROP TABLE IF EXISTS `inventory_actions`;
CREATE TABLE IF NOT EXISTS `inventory_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `email_manager` tinyint(1) NOT NULL,
  `email_supplier` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`,`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=78 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `transaction_id`, `timestamp`, `total`, `status`) VALUES
(26, 18, 0, 1399108526, 10263, 2),
(27, 18, 0, 1399798942, 6345, 2),
(28, 22, 0, 1399804076, 495, 2),
(29, 18, 0, 1399684104, 675, 2),
(30, 18, 0, 1399724135, 940, 2),
(31, 18, 0, 1399804178, 2437, 3),
(32, 18, 0, 1399815995, 2447, 3),
(33, 18, 0, 1399819628, 26201, 2),
(34, 17, 0, 1399821108, 700, 2),
(35, 18, 0, 1399822417, 6345, 2),
(36, 18, 0, 1400139841, 10263, 2),
(37, 17, 0, 1400236740, 1048, 2),
(38, 17, 0, 1400237982, 540, 2),
(39, 17, 0, 1400238169, 540, 2),
(40, 17, 0, 1400238196, 540, 2),
(41, 17, 0, 1400238310, 540, 2),
(42, 17, 0, 1400238339, 540, 2),
(43, 17, 0, 1400238376, 450, 2),
(44, 17, 0, 1400238413, 540, 2),
(45, 17, 0, 1400238498, 540, 2),
(46, 17, 0, 1400238979, 560, 2),
(47, 17, 0, 1400239121, 650, 2),
(48, 17, 0, 1400239236, 900, 2),
(49, 17, 0, 1400239357, 2557, 2),
(50, 17, 0, 1400239433, 2540, 2),
(51, 17, 0, 1400239599, 560, 2),
(52, 17, 0, 1400240424, 560, 2),
(53, 17, 0, 1400240490, 700, 2),
(54, 17, 0, 1400244766, 720, 3),
(55, 17, 0, 1400244783, 720, 2),
(56, 17, 0, 1400244890, 560, 2),
(57, 17, 0, 1400246121, 560, 1),
(58, 17, 0, 1400246671, 560, 1),
(59, 17, 0, 1400247268, 1149, 1),
(60, 17, 0, 1400247699, 1185, 1),
(61, 18, 0, 1400762350, 1500, 1),
(62, 18, 0, 1400762617, 600, 1),
(63, 18, 0, 1400764942, 1140, 1),
(64, 22, 0, 1400766414, 700, 1),
(65, 22, 0, 1400766497, 1110, 1),
(66, 36, 0, 1400787467, 1599, 2),
(70, 18, 0, 1400833245, 573, 3),
(71, 22, 0, 1400858741, 8614, 1),
(72, 22, 0, 1400862275, 1508, 2),
(73, 22, 0, 1400866375, 7066, 1),
(74, 50, 0, 1400879563, 4146, 2),
(75, 18, 0, 1401051464, 765, 1),
(76, 18, 0, 1401051637, 10263, 0),
(77, 18, 0, 1401056487, 1420, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=195 ;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `qty`, `price`, `discount_id`) VALUES
(57, 26, 1, 1, 110, 0),
(58, 26, 2, 4, 90, 0),
(59, 26, 3, 5, 560, 0),
(60, 26, 4, 3, 999, 0),
(61, 26, 5, 2, 1299, 0),
(62, 26, 11, 2, 699, 0),
(63, 27, 14, 1, 150, 0),
(64, 27, 16, 1, 250, 0),
(65, 27, 13, 13, 199, 0),
(66, 27, 9, 1, 699, 0),
(67, 27, 3, 1, 560, 0),
(68, 27, 6, 1, 160, 0),
(69, 27, 2, 1, 90, 0),
(70, 27, 8, 1, 440, 0),
(71, 27, 1, 1, 110, 0),
(72, 27, 5, 1, 1299, 0),
(73, 28, 17, 1, 45, 0),
(74, 29, 17, 5, 45, 0),
(75, 30, 17, 2, 45, 0),
(76, 30, 15, 1, 250, 0),
(77, 30, 14, 1, 150, 0),
(78, 31, 8, 1, 440, 0),
(79, 31, 9, 1, 699, 0),
(80, 31, 4, 1, 999, 0),
(81, 31, 10, 1, 299, 0),
(82, 32, 22, 2, 250, 0),
(83, 32, 21, 1, 199, 0),
(84, 32, 15, 1, 250, 0),
(85, 32, 12, 1, 199, 0),
(86, 32, 11, 1, 699, 0),
(87, 32, 8, 1, 440, 0),
(88, 32, 6, 1, 160, 0),
(89, 33, 1, 2, 110, 0),
(90, 33, 2, 9, 90, 0),
(91, 33, 3, 5, 560, 0),
(92, 33, 4, 3, 999, 0),
(93, 33, 5, 2, 1299, 0),
(94, 33, 11, 23, 699, 0),
(95, 33, 9, 1, 699, 0),
(96, 34, 22, 1, 250, 0),
(97, 35, 14, 1, 150, 0),
(98, 35, 16, 1, 250, 0),
(99, 35, 13, 13, 199, 0),
(100, 35, 9, 1, 699, 0),
(101, 35, 3, 1, 560, 0),
(102, 35, 6, 1, 160, 0),
(103, 35, 2, 1, 90, 0),
(104, 35, 8, 1, 440, 0),
(105, 35, 1, 1, 110, 0),
(106, 35, 5, 1, 1299, 0),
(107, 36, 1, 1, 110, 0),
(108, 36, 2, 4, 90, 0),
(109, 36, 3, 5, 560, 0),
(110, 36, 4, 3, 999, 0),
(111, 36, 5, 2, 1299, 0),
(112, 36, 11, 2, 699, 0),
(113, 37, 10, 2, 299, 0),
(114, 38, 2, 1, 90, 0),
(115, 39, 2, 1, 90, 0),
(116, 40, 2, 1, 90, 0),
(117, 41, 2, 1, 90, 0),
(118, 42, 2, 1, 90, 0),
(119, 44, 2, 1, 90, 0),
(120, 45, 2, 1, 90, 0),
(121, 46, 1, 1, 110, 0),
(122, 47, 1, 1, 110, 0),
(123, 47, 2, 1, 90, 0),
(124, 48, 2, 1, 90, 0),
(125, 48, 15, 1, 250, 0),
(126, 48, 1, 1, 110, 0),
(127, 49, 4, 1, 999, 0),
(128, 49, 9, 2, 699, 0),
(129, 49, 6, 1, 160, 0),
(130, 50, 1, 1, 110, 0),
(131, 50, 8, 1, 440, 0),
(132, 50, 12, 10, 199, 0),
(133, 51, 1, 1, 110, 0),
(134, 52, 1, 1, 110, 0),
(135, 53, 15, 1, 250, 0),
(136, 54, 2, 3, 90, 0),
(137, 55, 2, 3, 90, 0),
(138, 56, 1, 1, 110, 0),
(139, 57, 1, 1, 110, 0),
(140, 58, 1, 1, 110, 0),
(141, 59, 15, 1, 250, 0),
(142, 59, 14, 1, 150, 0),
(143, 59, 10, 1, 299, 0),
(144, 60, 15, 1, 250, 0),
(145, 60, 8, 1, 440, 0),
(146, 60, 17, 1, 45, 0),
(147, 61, 14, 7, 150, 0),
(148, 62, 14, 1, 150, 0),
(149, 63, 8, 1, 440, 0),
(150, 63, 15, 1, 250, 0),
(151, 64, 15, 1, 250, 0),
(152, 65, 15, 2, 250, 0),
(153, 65, 6, 1, 160, 0),
(154, 66, 14, 1, 150, 0),
(155, 66, 4, 1, 999, 0),
(156, 68, 23, 2, 123, 0),
(157, 69, 9, 1, 699, 0),
(158, 69, 20, 200, 5, 0),
(159, 70, 23, 1, 123, 0),
(160, 71, 2, 3, 90, 0),
(161, 71, 4, 2, 999, 0),
(162, 71, 16, 1, 250, 0),
(163, 71, 14, 6, 150, 0),
(164, 71, 5, 4, 1299, 0),
(165, 72, 23, 6, 123, 0),
(166, 72, 6, 2, 160, 0),
(167, 73, 15, 4, 250, 0),
(168, 73, 13, 0, 199, 0),
(169, 73, 11, 2, 699, 0),
(170, 73, 9, 1, 699, 0),
(171, 73, 7, 0, 1500, 0),
(172, 73, 5, 1, 1299, 0),
(173, 73, 3, 3, 560, 0),
(174, 73, 1, 1, 110, 0),
(175, 73, 8, 2, 440, 0),
(176, 74, 15, 3, 250, 0),
(177, 74, 13, 0, 199, 0),
(178, 74, 11, 2, 699, 0),
(179, 74, 9, 1, 699, 0),
(180, 74, 7, 0, 1500, 0),
(181, 74, 5, 1, 1299, 0),
(182, 74, 3, 0, 560, 0),
(183, 74, 1, 0, 110, 0),
(184, 74, 8, 0, 440, 0),
(185, 75, 17, 7, 45, 0),
(186, 76, 1, 1, 110, 0),
(187, 76, 2, 4, 90, 0),
(188, 76, 3, 5, 560, 0),
(189, 76, 4, 3, 999, 0),
(190, 76, 5, 2, 1299, 0),
(191, 76, 11, 2, 699, 0),
(192, 77, 2, 1, 90, 0),
(193, 77, 3, 1, 560, 0),
(194, 77, 6, 2, 160, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barcode_value` varchar(30) COLLATE utf8_bin NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `brand` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `stock_qty` int(11) DEFAULT NULL,
  `reorder_qty` int(11) NOT NULL DEFAULT '0',
  `supplier` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=24 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `barcode_value`, `name`, `description`, `brand`, `category`, `price`, `stock_qty`, `reorder_qty`, `supplier`) VALUES
(1, '', 'Abe & Cloe Stilton', 'A deliciously smooth, creamy and tangy Stilton cheese. Prepared by hand in small batches on our farm.', 0, 4, 110, -1, 20, 1),
(2, '', 'Kiwi fruit', 'Sumptous kiwi fruits, grown in poly tunnels using modern, organic farming methods.', 0, 3, 90, -3, 30, 2),
(3, '', 'Mixed fruit salad', 'A prepared fruit salad showcasing the best of the current picks from our orchards.', 0, 3, 560, -4, 20, 3),
(4, '', 'Veg box', 'A selection of seasonal vegetables from the farm.', 0, 2, 999, 4, 10, 0),
(5, '', 'Italian veg basket', 'An Italian inspired medley of vegetables.', 0, 2, 1299, 14, 10, 0),
(6, '', 'Watermelon', 'Ok, so we don''t grow these ourselves, but we have found one of the best suppliers around with a carbon neutral supply chain.', 0, 3, 160, 9, 10, 4),
(7, '', 'Stir Fry Kit', 'All the ingredients for a taste of the Orient. Contains: 4 Chicken Breasts, Red Peppers Spring Onions, Pak Choi, Garlic, Rice Noodles and one of our recipe sheets.', 0, 2, 1500, 0, 10, 2),
(8, '', 'Fajita kit', 'All the ingredients for a Mexican inspired treat for all the family. Contains: Onions, Red Peppers, Tomatoes, Fresh Coriander, Garlic and one of our recipe sheets.', 0, 2, 440, 0, 10, 0),
(9, '', 'Seasonal fruit basket', 'A selection of seasonal fruit from the Abe & Cloe orchards.', 0, 3, 699, 2, 10, 0),
(10, '', 'Carrots', 'One of our mainstays here at Abe & Cloe. The humble carrot.', 0, 1, 299, 497, 200, 0),
(11, '', 'Organic free range chicken', 'Reared here on our farm, these chickens a grown in organic certified, free range environments and are fed on a healthy mixture of corn and vegetable scraps.', 0, 7, 699, 54, 20, 0),
(12, '', 'Onions', 'The onion. A store cupboard essential.', 0, 1, 199, 990, 300, 0),
(13, '', 'Courgettes', 'A top pick from our neighbours farm. These courgettes are a great addition to a rattatouille ', 0, 1, 199, 0, 10, 1),
(14, '', 'Artichokes', 'These artichokes are grown here on our farm using certified organic soils and techniques.', 0, 1, 150, 4, 5, 0),
(15, '', 'Fresh basil', 'Fresh herbs add an extra special zing to your cooking.', 0, 6, 250, 0, 20, 0),
(16, '', 'Fresh coriander', 'Fresh herbs add an extra special zing to your cooking.', 0, 6, 250, 22, 20, 0),
(17, '', 'Apple', 'Direct from our orchards, our seasonal apple varieties are left on the trees for that little bit longer to ensure maximum taste.', 0, 3, 45, 292, 100, 0),
(18, '', 'Turkey Crown', 'This product has no image stored, though if use_google_images is set to true in config then an image will be obtained from google image search API\n', 0, 7, 899, 0, 10, 0),
(20, '', 'Potatoes', 'The humble potato, you guessed it grown in organic conditions on our farm!', 0, 1, 5, 3800, 1000, 0),
(21, '', 'Fresh Parsley', 'Re-live the 80&#039;s cooking experience and sprinkle parsley into everything.', 0, 6, 199, 300, 50, 0),
(22, '', 'Chives', 'Fresh chives - great in a potato salad or coleslaw', 0, 6, 250, 60, 10, 0),
(23, '', 'Asparagus Tips', 'Grown here on our farm these asparagus tips are delicious pan fried in butter and garlic and served with chicken or salmon.', 0, 1, 123, 71, 90, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`) VALUES
(1, 'Vegetables'),
(2, 'Vegetable Boxes'),
(3, 'Fruit'),
(4, 'Dairy'),
(5, 'Prepared Boxes'),
(6, 'Herbs'),
(7, 'Meat');

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

DROP TABLE IF EXISTS `recipes`;
CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  `serves` int(11) NOT NULL,
  `prep_time` int(11) NOT NULL,
  `cook_time` int(11) NOT NULL,
  `method` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=26 ;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `serves`, `prep_time`, `cook_time`, `method`) VALUES
(1, 'A Yummy Recipe', 6, 15, 30, 'This is where the recipe method instructions go.'),
(2, 'Chicken Fajitas', 4, 15, 20, 'Brown Chicken in a large wok or saute pan'),
(3, 'Crazy concoction', 4, 30, 45, 'Get ingredients\r\nMix in a bowl\r\nServe\r\nEat'),
(4, 'Chicken Lasanga', 4, 45, 60, 'Fry Onions + Garlic\r\nAdd chicken strips and brown\r\nAdd tinned tomatoes\r\nAdd Vegetables\r\nMake cheese sauce\r\nCombine \r\nBake'),
(12, 'Jungle Curry', 4, 20, 30, 'Curry'),
(16, 'Asparagus Soup ', 2, 10, 20, 'Cook asparagus\r\nServe'),
(17, 'Carrot Soup', 2, 10, 20, 'Cook carrots\r\nServe'),
(18, 'Onion Soup', 4, 20, 60, 'Chop and fry ingredients then add 1 litre chicken stock and simmer gently for one hour.\r\nServe with crusty bread.'),
(20, 'Chicken and Asparagus', 6, 15, 30, 'Slice Chicken into bite-sized chunks. \n\nDeep fry in 170 degree celsius oil for 8 minutes until golden. \n\nDust with paprika, set aside to cool.\n\nBlanche the asparagus and then, using a blender or mortar and pestle, blend to a smooth paste. \n\nA spoonful of horseradish sauce will add a bold extra dimension to the sauce. \n\nServe with promises of a trip to the local takeaway.'),
(21, 'Fruit And Stilton Salad', 6, 20, 5, 'Combine ingredients\r\nFry asparagus in oil with a little ginger and garlic\r\nToast some bread \r\nServe with dips'),
(22, 'Stilton Soup', 2, 5, 10, 'Cut stilton into weapon sized cubes.\n\nDrizzle with washing up liquid and then, with a blowtorch, gently sear the edges until bubbling. Avoid inhaling the fumes. \n\nWhen the dish starts to make your stomach turn, assemble your guests.'),
(23, 'Chicken Masala', 4, 20, 60, 'Clean peel and dice 3 onions, 4 cloves of garlic, a thumb sized piece of ginger and mushrooms.\r\nFry all the vegetables except the mushrooms, until soft in 4 tablespoons of Ghee/Butter/Oil.\r\nAdd 4tsp Garam Masala, 4 tsp Ground Coriander, 4tsp Ground Cumin, 2tsp Ground Turmeric, and, Chilli Powder, Salt and Pepper according to taste.\r\nFry until the spices are fragrant then add the tomatoes from one tin of whole plum tomatos (not the juice!).\r\nCut the chicken into bite sized pieces and place it along with the mushrooms into the pot.\r\nTop up with enough water to cover everything and simmer gently for one to one and a half hours, until the chicken is cooked through and the sauce thickens.\r\nServe with boiled basmati rice, breads and pickles.\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `recipe_items`
--

DROP TABLE IF EXISTS `recipe_items`;
CREATE TABLE IF NOT EXISTS `recipe_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipe_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty_required` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=142 ;

--
-- Dumping data for table `recipe_items`
--

INSERT INTO `recipe_items` (`id`, `recipe_id`, `product_id`, `qty_required`) VALUES
(1, 2, 8, 1),
(2, 2, 11, 1),
(5, 2, 16, 1),
(11, 3, 15, 4),
(12, 3, 13, 5),
(13, 3, 11, 2),
(14, 3, 9, 1),
(15, 3, 7, 4),
(16, 3, 5, 1),
(17, 3, 3, 3),
(18, 3, 1, 2),
(19, 3, 1, 1),
(20, 3, 8, 2),
(21, 4, 1, 1),
(22, 4, 5, 1),
(23, 4, 11, 1),
(24, 4, 15, 1),
(70, 12, 23, 1),
(72, 12, 21, 1),
(73, 12, 20, 1),
(75, 12, 17, 1),
(76, 12, 16, 1),
(80, 12, 12, 1),
(81, 12, 11, 1),
(92, 16, 23, 20),
(93, 17, 10, 10),
(94, 18, 22, 1),
(95, 18, 17, 1),
(96, 18, 15, 1),
(97, 18, 12, 6),
(98, 19, 22, 1),
(99, 19, 17, 1),
(100, 19, 15, 1),
(101, 19, 12, 6),
(102, 20, 23, 6),
(103, 20, 6, 2),
(104, 21, 23, 1),
(105, 21, 22, 1),
(106, 21, 21, 1),
(107, 21, 17, 1),
(108, 21, 15, 1),
(109, 21, 12, 1),
(110, 21, 1, 1),
(111, 22, 1, 6),
(125, 1, 23, 12),
(126, 1, 13, 5),
(127, 1, 6, 4),
(128, 1, 4, 1),
(139, 23, 16, 1),
(140, 23, 12, 4),
(141, 23, 11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `saved_baskets`
--

DROP TABLE IF EXISTS `saved_baskets`;
CREATE TABLE IF NOT EXISTS `saved_baskets` (
  `id` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `saved_baskets`
--

INSERT INTO `saved_baskets` (`id`, `user_id`, `name`) VALUES
('18x535c46797e1af6.13336200', 18, 'hanks small feast'),
('18x535d06f198d000.68097512', 18, 'Girls Night In'),
('18x535d0733685012.31655062', 18, 'lot&#039;s of food'),
('18x535d1174055b85.36892171', 18, 'FRUIT!'),
('18x535d934eea68f', 18, ''),
('18x536e5bcd12bbd', 18, 'Sooo much fooood'),
('18x536e6c3a8e46c', 18, ''),
('19x535d93580ed1c', 19, ''),
('19x535d941a9b5be', 19, ''),
('19x535d97d74c030', 19, ''),
('19x535da3fd7a583', 19, '');

-- --------------------------------------------------------

--
-- Table structure for table `saved_basket_items`
--

DROP TABLE IF EXISTS `saved_basket_items`;
CREATE TABLE IF NOT EXISTS `saved_basket_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=74 ;

--
-- Dumping data for table `saved_basket_items`
--

INSERT INTO `saved_basket_items` (`id`, `list_id`, `product_id`, `qty`) VALUES
(19, '18x535c46797e1af6.13336200', 1, 1),
(20, '18x535c46797e1af6.13336200', 2, 4),
(21, '18x535c46797e1af6.13336200', 3, 5),
(22, '18x535c46797e1af6.13336200', 4, 3),
(23, '18x535c46797e1af6.13336200', 5, 2),
(24, '18x535c46797e1af6.13336200', 11, 2),
(32, '18x535d06f198d000.68097512', 1, 2),
(33, '18x535d06f198d000.68097512', 2, 9),
(34, '18x535d06f198d000.68097512', 3, 5),
(35, '18x535d06f198d000.68097512', 4, 3),
(36, '18x535d06f198d000.68097512', 5, 2),
(37, '18x535d06f198d000.68097512', 11, 23),
(38, '18x535d06f198d000.68097512', 9, 1),
(39, '18x535d0733685012.31655062', 14, 1),
(40, '18x535d0733685012.31655062', 16, 1),
(41, '18x535d0733685012.31655062', 13, 13),
(42, '18x535d0733685012.31655062', 9, 1),
(43, '18x535d0733685012.31655062', 3, 1),
(44, '18x535d0733685012.31655062', 6, 1),
(45, '18x535d0733685012.31655062', 2, 1),
(46, '18x535d0733685012.31655062', 8, 1),
(47, '18x535d0733685012.31655062', 1, 1),
(48, '18x535d0733685012.31655062', 5, 1),
(49, '18x535d1174055b85.36892171', 2, 1),
(50, '18x535d1174055b85.36892171', 3, 1),
(51, '18x535d1174055b85.36892171', 6, 2),
(52, '18x535d934eea68f', 3, 3),
(53, '19x535d93580ed1c', 1, 5),
(54, '19x535d93580ed1c', 6, 3),
(55, '19x535d93580ed1c', 7, 4),
(56, '19x535d941a9b5be', 7, 8),
(57, '19x535d941a9b5be', 10, 3),
(58, '19x535d97d74c030', 1, 8),
(59, '19x535d97d74c030', 6, 4),
(60, '19x535d97d74c030', 11, 5),
(61, '19x535da3fd7a583', 1, 10),
(62, '19x535da3fd7a583', 2, 16),
(63, '18x536e5bcd12bbd', 1, 10),
(64, '18x536e5bcd12bbd', 2, 12),
(65, '18x536e5bcd12bbd', 3, 5),
(66, '18x536e5bcd12bbd', 4, 11),
(67, '18x536e5bcd12bbd', 5, 12),
(68, '18x536e5bcd12bbd', 11, 12),
(69, '18x536e6c3a8e46c', 1, 2),
(70, '18x536e6c3a8e46c', 2, 4),
(71, '18x536e6c3a8e46c', 3, 5),
(72, '18x536e6c3a8e46c', 4, 5),
(73, '18x536e6c3a8e46c', 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `email` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` tinytext COLLATE utf8_bin NOT NULL,
  `password` varchar(5012) COLLATE utf8_bin NOT NULL,
  `email` varchar(400) COLLATE utf8_bin NOT NULL,
  `last_authenticated` int(11) NOT NULL,
  `last_active` int(11) NOT NULL,
  `group` int(11) NOT NULL,
  `joined` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=51 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `last_authenticated`, `last_active`, `group`, `joined`) VALUES
(17, 'admin', '$2y$10$YmmgLqqnxpom/VNevfdt8OcovbvX1gNIjvJPqPiP/9xVkWX9K.aby', 'nick@thejubster.co.uk', 1398353026, 1401135979, 5, 0),
(18, 'hank', '$2y$10$szZNEMaD9Ob4i9nWSflDR.Jgr/TmFjs0vQ6bfPh3MA0TpbjlaymIu', 'hank@thejubster.co.uk', 1398372320, 1401177453, 0, 0),
(19, 'babs', '$2y$10$6EJ5ksluYhgmNHoWzXOo2uU8/7Fb3Xsqsw/nvkFI7dSgF.qulvw.2', 'babs2@thejubster.co.uk', 1399819079, 1400243993, 2, 0),
(21, 'jock', '$2y$10$/12wjngRCpJaSfRdgiDz1evdatgYYNS2/3UU0e0.LC6bxIzrGxEju', 'jock@thejubster.co.uk', 1398556588, 0, 3, 0),
(22, 'manager', '$2y$10$QcTzHjpuC/8quIVivCHzE.ST6/qrJ.EzCs2n9W5HX3GdV2RNCexq2', 'manager@thejubster.co.uk', 1398787335, 1401137608, 4, 0),
(23, 'orderpicker', '$2y$10$oDqhFw9V3fb6AoPNvyieFuouK89HuC5i6ux.VdJurmUp1VRSD8gLC', 'orderpicker1@thejubster.co.uk', 0, 1400775471, 1, 0),
(49, 'will', '$2y$10$5wz7roburuq5paM.r3HtpukwTkJzIO6/hY5f.kzafhqbllR6O3uSq', 'will@thejubster.co.uk', 1400875228, 0, 0, 1400875203),
(50, 'pernille frandsen', '$2y$10$O2P8S9lTTDyQ7sFFV4SrJO.z2R4ZKggtTStvXAdV2OiurpPSkAuC2', 'pernillehbfrandsen@gmail.com', 0, 1400879678, 0, 1400879405);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`) VALUES
(0, 'Customer'),
(1, 'Order Picker'),
(2, 'Administrator'),
(3, 'Accounts'),
(4, 'Manager'),
(5, 'Site Admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_members`
--

DROP TABLE IF EXISTS `user_group_members`;
CREATE TABLE IF NOT EXISTS `user_group_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user_group_members`
--

INSERT INTO `user_group_members` (`id`, `group_id`, `user_id`, `added_on`) VALUES
(1, 1, 17, '2014-04-28 01:44:03'),
(2, 6, 18, '2014-04-28 01:44:36'),
(4, 6, 19, '2014-04-28 01:45:16'),
(5, 6, 20, '2014-04-28 01:45:16'),
(6, 6, 21, '2014-04-28 01:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

DROP TABLE IF EXISTS `verifications`;
CREATE TABLE IF NOT EXISTS `verifications` (
  `hash` varchar(32) COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `verifications`
--

INSERT INTO `verifications` (`hash`, `created`, `user`) VALUES
('1871f541f8348bf58f8981f21f867972', '2014-05-23 21:10:55', 50);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
