<?php

  // Insist on SSL connection
  make_connection_secure();

  $login_error ='';

  // Process submitted values
  
  if(!empty($_POST)){

    if( isset($_POST['username']) && isset($_POST['password']) && !empty($_POST['username']) && !empty($_POST['password'])){

      $username = sanitise($_POST['username']);
      $password = sanitise($_POST['password']);

      $login =  $user->Authenticate($username,$password);

      if($login!=FALSE){

        $_SESSION['uid'] = $login;

        // TODO allow user to specify landing page in config
      if(isset($userprefs)){
        page_redirect($userprefs['default_page']);
      }
      else{
        // Redirect to teh account page for now
        page_redirect('account');
        }
      }


    }

  // Getting this far means that one of the error conditions we test for above has been detected. As this is the login form we don't
  // want to be giving too much away as to what went wrong so this one error message fits all shape and size of error

  $login_error = "<div class='login_feedback'>".
                    $lang['login']['login_failed']
                ."</div>";

  }
