<?php
  
// Set the user level for this page.

$user->authorise(0);

  // We set this when we are viewing the user record page as an admin user
  
  $user_lookup = (!isset($user_lookup) ? '' : $user_lookup);

  /* Grab all the orders associated with the user set above or the currently logged in user by default
  *  then call the method to build html tables wrapped in divs, containing all previous orders ready for display ($tdata)
  */

  $past_orders = $orders->getOrderHistory($user_lookup);
  
  $tdata = $orders->orderHistoryTable($past_orders);

  $num_of_orders = count($past_orders);


