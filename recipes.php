<?php
  
  $recipes = $product->getAllRecipes();
  
  
  $html='<h1>Recipes</h1>';
  $html .="<p class='page_top'>This selection of recipes delicious recipes submitted by our staff, can all be made using ingredients you can buy from us. How convenient!<br><br >
          To make it even easier, you can quickly add all of the ingredients to your basket with just one click. 
          </p>";
  
  
  if(isset($_GET[''])){
  
    
  
  //Display specific recipe
  
    if(isset($_GET['recipe'])){
  
  
  
    }
  
  // Display all recipes with specified ingredient


    if(isset($_GET['in'])){


      if(ctype_digit($_GET['in'])){

        $recipes = $product->findInRecipes($_POST['in']);

        printR($recipes);




      }else{

        // not a number

      }
  
  
    }
  


  
  }
  else{
    
  
  
  
  // Dispaly ALL recipes
    
    foreach($recipes as $key => $recipe){
    
      $ingredients = $product->findFromRecipes($recipe['id']);
    
      $list ='';
      
      foreach($ingredients as $key => $ingredient){
    
        $ingredient_info = $product->lookup($ingredient['product_id']);

          extract($ingredient);
          extract($ingredient_info);


          $price = calculatePrice($price);
    
        $list .="<div class='field'><label class='left' for='$product_id'>$name - ($price ea)</label>
                  <input type='number' name='basket_items[$product_id]' value='$qty_required'> </div>";
      }

      // Put the method into an unordered list, replacing new lines with <li>
      
      $recipe_description ='<div><ul><li>';
      $recipe_description .= str_replace(["\n"],['</li><li>'],$recipe['method']);
      $recipe_description .= '</li></ul></div>';
      
      $recipe_image = $product->getImage($recipe['id'],$recipe['name'],false);
      $recipe_name = $recipe['name'];
      $html .="
    
      <div class='product'>
        <form action='./?p=basket' method='post'>
        <h3><i class='fa fa-file-text-o'></i>
        ". $recipe['name']." <b class='number_spot' title='This recipe requires ". count($ingredients)
        ." ingredients that be purchased directly from us today'>". count($ingredients) ."</b></h3>
        
                $recipe_image
      
        <h4 style='float:left'><i class='fa fa-child fa-fw'></i> Serves: ".$recipe['serves']."</h4>
        <h5 style='float:left'><i class='fa fa-spoon fa-fw'></i> Prep: ".$recipe['prep_time']." mins</h5>
        <h5 style='float:left'><i class='fa fa-clock-o fa-fw'></i> Cook: ".$recipe['cook_time']." mins</h5>

        <p>Recipe requires ". count($ingredients) ." products available to buy today</p>

        <div class='folded'>
          <p><i class='fa fa-chevron-down'></i> Recipe Details</p>
          <hr>
          <i class='fa fa-pencil'></i> Method:
          $recipe_description
          <hr>
          <i class='fa fa-pencil'></i> Ingredients
          <hr>
          $list
         <div class='field right'>
            <button type='submit' name='a' value='update_basket'><i class='fa fa-plus'></i> Add To Basket</button>
          </div>
        </div>
        
   
      </form>
      </div>
    
    
      ";
    
      
  
    }
  
  }
  
