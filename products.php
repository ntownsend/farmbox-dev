<?php
  
  
  // These are the sort colums we will accept, to prevent additional ones
  // being passed by modifying the values sent in the $_GET request
  // this is important as we are building these int the prepared pdo statement in User.inc
  // as PDO doesn't play nicely when you try and bind params on the ORDER BY condition
  // TODO - IS the above really necessary - wil bindParams not do this now we know how to actually use it??s

  $order_by = array('name','category','price');



  // include the view_choices bar..
  
  include"./view_choices.php";



  // ... so that we can select from the db based on the choices made
  
  $product_list = $product->getAllFromCategory($selected_category,$sort_by,$sort_direction,$start_row,$row_limit);
 

  // Gather info about what is already stored in the shopping basket so that the
  // available stock can be calculated 
  
  $product_ex ='';
  foreach($product_list as $key => $product_info){

    $inbasket = 0;
    $inbasketselect='';
    
    if(!empty($_SESSION['basket_items'])){

      if(array_key_exists($product_info['id'],$_SESSION['basket_items'])){
  
          $p = $product_info['id'];
          
        // How many of this product are already in the basket (if any)
        $inbasket = $_SESSION['basket_items']["$p"];
        $inbasketselect = 'bordered';
        
        
        
      }
    }

    // Check to see whether the item is in stock
    // disable the form element if this is the case


    if($product_info['stock_qty']-$inbasket>0){
	  $stock = $product_info['stock_qty'] - $inbasket;
          $instock = "<u class='fa fa-check-o green'>".$stock." In Stock</u>" ;
          $disabled = '';
    }
    else{ 
      $stock=0;
      $instock = "<u class='fa fa-times red'>Out of stock</u>" ;
      $disabled = 'disabled';

    }

    // Search the recipes database to see if this product is featured in any recipes

    $inrecipes = $product->findInRecipes($product_info['id']);

      $no_of_recipes = count($inrecipes);

      $recipe_link = "<a href='./?p=recipes&amp;in=". $product_info['id'] ."' class='small'><i class='fa fa-cutlery big'></i><b><br> $no_of_recipes ";
      $recipe_link .= ($no_of_recipes>1 ? 'recipes' : 'recipe' );
      $recipe_link .= "</a></b>";

      $recipe_link = ($no_of_recipes==0 ? '' : $recipe_link);
    
    // Create the content based how many of this item are already in the basket
     
    $inbasketcount = ($inbasket!=0 ? "<b><u class='basket_totals'>".$inbasket ."</u></b>in your basket<br>" : '') ."$instock";
    $inbasketlogo = ($inbasket!=0 ? "<h6><i class='fa fa-check-square-o fa-2x '></i></h6>" : '');

    $img_url = $product->getImage($product_info['id'],$product_info['name']);


    // Produce the html for each item returned.
    // This is the GRID layout version
    // TODO - add the list layout version



    // Generate the html for the item's display in the template

    $product_ex .= "
    <div class='product $inbasketselect'>
      <form action='./?p=products' method='post'>
      <h3><i class='fa fa-cube'></i>
      ".
      ucwords($product_info['name'])
      ."</h3>$inbasketlogo
      
      $img_url
      <h4 >$inbasketcount</h4>
      <h5 class='price'>£".calculatePrice($product_info['price'])." ea</h5>
      <input type='hidden' name='product_code' value='".$product_info['id']."' >
      <input type='number' min='0' max='$stock' name='qty' value='1' $disabled>
      <button type='submit' class='small' name='a' value='add_to_basket'  $disabled><i class='fa fa-plus'></i> </button>
      <p>". $product_info['description'] ."</p>
      <div class='icon_bar'>
      $recipe_link
      
      </div>
    </div>
    </form>
    ";
  
  };
