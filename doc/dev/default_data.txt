## Users / Customers


Admin - Store Owner
Chris - Store Worker
Wendy - Store Worker
Paula - Receptionist
Madeline - Store Accounts


Alice
Bob
Charles
David
Ellen
Frank
Gina
Harry
Ian
Jane

## Recipes

:soups
Carrot and Coriander Soup
:starters
Stuffed Potato Skins
:mains
Potato Gratin

Roast Vegetable Platter

:family_Favourites
Mac and Cauliflower Cheese

: World
Vegetable Masala
Kung Po Vegetable

:Deserts
Apple and Something Surprise


## Products

Carrots
Apples
Potatoes
Cauliflower
Spinach
Asparagus
Parsnips
Cabbage
Kale
Garlic
Onions
Shallots

Honey
Fruit Juice
Meats
  Gammon
  Bacon
  Sausages

  Beef Joints
  Steaks
  Burgers

Eggs
Milk

Breads

## Boxes

1: Small
      500g Pots  
      500g Carrots
      250g Onions
      1 bulb Garlic
      500g Cabbage/Spinach/Kale
      1/2 Cucmber
      150g Tomatoes

2. Medium
      1kg Pots  
      750g Carrots
      400g Onions
      3 bulb Garlic
      750g Cabbage/Spinach/Kale
      300g Tomatoes
      1 Cucumber


3. Large
      1.5kg Pots  
      750g Carrots
      600g Onions
      5 bulb Garlic
      750g x2 From choice of: Cabbage/Spinach/Kale
      300g Tomatoes
      1 Cucumber
      200g
      1 Litre Apple Juice
      
4. Family

      1.5kg Pots  
      750g Carrots
      600g Onions
      5 bulb Garlic
      750g x2 From choice of: Cabbage/Spinach/Kale
      300g Tomatoes
      1 Cucumber
      200g
      1 Litre Apple Juice
      2 Litres Milk
      1 Artisan Loaf



## Customer records

Chats
  Alice
  23/1/2014
  Issue: Suspend repeat order for two weeks
  Agent:
