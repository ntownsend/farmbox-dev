<?php

/********************************************************************************************** 
 * Login page 
 */


$lang['login'] = array(

    'username'=> 'Brugernavn',
    'password' => 'Adgangskode',
    'signin_message' => 'Log ind',
    'signup_message' => 'Registrere',
    'forgot_password' => 'Glemt adgangskode',
    'forgot_username' => 'Glemt brugernavn',
    'button_action' => 'Log Ind',
    'remember_me' => 'Husk mig'
    
);


$lang['membership'] = array(

  'not_a_member_yet' => 'Ikke medlem endnu?',
  'not_a_member_yet_message' => 'Vær med os, er gratis, og som en velkommen behandler du kan have 50% rabat på din første ordre hjælp af kode VegBox <code>FIRST50</code> når du '

);


$lang['navigation'] = array(
    'products'=> 'Produkter',
    'products_tooltip' => 'Gennemse vores sortiment af produkter',
    'brands' => 'Brands',
    'brands_tooltip' => 'Gennemse producenterne vi materiel',
    'recipes' => 'Opskrifter',
    'recipes_tooltip' => 'Find lækre opskrifter til at gøre brug af vores produkter',
    'contact_us' => 'Kontakt os',
    'contact_us_tooltip' => 'Kontakt os',
    'live_chat' => 'live chat',
    'live_chat_tooltip' => 'Har du et spørgsmål? - Få på umiddelbare reaktion ved hjælp af vores Live Chat-'

);





/********************************************************************************************** 
* Shopping cart
*/

$lang['cart']['view_cart']='Se din indkøbskurv';
$lang['cart']['checkout']='Fortsæt til kassen';

