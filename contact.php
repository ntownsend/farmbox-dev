<?php

  $error =array();

  // Check for empty form values and respond


  if(!empty($_POST)){
    switch (true){
  
      case (!isset($_POST['name'])):
        $error['name']['err'] = 'No value entered';        
      
      case (!isset($_POST['email'])):
        $error['email']['err'] = 'No value entered';
  
      case (empty($_POST['message'])):
        $error['message']['err'] = 'No message entered';
  
      }


  // If no errors about empty values then continue processing the values submitted.
  // or skip to the form and inform the user as to what happeed.

  if(empty($error)){

    // sanitise form values to strip out anything nasty

    $name = sanitise($_POST['name']);
    $email = sanitise($_POST['email']);
    $message_text = sanitise($_POST['message']);
    $phone = (!empty($_POST['phone']) ? $_POST['phone'] : null);

    // Filter and validate the email address
    // It's in the users interest to provide accurate info
    // if they expect a response though!
    
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
    
      $error['email']['err'] = $lang['register']['email_error'];
      $error['email']['val'] = $email;

    }

    // check tel - this is tricky for telephone numbers as different countries and areas have different formats
    // our assumption here is going to be that as this is a contact form then it is in the customers interests to
    // fill in their actual telephone number. We check the email address for validity so letting this value pass
    // unchecked, albeit sanitised for the db, should be ok in 99% of use cases.

    $message->add($name,$email,$phone,$message_text);

    // An error above would trigger or main exception handler so we can set the action on success here unconditionally

    display_feedback("Thanks, your message has been recieved. We do our best to respnd within 24 hours, Mon-Sat",
                     'Message submitted',
                     'products',
                     'notify');


  }

}


  // Prepare elements for the template


  
  $name_message     = (isset($error['name']['err'])      ? $error['name']['err']     : $lang['contact']['name_guidance'] );
  $email_message    = (isset($error['email']['err'])     ? $error['email']['err']    : $lang['contact']['email_guidance']);
  $phone_message    = (isset($error['phone']['err'])     ? $error['phone']['err']    : $lang['contact']['phone_guidance']);
  $message_message  = (isset($error['message']['err'])   ? $error['message']['err']  : $lang['contact']['message_guidance']);
  

    // Display errors re no values to user


  $name_class     = (isset($error['name'])            ? 'input_error'             : '' );
  $email_class    = (isset($error['email'])           ? 'input_error'             : '' );
  $message_class  = (isset($error['message'])         ? 'input_error'             : '' );

  $name_val       = (isset($error['name']['val'])     ? $error['name']['val']     : '' );
  $email_val      = (isset($error['email']['val'])    ? $error['email']['val']    : '' );
  $message_val    = (isset($error['message']['val'])  ? $error['message']['val']  : '' );


  // If a user is logged in then replace the fields with disabled elements prepopulated with their info and hidden form elements
  // so that the information is submitted with the form (disabled elements are not submitted!)
    
  $name_val = (isset($userinfo) ? $userinfo['username'] : $name_val);
  $email_val = (isset($userinfo) ? $userinfo['email'] : $email_val);

  $name_disabled = (isset($userinfo) ? 'disabled' : '');
  
  $name_alt = (!empty($name_disabled) ? "<input type='hidden' name='name' value='". $userinfo['username'] ."' />" : '');
  $email_alt = (!empty($name_disabled) ? "<input type='hidden' name='email' value='". $userinfo['email'] ."' />" : '');
  

