<?php

// Check that we are logged in
  
  if(isset($_SESSION['uid'])){
  
    require_once('./inc/Class.Chat.inc.php');
  
    $chat = new Chat($databaseOb);
  
  
    // If we are a customer then check to see if a member of staff is connected to chat.
      
    if($userinfo['group']==0){
    $connected_staff = $chat->chatConnected($staff=true);
  
      // if staff connected to chat
      if(count($connected_staff)>0){
        // do chat
  
       
  
      }
     else{
  
        display_feedback("There isn't a member of staff connected to chat at the moment.
                              This service is only available during shop opening hours which are 8am-7pm 7 days a week. Try again in a minute",
                            'Chat currently unnavailabe',
                            'contact',
                            'notify'
                          );
  
      exit;
      
      }
  
    }
  
  
    // Set things up for an admin user
  
    if(!$userinfo['group']==0){

    
      $conns = $user->listCustomers();
      $replying_to = '';
      foreach ($conns as $key => $user){
        
          // This is kludgy.
          // We build a list of all users and put them in a drop down
          // TODO - This is appalling, needs to be improved. Will likely fail :-(
      
          $replying_to .= "<option value='". $user['id']."'>". $user['username']."</option> ";
       
          // Get list of connected customers, build a list of them and allow to choose which one we are replying to
          //
      }
      
  
  
        
    }
  
  
    $html = "
      <div id='chat_window' class='chat_window'>
    
        <a name='message1'></a>
    
      </div>
    
    
    
      
    
      <div class='field chat_message'>
      <form action=''>
      
    ";
  
    
    if($userinfo['group']!=0){
  
      $html .= "
      <div class'field'>
      <label for='replying_to'>Replying to </label>
        <select id='replying_to' name='replying_to'>

          $replying_to
        </select>
        </div>
        ";
    }else
    {
      $html .= "<input id='replying_to' type='hidden' name='replying_to' value='0' />"; 
    }
    
       
  
    $html .= "
          <input autofocus id='message' type='text' name='message' placeholder='Type your message here...' value=''/>
          <input id='send_message' name='send_message' type='submit' value='Send Message'/>
    <!--
          <button id='send_message' name='send_message' type='submit'><i class='fa fa-chevron-right'></i>Send Message</button>
    -->
        </form>
      </div>
    ";
  
  
  }else
  {
    display_feedback("Live chat is only available to registered account holders. You'll need to create a FREE account if you don't have one, or sign in if you do'",'Sign in required','login');


    $html = "<p> Live chat is only available to registered account holders. You can create an account for FREE right now</p>";
    $page = 'login';
  }

