<?php

  if(!isset($_SESSION['uid'])){

    page_redirect('login');

    }



  if(isset($_POST['a'])){

    switch($_POST['a']){

   // Saved basket actions

   

      case 'restore_basket':
        $shopping_basket->restore($_POST['basket_id']);
        $page='savedbaskets';
        break;
        
      case 'rename':
        $shopping_basket->rename($_POST['basket_id'],$_POST['basket_name']);
        $page='savedbaskets';
        break;


      case 'delete_basket':
      $shopping_basket->delete($_POST['basket_id']);
      $page='savedbaskets';
    


    }


  }

  // Retrieve list of saved baskets
  // 

  $baskets = $shopping_basket->listSavedBaskets();
  
  // For each of the saved baskets extract the items for that
  // basket id from the saved_list_items table
  
  $product_data='';


  foreach($baskets as $key => $val){

    $basket_items = $shopping_basket->listSavedItems($val['id']);


    $basket_total = 0;
  

    // For each basket we will need a table
    // TODO - Two actions based on same var - make this an if statement
    $basket_name = (!empty($val['name']) ? $val['name'] : $val['id']  );
    $cur_basket_name = (!empty($val['name']) ? $val['name'] : ''  );
    $basket_totals = $shopping_basket->savedBasketTotals($val['id']);
    $product_data .= "<div class='butt_after float_left'>
    <h2><i class='fa fa-shopping-cart'></i> $basket_name</h2>
    <h6 class='normal'>".$basket_totals['count']." items ".$basket_totals['calculated_total']."</h6>
    <table class='oddeven'><tr>

      <th colspan='6'>
        Saved Basket: ". $basket_name ." 
      </th>

    </tr>";

    // Iterate over each of the items in the basket
    // retrieving pertinent info

    
    foreach($basket_items as $value){
       
        $product_description = $product->lookup($value['product_id']);

        // price is stored as a integer in the base currency o the site.
        // TODO - move this to a funcion somewhere tp alow for conversion
        
        $product_price = number_format($product_description['price']/100,2);
        
        $row_total = number_format( ($product_price * $value['qty']),2);
        $basket_total = $basket_total + $row_total;
       
        $product_data .= "
          <tr>
            <td>".
              $value['product_id']
              ."
              </td>
              <td>".
              $product_description['name'].
              "
              </td>
              <td>$product_price<td>
              <td>
              " .

              $value['qty']

              ."</td><td>$row_total</td></tr>";

    };

    $product_data .=  "
    <tr>
      <td colspan='5'>Basket Total (ex Delivery) </td>
      <td>$basket_total</td>
    </tr>
    <tr><td colspan='6' class='right'>
    <form action='./?p=savedbaskets' method='post'>
    <input type='hidden' name='basket_id' value = '".$val['id']."' />
      <input type='text' name='basket_name' placeholder='Give this basket a name...'  value='$cur_basket_name'>
      <button class='small' name='a' value='rename' type='submit'><i class='fa fa-refresh'></i> Rename</button>
      </td></tr>
    <tr><td colspan='6' class='right'>
      
      <button type='submit' name='a' value='delete_basket' class='red_button'><i class='fa fa-trash-o'></i> Delete Basket</button>
      <button type='submit' name='a' value='restore_basket' ><i class='fa fa-cloud-upload'></i> Restore Basket</button>
      
      </form>
    </table></div>";

    $basket_total = number_format($basket_total,2);

  }
 
