CODING PRACTISE

# Case

Classnames:   ExampleClassName
Functions:    sendsDataSomewhere
Class_vars:   $_my_class_var
Global vars   $my_global_var

# Comments

Class names, function names, large descriptive blocks


Development comment toggle indicator:   //~~

Todo tag   # TODO: some thing todo, multiple lines need multiple opening tags
