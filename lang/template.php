<?php

/********************************************************************************************** 
 * Login page 
 */

$lang['login']['login_message']='Please login';
$lang['login']['forgot_password']='Forgotten Password';
$lang['login']['forgot_username']='Forgotten Username';



/********************************************************************************************** 
* Shopping cart
*/

$lang['cart']['view_cart']='View your basket';
$lang['cart']['checkout']='Proceed to checkout';

