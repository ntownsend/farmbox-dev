<?php

/********************************************************************************************** 
 * Login page 
 */



$lang['login'] = array(

    'username' => 'Username',
    'password' => 'Password',
    'logout' => 'Logout',
    'signin_message'=> 'Sign In',
    'signup_message' => 'Sign Up',
    'reset_login' => 'I have forgotten my username/password',
    'button_action' => 'Sign In',
    'remember_me' => 'Remember me',
    'login_failed' => 'There was a problem logging you in.<br ><br> Please try again or use the link below to reset your account.'
);


$lang['contact'] = array(


    'email_guidance' => 'Enter the email address you would like us to use to communicate with you. ',
    'name_guidance' => 'Enter your name so we can address you properly when responding',
    'message_guidance' => 'Enter your message here',
    'phone_guidance' => 'If you supply your phone number here, one of our team will call you back to discuss your enquiry.'



  );


$lang['register'] = array(
    'username' => 'Choose your username',
    'password' => 'Choose your password',
    'password_confirm' => 'Confirm password',
    'password_strength' => 'Password strength',
    'password_strengths' => [ 'Fail','Weak','Average','Good','Strong'],
    'email_address' => 'Preferred email address',
    'billing_address' => 'Billing address',
    'house_no' => 'House name/no',
    'postcode' => 'Postcode',
    'street' => 'Street',
    'town' => 'Town',
    'county' => 'County',
    'country' => 'Country',
    'agree_to_terms' => 'I agree to the terms and conditions',
    'username_error' => 'That username isn\'t available',
    'value_accepted' => 'That\'s great, exactly what we needed!',
    'username_guidance' => 'Choose a username '.  $conf['auth']['username_policy']['min_characters'] .' to '. $conf['auth']['username_policy']['max_characters'] .' characters long. It should contain only numbers, letters, and underscores.',
    'email_error' => 'There was a problem with the email address entered, please check and try again',
    'email_guidance' =>  $lang['contact']['email_guidance'] . 'A verification email will be sent to this address to complete your signup',
    'password_guidance' => 'Password must be between '. $conf['auth']['password_policy']['min_characters'] .' and '. $conf['auth']['password_policy']['max_characters'] .' characters long',
    'password_confirm' => 'Enter your chosen password again',
    'password_error' => 'Password was not accepted, please try again',
    //'email_body' => 'Hi '. $register['username'] .' To complete your registration with '. $conf['core']['company_name'] .' all you have to do is click on the link below to confirm your registration. ##EMAIL_LINK## If the link doesn\'t work, try copying and pasting into a broweser window.<br> <br>Cheers<br><br> The guys at '. $conf['core']['company_name'],
    'email_sent' => 'Registration is almost complete!<br><br>A confirmation email has been sent to <b>##EMAIL##</b> . <br> Check your email and follow the link in the email to complete your registration.',
    'email_sent_title' => 'Almost done!',
    'email_error_exists' => 'That address can\'t be used',
);


$lang['forms']['account_reset']['form_title'] = 'Reset your account';
$lang['forms']['account_reset']['email_address'] ='Enter your email address';
$lang['membership'] = array(

  'not_a_member_yet' => 'Not a member yet?',
  'not_a_member_yet_message' => 'Joining us is FREE, and as a welcome treat you can have 50% off your first VegBox order using code <code>FIRST50</code> when you place your first order!'

);

$lang['navigation'] = array(
    'products' => 'Products',
    'products_tooltip' => 'Browse our range of products',
    'brands' => 'Brands',
    'brands_tooltip' => 'Browse the manufacturers we stock',
    'recipes' => 'Recipes',
    'recipes_tooltip' => 'Find tasty recipes to make using our products',
    'findus' => 'Visit Us',
    'findus_tooltip' => 'Get directions to our FarmShop where all of our produce is available',
    'contact_us' => 'Contact Us',
    'contact_us_tooltip' => 'Contact Us',
    'live_chat'  => 'Live Chat',
    'live_chat_tooltip'  => 'Have a question? - get an immediate response by using our Live Chat'

);

/********************************************************************************************** 
* Shopping cart
*/

$lang['cart']['view_cart']='View your basket';
$lang['cart']['checkout']='Proceed to checkout';
 
$lang['error']['db_connect'] = "Database connection error";
