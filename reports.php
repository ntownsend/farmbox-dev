<?php  
// Set the user level for this page.

$user->authorise(3);

$logged_in_users = $user->getActiveUsers();

$num_users = count($logged_in_users)-1;

$reg_users = $user->numOfUsers();
$unver_users = $user->numOfUsers(true);
$customers = $user->numOfUsers(false,true);


// RECIPES

  $recipes = $product->getAllRecipes();
  $num_recipes = count($recipes);
  
// Products

  $total_products = $product->countOf(false);
  $instock_products = $product->countOf(true);

// Orders

  $total_orders = $orders->totalOrders();
  $pending_orders = count($orders->getPackableOrders());
  $packed_orders = count($orders->getPackedOrders());
  $sent_orders = count($orders->getSentOrders());

// Messages

   $messages_total = $message->total();
   $new_messages_total = count($message->awaitingResponse());

// These are pretty approximate, but close enough

  $now = date('U');

  $one_day = intval(24*60*60);

  $one_week = $one_day*7;

  $one_month = $one_day*30;

  $one_year = $one_day*365;

// Day totals


  $day_ago = $now-$one_day;

  $day_total = $orders->getOrders($day_ago,$now);

  $yesterday = $now-$one_day;

  $yesterdays_yesterday = $yesterday-$one_day;

  $yesterdays_total = $orders->getOrders($yesterdays_yesterday,$yesterday);

  $week_ago = $now-$one_week;

  $week_total = $orders->getOrders($week_ago,$now);


  $two_weeks_ago = $week_ago-$one_week;

  $prev_week_total = $orders->getOrders($two_weeks_ago,$week_ago);


  $month_ago = $now-$one_month;
  $two_months_ago = $month_ago-$one_month;

  $month_total = $orders->getOrders($month_ago,$now);

  $last_month_total = $orders->getOrders($two_months_ago,$month_ago);

  $year_ago = $now-$one_year;

  $year_total = $orders->getOrders($year_ago,$now);


// Week Totals



// Month Totals





// Month Totals

