<?php

/**
 * Search
 */

  if(isset($_GET['q'])){

    $search_query = sanitise($_GET['q']);
    
    $search_results = $product->searchAll($search_query);

  
    printR($product->makeSearchSuggestions());

    /* Calculate totals
     */
  
    
    $product_results = $search_results['product_data'];
    $num_of_product_results = count($product_results);

    $recipe_results = $search_results['recipe_data'];
    $num_of_recipe_results = (!empty($recipe_results) ? count($recipe_results) : 0);
 
    $total_results = $num_of_product_results + $num_of_recipe_results;
    $result_word = ($total_results == 1 ? 'result' : 'results');

    /* Product results
     */


    $search_results = '';
    foreach ($product_results as $key => $data){
      // Retrieve the product image/google image
      $img = $product->getImage($data['id'],$data['name']);
      $data['description'] =  str_ireplace("$search_query","<b><u>$search_query</u></b>",$data['description']);
      // Trim the string to a more sensible length
      $data['description'] = substr($data['description'],0,135) ."&#8230;";
      $search_results .= "<div class='search_results'>
                          $img
                          <i class='fa fa-cube blue'></i><a href='./?p=products&id=".$data['id']."'>".$data['name']."</a><br>
                          <p>". $data['description'] ."</p>
                          </div>
                         ";
  
  
    }

    /* Recipe results
     */
    


    foreach ($recipe_results as $data){
  
      $data['method'] = str_ireplace("$search_query","<b><u>$search_query</u></b>",$data['method']);
      // Trim the string to a more sensible length
      $data['method'] = substr($data['method'],0,135) ."&#8230;";
      // Retrieve the recipe image/google image
      $img = $product->getImage($data['id'],$data['name'],false);
      $formatted_recipe_results .= "<div class='search_results'>
                          $img
                          <i class='fa fa-file-text-o blue'></i><a href='./?p=recipes&in=".$data['product_id']."'>".$data['name']."</a><br>
                          <p>". $data['method'] ."</p>
                          </div>
                         ";
  
  
    }









    $hint='';
        
    if($total_results===0){

      $hint = "<h3><i class='fa fa-question-circle blue'></i> Try refining your search and searching again</h3>";

    }
  
    $title = "<i class='fa fa-eye blue '></i>  Your search for <b><u>$search_query</u></b> returned $total_results $result_word";
    $product_word = ($num_of_product_results==1 ? 'product' : 'products');    
    $number_of_product_results = ($num_of_product_results >0 ?  "<h3 class='search_results'><i class='fa fa-cube blue'></i>
                                                                 Found <b>$num_of_product_results</b> $product_word</h3>"
                                                             :  '');

                                                     
    $recipe_word = ($num_of_recipe_results==1 ? 'recipe' : 'recipes');                                                         
    $number_of_recipe_results = ($num_of_recipe_results >0 ? "<h3 class='search_results'><i class='fa fa-file-text-o blue'></i>
                                                              Found <b>$num_of_recipe_results</b> $recipe_word</h3>"
                                                           : '');



  }else
  {
    $title = 'Search our products and recipes';
    $hint ='';
    $search_results ='';
    $number_of_recipe_results = '';
    $number_of_product_results = '';
    $formatted_recipe_results ='';
  }
  
  
    /* Search Result Output
     */
  
  ?>
  <div class='page_top'><h2><?=$title?></h2>
    <?=$hint?>
      <?=$number_of_product_results?>
  <?=$number_of_recipe_results?>  

  </div>
          
  <form action='./?p=search' method='get'>
    <div class='field'>
      <label for='search'>Search: </label>
      <input autofocus list='search' type='search' name='q' value='<?=$search_query?>' placeholder='Search...' />
      <button type='submit' name='p' value='search'>Go</button> 
    </div>
  </form>
  <hr>

  <?=$number_of_product_results?>
  <?=$search_results?>

  <?=$number_of_recipe_results?>  
  <?=$formatted_recipe_results?>
  

  
