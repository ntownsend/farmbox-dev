<?php
  
  /**
   * Chat class
   *
   * Provides live chat features
   *
   * @author Nick Townsend <nick.townsend@anglia.ac.uk>
   * @copyright 2014 Nick Townsend
   * @license http://www.php.net/license/3_01.txt PHP License 3.01
   */

  Class Chat{


    protected $sender;

  public function __construct($db_connection){

    global $userinfo;

    //  If it is a staff member then we set the user id as 0 so that all staff can receive the replies.
    $this->sender = ($userinfo['group']==0 ? $userinfo['id'] : '0' );
    
    $this->db_connection = $db_connection;

  }

  // Get last message


  //  Send Message1

  public function sendMessage($message,$recipient='0'){

    // Make timestamp

    $timestamp = date('U');

    $query_stmt = $this->db_connection->prepare("INSERT INTO chat_messages (msg_from,msg_to,message,timestamp) VALUES (:sender,:recipient,:message,:timestamp)");
    $query_stmt->execute(array('sender'=>$this->sender, 'recipient'=>$recipient,'message'=>sanitise($message),'timestamp'=>$timestamp));

  }



  
  /**  Get  Message
   *
   *  Retrieves the last message in the db that the user sent or received
   * 
   */

  public function getLastMessage(){

    // Make timestamp

    $timestamp = date('U');

    $query_stmt = $this->db_connection->prepare("SELECT * FROM chat_messages WHERE msg_to = :uid  OR msg_from = :uid ORDER BY timestamp DESC LIMIT 0,1");
    $query_stmt->execute(array('uid'=>$this->sender));
    $returned_row = $query_stmt->fetch(PDO::FETCH_ASSOC);
    
    return $returned_row;
  
  }

  /** Updats the chat db to reflect who is connected
   *
   */


   public function updateConnected($user_id){


    $query_stmt = $this->db_connection->prepare("UPDATE chat set timestamp = :timestamp WHERE user_id = :user_id");

    $timestamp = date('U');

    $query_stmt->execute(array('timestamp'=>$timestamp,'user_id'=>$user_id));

    



   }


  /** When a user last connected
   *
   */


   public function lastConnected($user_id){


    $query_stmt = $this->db_connection->prepare("SELECT timestamp FROM chat WHERE user_id = :user_id");

    $query_stmt->execute(array('user_id'=>$user_id));

    $returned_row = $query_stmt->fetch(PDO::FETCH_ASSOC);

    return $returned_row['timestamp'];
    



   }


  /** Check for connected customers or staff users
   * to the chat page.
   *
   *
   */

  public function chatConnected($staff=false){

    global $user;

    // Get recently connected users. the chat handler updates every 2 seconds and this records connected users
    // so we check for anyone who connected in the last 2 seconds. After this they are considered to be no longer connected
    
    $search_time = (date('U'))-2;
    $query_stmt = $this->db_connection->prepare("SELECT * FROM chat WHERE timestamp > :search_time  ");
    $query_stmt->execute(array('search_time'=>$search_time));
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);

    // Determine the group of each user that we find
    
    foreach($returned_data as $key => $info){

      $user_id = $info['user_id'];
      $group_id = $user->info($user_id)['group'];

      if($group_id>0){

          $admin_users[] = $user_id;

      }else
      {
        $customer_users[] = $user_id;
      }
      

    }

    if($staff=true){
        printR($admin_users);
      return $admin_users;
    }
    else{
        printR($customer_users);
      return $customer_users;
    }


  }





  }
