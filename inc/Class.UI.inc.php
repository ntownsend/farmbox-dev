  <?php
  
  
  
    // Set the right currency locale.
  
  
    // TODO is the UTF part only required on Debian?
    setlocale(LC_MONETARY, $conf['core']['language'].".UTF-8");
   

  /* For debug/developmenyt use only, this allows a quicker output of an array inside pre tags to
   * make the output easier to read.
   *
   *
   */


  function printR($array){
  
    echo "<pre>";
    print_r($array);
    echo "</pre>";
  
    }

  
  
  /** Custom exception handler
   * Allows debug mode to output lots of info to screen that we
   * would not want out there on production system and for all 
   * of this to be kept hidden in production
   *
   *
   * http://www.php.net/manual/en/function.set-exception-handler.php
   */
  
    function exception_handler($exception){
  
    global $site_uri;
  
    // For production just put it into the apache log.
    // This is better than a custom log as messages
    // will be noticed by the system administrator
    error_log("NT-DEBUG:: EXCEPTION THROWN BY ". $exception->getFile() .' ON LINE '. $exception->getLine() . ' ERROR::'. $exception->getMessage() );
  
  
    error_debug($exception->getMessage());
  //    error_debug($exception);
  
      display_feedback("An error has occured that means we can't process your request. Please try your last action again.",'Server Error','');
      
      
    }
  
  set_exception_handler('exception_handler');
  
  /*  TODO - REMOVE THIS
   *
   *
   */
  
    function validatePassword($pass){
  
      
  
      }

    /** Encodes the email subject line with base64 so that unicode characters can be used in the subject line,
     * needless beautification perhaps... there is some suggestion that this is otherwise, nonetheless it's  a neat touch
     * referenced at http://www.xpertdeveloper.com/2013/05/set-unicode-character-in-email-subject-php/
     */

     function encodeSubject($subject_line){

      global $conf;

          $subject = '=?UTF-8?B?' . base64_encode($conf['core']['email_subject'].' '. $subject_line)  .'?=';

          return $subject;
 
       }

  
    /*
     *
     *
     *
     *
     */
  
  function error_debug($message){
  
    // For development - put the error on the page
    echo "<div class='topdebug'><pre>$message</pre></div>";
    
    
    }
  
  /** Logs system actions to a central file
   *
   *
   *
   */ 
  
  
  function systemLog($_log_message){
  
    global $conf;
  
    echo $_log_message;
  
    }
  
  /* Checks for a https connection and redirects
   * an unencrypted request to the https version of the page
   * 
   */
  
  
  function make_connection_secure(){
  
    global $conf;
    global $flipflop;
    if(!$conf['core']['secure_connection']==1){
      header("Location:$flipflop");
    } 
  
  }
  
  
    /*
     *
     *
     *
     *
     */
  
  function userLoggedIn(){
  
    return(isset($_SESSION['uid']) ? true : false);
    
    }
  
  
    /*
     *
     *
     *
     *
     */
  
  function calculatePrice($price){
  
  
      //TODO Convert from the local currency to the users currency
    
      $conversion_rate = 1.0;
  
      // Return the price, converted and decimalised
      return number_format(($price * $conversion_rate)/100,2);
  
  
    }
  
  
    /* Displays feedback to the user in a non-javascript but javascripty way
     * This would be the fallback behavior for javascript being disabled were 
     * javascript more extensively woven into the current implementation
     *
     *
     * 
     */
  
  function display_feedback($_message,$_title='',$link='',$type='error'){
  
    global $request_link;
  
    if(empty($link)){
      $link = $request_link;
    }
    else{
    
      $link = './?p='.$link;
    
    }
  
    $class='';
    switch($type){
  
      case 'error':
      break;;
  
      case 'notify':
      $class='notify';
      break;;
  
  
    }
    
    echo "<div class='alert_overlay $class'>
    <h2>$_title</h2>
    <p>$_message
    <br>
    <a onclick='' href='". $link ."' />close</a>
    </p>
    
    </div>";
    
  }
  
  
  function pageRenderTime(){
  
    global $timer_start;
  
          $timer_stop = microtime(true);
          $time_taken = $timer_stop - $timer_start;
  
    return $time_taken ;
  
    }
  
    // Sanitises user input1
    /*
     *
     *
     *
     *
     */
  
    function sanitise($var){
    
        $cleanvar = strip_tags($var);
        $cleanvar = htmlspecialchars($cleanvar,ENT_QUOTES);

        // TODO = URGENT more stuff here
        
        return $cleanvar;
    
      }

    /* Handles the sending of email
     * encodes the subject
     * includes a plain text version
     * referenced from
     * http://kevinjmcmahon.net/articles/22/html-and-plain-text-multipart-email-/
     */
  

    function sendEmail($to,$subject,$message){

      global $conf;
    
      $headers = $conf['core']['email_headers'];
      

      $subject = encodeSubject($subject);
    
      $message_plain = strip_tags(str_replace(['<table>','<td>','<hr>'],[' \n\r','\t','\n\r'],$message));

      $message_boundary = uniqid('FB_',TRUE);
      $headers .= "Content-Type: multipart/alternative;boundary=".$message_boundary . "\r\n";

        $message_full  = "MIME encoded message follows";
        $message_full .= "\r\n\r\n--". $message_boundary . "\r\n";
        $message_full .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
        $message_full .= $message_plain;
        $message_full .= "\r\n\r\n--" . $message_boundary . "\r\n";
        $message_full .= "Content-type: text/html;charset=utf-8\r\n\r\n";
        $message_full .= $message;
          

      mail($to,$subject,$message_full,$headers);
      
    return true;


    }


  
  
  /** makes a nice link for the navigation
   *
   * and allows the easy incorporation of Font Awesome icons
   *
   */
   
  
  function makeNavLink($destination,$fa_icon='',$text,$classes='',$echo=false){
  
                $fa_icon = (empty($fa_icon) ? '' : "<i class='fa $fa_icon '></i> ");
  
                $link = "<li>
                            <a href='./?p=$destination' class='$classes'> $fa_icon $text</a>
                          </li>";
  
              if($echo==false){
                
                return $link;
              }
              else{
  
  
                echo $link;
              }
              
            }
            
  
  
  
  
    /*
     *
     *
     *
     *
     */
  
  function page_redirect($_page='products'){
    global $site_uri;
    
    header("Location:$site_uri?p=$_page");
  
    }
  
  
  
  function validatePasswords($p1,$p2){
    
          $errors = array();
        
          if(!isset($p1)||(!isset($p2))||($p1!=$p2)){
    
            return false;
            break;
        
          }
  
          // TODO REGEX FOR UNWANTED CHARS?
    
          return true;
     
        }
  
  
      
  
  
  
    /* Debug function  - only used during development
     *
     *
     *
     *
     */
  
  function debug($level='0'){
  
    global $conf;
    global $git_uri;
    global $git_build_ver;
    global $timer_start;
    global $flipflop;
    if($conf['debug']>0){
        
      // Extract core and db config values  
      extract($conf['core']);
      extract($conf['db']);
      
    
      $mysql_ver='unknown';
    
      
    
      $git_last_commit = `git log -1 --pretty=format:"%ar"`;
      $phpmyadmin= ($conf['core']['secure_connection']==1 ? 'https' : 'http') .'://'. $_SERVER['HTTP_HOST'] . '/phpmyadmin';
    
      include"./debug.php";
    }
  
  }
  
