<?php

/***********************************************************************************************************************
 * Database
 *
 * Farmbox uses the PHP PDO functionality (http://www.php.net/manual/en/intro.pdo.php)
 * useful discussion in understanding why : 
 * http://code.tutsplus.com/tutorials/why-you-should-be-using-phps-pdo-for-database-access--net-12059
 *
   */
  
  $conf['db']['host']='127.0.0.1';
  $conf['db']['username']='farmbox';
  $conf['db']['password']='f4rmb0x';
  $conf['db']['database']='farmbox';
  



 /***********************************************************************************************************************
 * Application Core
 *
 *
 */

  
  // The shop location - iused for direction retrieval
  $conf['core']['shop_location'] = 'Foxton, UK';
  // Default stylesheet to use
  $conf['core']['style'] = 'default';
  // The email address to send stock notifications to 
  $conf['core']['manager_email'] = 'nick@thejubster.co.uk';
  // Whether user registration is allowed
  $conf['users']['can_register'] = true;

  // Company details
  $conf['core']['company_name']='Abe & Cloe.';
  $conf['core']['company_address']='Building 1, Vegtown';
  $conf['core']['vat_number']='0012346578';
  $conf['core']['site']['delivery_cost'] = '4.50';
  $conf['delivery']['free_delivery_threshold_price']=20.00;

  // Email settings
  $conf['core']['email_address']='farmbox.website@thejubster.co.uk';
  $conf['core']['noreply_address'] = 'noreply@thejubster.co.uk';
  $conf['core']['email_subject'] = '★ ['. $conf['core']['company_name'] .']';
  $conf['core']['email_headers'] = 'MIME-Version: 1.0' . "\r\n";
  $conf['core']['email_headers'] .= 'From: =?UTF-8?B?'. base64_encode($conf['core']['company_name']) . "?=<". $conf['core']['noreply_address'] .">\r\n";
  $conf['core']['email_headers'] .= 'Reply-To: '. $conf['core']['company_name'] . '<'. $conf['core']['noreply_address'] .'> ' . "\r\n";


  // Social media links
  $conf['social']['google_plus'] = 'abe_and_cloe_googleplus_address';
  $conf['social']['twitter'] = 'abe_and_twitter_address';
  $conf['social']['facebook'] = 'abe_and_cloe_facebook_address';

  // Inner mechanics
  // TODO - this could be improved and tidied
  $conf['core']['default_timezone']='Europe/London';
  $conf['core']['app_version']=file_get_contents('./farmbox_version');

  // Image directories
  $conf['core']['product_image_dir'] = './cache/product_images/';
  $conf['core']['recipe_image_dir'] = './cache/recipe_images/';

  // SEO elements used in document head section
  
  $conf['core']['strapline'] = 'Healthy Organic Food Delivered Directly To Your Door';
  $conf['core']['site_description'] = $conf['core']['company_name'] .'Healthy Organic Food Delivered Directly To Your Door. Browse through our range of healthy organic produce and order today ';
  $conf['core']['site_keywords'] = $conf['core']['company_name'] .',organic produce, farmshop, groceries, healthy food ';
  $conf['core']['site_classification'] = 'organic produce';
  $conf['core']['author']='9906420';


  // Site elements
  $conf['core']['logo_url']='./';
  $conf['core']['favicon_url']= './favicon.ico';
  $conf['core']['site_logo_link'] = "<a href='". $conf['core']['logo_url'] ."' title='". $conf['core']['company_name'] ."'>". $conf['core']['company_name'] ."</a>";

  $conf['core']['language']='en_GB';				// The default language for the site. so far :en_gb and da_dk
  $conf['core']['language_short'] = substr($conf['core']['language'],0,2);

  // Whether to use google image search when images can't be found locally'

  $conf['core']['use_google_image'] = true;


 /***********************************************************************************************************************
 * Authentication
 *
 */



  // Parameters for setting password length requirements
  $conf['auth']['username_policy']['min_characters'] = '6';
  $conf['auth']['username_policy']['max_characters'] = '32';


  // Parameters for setting password length requirements
  $conf['auth']['password_policy']['min_characters'] = '8';
  $conf['auth']['password_policy']['max_characters'] = '72';

  // How many number chars the password must contain
  $conf['auth']['password_policy']['numbers'] = '0';

  // How many special characters the password must contain
  $conf['auth']['password_policy']['specials'] = '0';

  // Whether to use password expiry (true/false)
  $conf['auth']['password_policy']['password_expiry'] = true;

  // Time period (days) after which passwords must be reset
  $conf['auth']['password_policy']['reset_period'] = '365';
  

  // Restricted words list - ()
  // TODO : make this a function
  $conf['auth']['restricted_words'] = array('password','1234','12345','123456');

// $conf['auth']['']


 /***********************************************************************************************************************
 * Administration
 *
 */


$config['admin']['super_admin']='FBAdmin';			// The name of the super_user admin

 /***********************************************************************************************************************
 * Mail Settings
 */


$conf['mail']['mta']='smtp';					// Mail delivery method: accepts: php, smtp, none

$conf['mail']['smtp']['host']='localhost';
$conf['mail']['smtp']['port']='25';
$conf['mail']['smtp']['auth']='false';
$conf['mail']['smtp']['user']='username';
$conf['mail']['smtp']['pass']='password';
$conf['mail']['smtp']['encryption']='none';



function IsConnectionHttps(){ 

  $connection_is_secure = ($_SERVER['HTTPS']==null ? false : true);
     

  return $connection_is_secure;

}
$conf['core']['secure_connection'] = IsConnectionHttps();

$conf['core']['site_link'] =$_SERVER['REQUEST_URI'] ;
$conf['core']['base_dir'] = '/FarmBox/farmbox-dev/';



// TODO clean this up
$link_uri = ($conf['core']['secure_connection']==1 ? 'https' : 'http') .'://'. $_SERVER['HTTP_HOST'] . '/';
$site_uri = ($conf['core']['secure_connection']==1 ? 'https' : 'http') .'://'. $_SERVER['HTTP_HOST'] . $conf['core']['base_dir'] ;
$conf['site_uri'] = ($conf['core']['secure_connection']==1 ? 'https' : 'http') .'://'. $_SERVER['HTTP_HOST'] . $conf['core']['base_dir'] ;
$flipflop = ($conf['core']['secure_connection']==0 ? 'https' : 'http') .'://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '';
// TODO: move this somewhere else?
$request_link = ($conf['core']['secure_connection']==0 ? 'http' : 'https') .'://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '';

// Whether to separate the user roles or stack them up more like wordpress does
// by default, this is set to true which allows
// the admin user all privileges,
// the manager all privileges up to manager level,
// the orderpicker all privileges up to order picker level.
// all account types are provided the customer links to allow staff to shop.

$conf['core']['priveleges']['stack'] = true;



$conf['core']['app_name']='FarmBox';

// Make application string <name> <version>
$conf['core']['full_version']=$conf['core']['app_name']. " " . $conf['core']['app_version'];

// Check Language file exists
$lang_file = "./lang/".$conf['core']['language'].".php";


// Only works on machines with git installed, so this won't work on XAMPP!

exec("git log --pretty=format:'%h' -n 1",$git_build);
$git_build_ver = $git_build[0];
exec("git config --get remote.origin.url", $git_url);
$git_uri = $git_url[0];


/* Debug level
 *
 * Note: debug information is only displayed to logged in users
 * who have administrative privelege level of 2 or above
 * 
 * This can be overidden by uncommenting the $conf['debug_available_to'] variable below
 * and choosing the admin level at which debug info will be displayed.
 * It's probably best not to fiddle with this on production servers
 * without a good reason!
 *
 * Debug verbosity
 * May be any number between 0 and 3
 * 0 - off (default)
 * 1 - PHP and database Errors - A good place to start!
 * 2 - Verbose - Lot's of info
 * 3 - (developer) - pretty much everything that happens
 */

$conf['debug'] =0;

/* SEE ABOVE COMMENT BEFORE UNCOMMENTING/CHANGING THIS! */
//$conf['debug_available_to'] = 2;


