<?php

// Set the user level for this page.

$user->authorise(1);


 if(isset($_POST['a'])){

    if($_POST['a']=='dispatched'){
      
        $orders->updateStatus($_POST['order_id']); 
      
    }
  }

// Get each order where transaction is completed

  $sendable_orders = $orders->getPackedOrders();

  
// Orders

  $pending_orders = count($orders->getPackableOrders());
  $packed_orders = count($orders->getPackedOrders());

  $html = '';
  foreach($sendable_orders as $order_id => $order_detail){

    // format the time into something pretty

    $order_placed = date('H:i:s d/m/y', $order_detail['timestamp']);

    $html .= "<div class='butt_after float_left'>
    <h2><i class='fa fa-truck'></i> Order ". $order_detail['id'] ."</h2>
    <h6>$order_placed</h6>
    <form action='./?p=dispatch' method='post'>
    <input type='hidden' name='order_id' value ='".$order_detail['id']."'/>
    <table class='evenodd'>
    ";

    // Get customer info for label

    $deliver_to = $user->info($order_detail['customer_id']);

    $delivery_info  = '<h3> Deliver To</h3><hr>';
    $delivery_info  .= '<h4>'. $deliver_to['username'] .'</h4>';
    $delivery_info .= $deliver_to['address_details']['house'] .'<br>';
    $delivery_info .= $deliver_to['address_details']['address'] .'<br>';
    $delivery_info .= $deliver_to['address_details']['postcode'] .'<br>';
    $delivery_info .= $deliver_to['address_details']['country'] .'<br>';

    

    $html .= "<tr><td colspan='4' class='bordered'>$delivery_info</td></tr>";
    $html .= "<tr><td colspan='4' class='right'> <button name='a' value='dispatched'><i class='fa fa-truck'></i> Order Dispatched</button></td></tr>";
    $html .= "</table></form></div>";




  }
