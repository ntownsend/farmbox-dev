<?php

  $html = '';

  if(isset($_POST['a'])){

    if($_POST['a']=='send_reply'){

      $recipient = $_POST['email_to'];

      $message_text = sanitise($_POST['reply_msg']);

      sendEmail($recipient,'Re: Website Enquiry',$message_text);
      $message->markReplied($_POST['msg_id'],$message_text);

    }

    if($_POST['a']=='delete_message'){

      $message->delete($_POST['msg_id']);


    }



  }


  // retrieve messages that have vbeen replied to 

  if(isset($_GET['replied'])){

    $all_messages = $message->getReplied();
  $link = "<a href='./?p=messages'>View new messages</a> ";
    foreach($all_messages as $id => $message_detail){
    
        $message_time = date('d/m/Y H:i:s',$message_detail['received']);
        
        $html .="<div class='butt_after'>
        <h2><i class='fa fa-envelope-o'></i> ". $message_detail['name'] ." (".$message_detail['email'].")</h2>
        <h6>".$message_time."</h6>
        
        <p>".$message_detail['message_text']."</p>
            <form action='./?p=messages' method='post'>
          <div class='field'>
    
            <input type ='hidden' name='msg_id' value='".$message_detail['id']."' />
            <input type ='hidden' name='email_to' value='".$message_detail['email']."' />
            <hr>
            <i class='fa fa-reply'></i> user: <b>" . $user->info($message_detail['replied_by'])['username']
            ."</b> replied to this message <b>on ".date('d/m/Y  \a\t H:i:s',$message_detail['replied_date']) ."</b>
            <br><br>
            ". $message_detail['reply_msg']."
  
          </div>
  
        </form>
        </div><hr>";
    
        
    
    
    
      }
    


  }else{

    // retrieve unreplied messages
  
    $all_messages = $message->awaitingResponse();
    $link = "<a href='./?p=messages&replied=true'>View replied messages</a> ";
  
    foreach($all_messages as $id => $message_detail){
    
        $message_time = date('d/m/Y H:i:s',$message_detail['received']);
    
        $html .="<div class='butt_after'>
        <h2><i class='fa fa-envelope-o'></i> ". $message_detail['name'] ." (".$message_detail['email'].")</h2>
        <h6>".$message_time."</h6>
        
        <p>".$message_detail['message_text']."</p>
            <form action='./?p=messages' method='post'>
          <div class='field'>
    
            <input type ='hidden' name='msg_id' value='".$message_detail['id']."' />
            <input type ='hidden' name='email_to' value='".$message_detail['email']."' />
            <label for='reply_msg'><i class='fa fa-reply'></i> Reply</label>
        
            <textarea rows='10' cols='90' name='reply_msg'></textarea>
          </div>
          <div class='field centered'>
            <button type='submit' class='red_button' name='a' value='delete_message'><i class='fa fa-trash-o'></i> Delete</button>
            <button type='submit' name='a' value='send_reply'><i class='fa fa-reply'></i> Send reply</button>
          </div>
        </form>
        </div><hr>";
    
        
    
    
    
      }
  

  }

    
  /** List message that have not been replied to
   *
   *
   */
  
  $messages_total = $message->total();
  $new_messages_total = count($message->awaitingResponse());
  $replied_messages_total = $messages_total-$new_messages_total;
     
  
  
