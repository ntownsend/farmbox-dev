<?php
  
  if(isset($_SESSION['rescue'])){

    // Password checking
  
        // TODO QUESTION - should I add slashes
        //$p1 = addslashes($_POST['password']);
        $p1 = $_POST['password'];
        $p2 = $_POST['password2'];
      
        if(validatePasswords("$p1","$p2")!=false){
          $p1 = UserSession::createPassword($p1);
          $user->changePassword($_SESSION['rescue'],$p1);
          $user->clearReset($_SESSION['rescue']);
          
          $_SESSION['uid'] = $_SESSION['rescue'];
          $_SESSION['rescue']='';
  
          $name = $user->info($_SESSION['uid']);
          $name = $name['name'];
          display_feedback("Password successfully changed. Welcome back".$name,"Password changed",'account','notify');

        }
        else
        {

        $page = 'resetpw';
          $errors['password']['message']='passwords dont match';
        }
        

  }
      
    
  
  //TODO - REORDER THE LOGIC OF THIS FILE
  
  
  
  
  
  elseif(isset($_GET['hash'])){
  
   // $hash = sanitise($_GET['hash']);
    $hash = $_GET['hash'];
  
    // If the email and hash exist then allow password reset
      
    $hash_user = $user->verifyReset($hash);
  
    if($hash_user!=false){
  
      $_SESSION['rescue'] = $hash_user;
      
      page_redirect("resetpw");
  
      //$page = 'resetpw';
  
    }
    else{

    display_feedback("This reset code has expired. Please request another one",'Invalid Code','reset_account');

    }
  
    
  }
  
  // This is a password change request
  
  elseif(isset($_SESSION['uid'])){

    $user->sendResetLink($userinfo['email']);

    display_feedback("We've sent you an email with a link to a secure page where you can change your password",'Password change','account','notify');



    }
  
  
  elseif(isset($_POST['email_address'])){
  
    $email_address = sanitise($_POST['email_address']);
  
  
      if($user->Exists($email_address)!=false){
  
        $user->sendResetLink($email_address);
  
      }
     else{

	display_feedback("We couldn't find that address in our records, please try again.","Address not found",'reset_account');

     }
  
  
    }
  
