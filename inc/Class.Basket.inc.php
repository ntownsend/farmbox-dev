<?php

Class Basket{


  protected $basket_id;
  protected $basket_items = array();
  private $db_connection;

  /* Constructor class
   *
   */
   
  public function __construct($db_connection){

  $this->db_connection = $db_connection;
    if(userLoggedIn()==true){
      $this->basket_id = uniqid($_SESSION['uid'].'x');
    }
    else{
      $this->basket_id = uniqid();
    }
    
    //error_log('Basket id: ' .$this->basket_id );
    $_SESSION['basket']['id'] = $this->basket_id;
    //$_SESSION['basket_items'] = '';
   // $_SESSION['basket']['object'] = $this;
        

  }

 /** Check the stock level of a specific product
   *
   *  @param $item The product id to lookup
   *  @return The stock level of that product
   *
   */

  public function checkStockLevel($item){


    $query_stmt = $this->db_connection->prepare("SELECT stock_qty FROM products WHERE id = :item");
    $query_stmt->execute(array('item'=>$item));
    $returned_row = $query_stmt->fetch(PDO::FETCH_ASSOC);

    

    return $returned_row['stock_qty'];
    



  }



 /** Adds an item to the product table
   *
   *
   */

  public function add($itemid,$qty,$single=true){

    $notify = false;
     
     

    if(!is_numeric(trim($qty))){

      display_feedback('That was not a number!'. htmlentities(trim($_POST['qty'])));
     

    }
    else{

      // check for sufficient stock quantity
      
      $stock_level = $this->checkStockLevel($itemid);

      // If we already have this product id in the basket then add the two qtys together

      $stock_level = (isset($_SESSION['basket_items'][$itemid]) ? $stock_level - $_SESSION['basket_items']["$itemid"] : $stock_level);

      // TODO move this to lang

      // Keep a copy of the requested amount for use in the feedback we may need if there isn't enough stock
      $orig_qty = $qty;


      if($stock_level<$qty){

        // If there isn't enough stock to add the no requested then set notify to true to trigger different handling for this qty

        $notify=true;
        
        $qty = $stock_level;


      }




      if(!empty($_SESSION['basket_items'][$itemid])){

        if($notify==false){
          $_SESSION['basket_items'][$itemid] = $_SESSION['basket_items'][$itemid] + $qty;
        }
        else
        {
          $_SESSION['basket_items'][$itemid] = $qty;
        }
        

      }
      else{

        if($qty>0){
        
          $_SESSION['basket_items'][$itemid] = $qty;  
        }
      }

      if($notify==true){

        if($single==true){
                      display_feedback("Sorry, we don't have quite enough of those in stock to add <b>$orig_qty</b> to your basket.
                      There are currently <b>$stock_level</b> left in stock for you to order today, all of which are now in your basket<br>
                      <b>Please be aware that these aren't guaranteed to be yours until you successfully complete your order! We're taking you to your basket now!</b><br><br>
                      Our stock is constantly replenished so we're sure to have more very soon.
                      ", "Not enough stock",'basket');
        }
        else{

          return $itemid;

        }
                      
      }

      

    }

  }

  /* Updates the basket by clearing it and then adding the updated content from POST
   * 
   * @input $pid  Product ID
   * @input $qty  Product Quantity
   *
   * @return void
   */

  public function update(){

    global $product;
  
    //$this->clear();

    $shortfall_items ='';
    
    foreach($_POST['basket_items'] as $pid => $qty){

      $update = $this->add($pid,$qty,false);

      if(!empty($update)){

        $shortfall_items .= $product->lookup($update)['name'] .', ';

      }


    }

    if(!empty($shortfall_items)){
      display_feedback("We couldn't add all of the $shortfall_items that you request to your basket as we don't quite have enough stock,
                        We've added all that we have. Our stock is constantly replenished so we're sure to have more very soon."
      ,"Couldn't add all items",'basket');
    }


  }


  /* Saves a product whose details have been updated
   *
   */

  public function save(){

      // put values from $_SESSIOM['basket_items'] into 'basket_items'
  
      // TODO - make sure this can only be done by logged in users.

      if(!isset($_SESSION['uid'])){
        
        page_redirect('login');

        // TODO - this renders without styling - (loaded before header)
        // =replaced with page redirect, not as informative but less ugly
        //display_feedback("You have to be logged in to save baskets. If you are not already a member, you can register in minutes.","Not logged in",'basket');
        //break;
      }
      
    
      $_query_stmt = $this->db_connection->prepare("INSERT INTO saved_baskets (id,user_id) VALUES (:bid,:uid)");
      $_query_stmt->execute(array('uid' => $_SESSION['uid'],'bid'=>$this->basket_id));
          
     
  
    // Now add the items to the  saved_basket_items table
  
  
      foreach($_SESSION['basket_items'] as $prod => $qty){
        $_query_stmt = $this->db_connection->prepare("INSERT INTO saved_basket_items (list_id,product_id,qty) VALUES (:bid,:pid,:qty)");
        $_query_stmt->execute(array('pid' => $prod,'qty'=>$qty,'bid'=>$this->basket_id));
      }
  
      
      // TODO move to lang
      display_feedback("Your basket has now been saved<br>You can restore it at any time from the 'Saved Baskets' section.","Basket Saved","savedbaskets&amp;a=cb",'notify');
  
  



  }
  
  /**
   *  Lists the number of saved baskets a user has
   *
   *
   * 
   *************************************************
   * @return  int    The number of saved baskets
   * 
   */

  public function numSaved($uid=''){


    

    // Call the function (and db) once only

    $saved = count($this->listSavedBaskets($uid));


    if(!empty($saved)){

      return $saved;

    }
    else{

      return 0;
    
    }

  }

  /** listSavedItems
   * 
   * Lists the items in a users saved basket
   *
   * 
   *************************************************
   *
   * @input   varchar   the id of a basket  
   * @return  array     A PDO Associtive array 
   * 
   */

  public function listSavedItems($basket){


    $query_stmt = $this->db_connection->prepare("SELECT * FROM saved_basket_items WHERE list_id = :basketid");
    $query_stmt->execute(array('basketid' => $basket));
 
    $returned_data = $query_stmt->fetchAll(PDO::FETCH_ASSOC);
     


  return $returned_data;

  }

  /* Calculates the total price of the specified savedbasket
   */

  public function savedBasketTotals($basket_id,$calculated=true){

    global $product;
    

    $basket_items = $this->listSavedItems($basket_id);
    $basket_total = array('count'=>0,'total'=>0);
    $i=0;
    foreach($basket_items as $key => $item){
      
      $price = $product->priceLookup($item['product_id'],false);
      $qty = $item['qty'];
      $item_total = $qty*$price;
      $basket_total['total'] = $basket_total['total']+$item_total;
      $basket_total['count']++;

    }

    $basket_total['calculated_total'] = calculatePrice($basket_total['total']);
    
    return $basket_total;

  }


  /** Lists the id of all saved baskets a user has
   *
   *
   * 
   *************************************************
   * @return array PDO ASSOC Array
   * 
   */

  public function listSavedBaskets($uid=false){
      
  
        $_query_stmt = $this->db_connection->prepare("SELECT id,name FROM saved_baskets WHERE user_id = :uid");

        $uid = ($uid==false ? $_SESSION['uid'] : $uid);
        $_query_stmt->execute(array('uid' => $uid));
     
        $returned_data = $_query_stmt->fetchAll(PDO::FETCH_ASSOC);
        

  return $returned_data;

  }

  /** Restores a saved basket from teh db to the session
   *
   **************************************************
   * 
   */

  public function restore($basket_id){

    $items = Basket::listSavedItems($basket_id);
    $_SESSION['basket_items'] = array();
    
    foreach($items as $value){

          $prid = $value['product_id'];
      
        $_SESSION['basket_items'][$prid] = $value['qty'];

      }

    display_feedback("Your saved basket has now been restored",'Basket Restored','basket','notify');

      
  }

  /** Delete a saved basket
   * 
   *
   *
   */

  public function delete($basket_id){


    $query_stmt = $this->db_connection->prepare("DELETE FROM saved_basket_items WHERE list_id = :basket_id");
    $query_stmt->execute(array('basket_id'=>$basket_id));
    $query_stmt = $this->db_connection->prepare("DELETE FROM saved_baskets WHERE id = :basket_id");
    $query_stmt->execute(array('basket_id'=>$basket_id));
    display_feedback("The saved basket was successfully deleted",'Basket Deleted','savedbaskets');


  }




/* rename basket
 * 
 */

 public function rename($basket_id,$name){

    $name = sanitise($name);
    

    $query_stmt = $this->db_connection->prepare("UPDATE saved_baskets  SET name = :name WHERE id = :basket_id");
    $query_stmt->execute(array('basket_id' => $basket_id,'name'=>$name));



 }


  /* Clears the shopping basket
   *
   *
   */

  public function clear(){

    $_SESSION['basket_items'] = '';

  }





  /** Totals up the cost of the items in the basket
   *
   *
   *  TODO - Store this in the session when we add it? we would recalculate the real price once we checkout... not sure
   */

  public function totalPrice(){

    $basket_total = 0.00;

    // The product object needs to be in scope here so we can lookup the price
    // and config so we have access to the delivery info

    global $product;
    global $conf;

    if(!empty($_SESSION['basket_items'])){
      foreach($_SESSION['basket_items'] as $id => $qty){
  
        $price = $product->priceLookup($id) * $qty;
  
        $basket_total = $basket_total + $price;
  
      }
  
    }


    // Set our subtotal now so that it doesn't include delivery or discount

    $basket_subtotal = $basket_total;


    // Calculate discount

    $discount =0;
    $discount_amount = '0';

    if(isset($_POST['promo_code'])){
      
      // TODO - A whole load of checking, and stuff with these 'one use' codes

      // TODO - Promo code lookup
      // TODO - as inlookup promo code in db
      // TODO - has user already redeemed this code?
      // TODO - if not then apply the discount
      // TODO - also have to store this when order placed using code

      // TODO redeemPromoCode()
      // TODO Samnity checking - db table;


      
      if(sanitise($_POST['promo_code']=='FIRST50')){
        $_SESSION['promo_code'] = sanitise($_POST['promo_code']);
        $_SESSION['basket_promo_discount'] = 0.5;
      }
    }

      $discount = (isset($_SESSION['basket_promo_discount']) ? $_SESSION['basket_promo_discount'] : $discount);
        
      

      $basket_total = $basket_total * (1-$discount);

      $discount_amount = $basket_subtotal -  $basket_total ;

      $discount_percent = $discount*100;
      
    

    // Calculate delivery
 ;
    $free_delivery_thresh = $conf['delivery']['free_delivery_threshold_price'];


    $delivery = $conf['core']['site']['delivery_cost'];

    // Only add delivery if basket total is less than the thresh-hold specified in config
    //TODO move this to config

    $delivery = ($basket_total>=$free_delivery_thresh ? '0.00' : $delivery);

    $basket_total = $basket_total + $delivery;

    // Totals


    $totals = array(
    
                    'basket_subtotal' => number_format($basket_subtotal,2),
                    'basket_total' => number_format($basket_total,2),
                    'discount_percentage' => $discount_percent,
                    'discount_amount' => number_format($discount_amount,2),
                    'delivery' => number_format($delivery,2)
                    );


    
    
    return $totals;


  }

  /** Returns the cnumber of items currently in the basket
   *
   */
    
  public function numberOfItems(){
    // Shows number of products

    $no_of_items=0;
    $out['num'] = 0;

    

    if(!empty($_SESSION['basket_items'])){
      foreach($_SESSION['basket_items'] as $id => $qty){
  
        $no_of_items=$qty+$no_of_items;

        $out['num'] .=$no_of_items;


        
      }
    }
    return $no_of_items;
    
  }

  /* Get the contents of a specified basket
   *
   *
   */

  public function getContents($_basket_id){


    // get all basket_items where basket_id = $_basket_id && userid = $userinfo['id']
    print_r($this-basket_items);

  }

  


}
