-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 12, 2014 at 02:54 AM
-- Server version: 5.5.35-1-log
-- PHP Version: 5.5.9-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `farmbox`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_resets`
--

CREATE TABLE IF NOT EXISTS `account_resets` (
  `hash` varchar(32) COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `account_resets`
--

INSERT INTO `account_resets` (`hash`, `created`, `user`) VALUES
('1d127e5346ef852dc42a38f4df9ea446', '2014-05-06 22:48:11', 27),
('38469cbd33887747e74eb7e937e2f3cf', '2014-05-06 22:43:02', 17),
('393071b14b75a2c97f1846b569637f92', '2014-05-06 22:45:50', 17),
('5634ea3d546c89276928bc595bd899fc', '2014-05-06 22:20:02', 17),
('6ae2b6ed0c359cc5d6a7132d3cec915b', '2014-05-06 22:46:45', 27),
('7dca9952d51af02aa12af895aec1f6c5', '2014-05-06 22:42:55', 17),
('98bcfc0969f61c8e65a4a8d32fa29a9b', '2014-05-06 22:15:54', 17),
('9eabf90ec73b061bc2b082987bb76766', '2014-05-06 22:18:21', 17),
('c222b6651ac2ee52334a65e75252d484', '2014-05-06 22:46:14', 17),
('d39669000f0e35c25d408436938c3863', '2014-05-06 22:16:55', 17),
('f49bd35e90c3a78e9e846cf43025b673', '2014-05-06 18:48:21', 18);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `chat_messages`
--

CREATE TABLE IF NOT EXISTS `chat_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_to` int(11) NOT NULL,
  `msg_from` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `message` mediumtext COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=216 ;

--
-- Dumping data for table `chat_messages`
--

INSERT INTO `chat_messages` (`id`, `msg_to`, `msg_from`, `timestamp`, `message`) VALUES
(43, 0, 18, 1399323909, 'asd'),
(44, 0, 18, 1399323916, 'Type your message here'),
(45, 0, 18, 1399323931, 'Hi said Hank'),
(46, 18, 0, 1399323940, 'Replying to hank'),
(47, 0, 18, 1399324078, 'hi'),
(48, 0, 18, 1399324100, 'hi there'),
(49, 0, 18, 1399324202, 'Type your message here'),
(50, 0, 18, 1399324231, ''),
(51, 0, 18, 1399324237, 'sdf'),
(52, 0, 18, 1399324244, 'sdfet'),
(53, 0, 18, 1399324658, 'sdfetdsdsdsdsd'),
(54, 0, 18, 1399324759, 'sdfetdsdsdsdsdasdasdas'),
(55, 0, 18, 1399325055, 'werwerwer'),
(56, 0, 18, 1399325113, 'asd3ddwwdsf<b>words</b><a href=''./?p=account''>link</a>'),
(57, 0, 18, 1399325153, 'asd3ddwwdsfwordslink'),
(58, 0, 18, 1399325174, 'words words words words words'),
(59, 0, 18, 1399325188, 'words words words words words'),
(60, 0, 18, 1399325242, 'sdf23c rv r g rwedc'),
(61, 0, 0, 1399325447, 'errrType your message here'),
(62, 0, 0, 1399325452, 'errrType your message here'),
(63, 0, 0, 1399325454, 'errrType your message here'),
(64, 0, 0, 1399325464, 'Type your message here'),
(65, 0, 0, 1399325468, 'asd'),
(66, 0, 0, 1399325469, 'asd'),
(67, 0, 0, 1399325469, 'asd'),
(68, 0, 0, 1399325470, 'asd'),
(69, 0, 0, 1399325471, 'asd'),
(70, 0, 0, 1399325471, 'asd'),
(71, 0, 18, 1399325587, 'Type your message here'),
(72, 0, 18, 1399325930, 'sdfsdf'),
(73, 0, 18, 1399325979, 'Type your message here'),
(74, 0, 18, 1399325989, 'another message'),
(75, 0, 0, 1399326310, 'Type your message here'),
(76, 0, 0, 1399326313, ''),
(77, 0, 0, 1399326315, ''),
(78, 0, 0, 1399326319, 'asd'),
(79, 0, 18, 1399326327, 'another message'),
(80, 0, 0, 1399326573, 'Type your message here'),
(81, 0, 18, 1399326593, 'something'),
(82, 0, 0, 1399326634, 'hi me'),
(83, 0, 18, 1399326690, 'asdasdasdasdasdasd'),
(84, 0, 0, 1399327826, 'Type your message here'),
(85, 0, 18, 1399327837, 'a2df'),
(86, 0, 18, 1399328682, 'a2df'),
(87, 0, 0, 1399328688, 'hi hank'),
(88, 0, 18, 1399328824, 'asd'),
(89, 0, 18, 1399328830, 'asdasd'),
(90, 18, 0, 1399328835, 'hi hank'),
(91, 0, 18, 1399328882, 'is this for real?'),
(92, 18, 0, 1399328896, 'yes hank, it''s for real'),
(93, 0, 18, 1399328941, 'is this really for real?'),
(94, 0, 18, 1399328957, 'is this really for real?'),
(95, 18, 0, 1399328976, 'yes hank, it''s for real, jeez'),
(96, 0, 18, 1399329182, 'O. M . G.'),
(97, 0, 18, 1399329196, 'Type your message here'),
(98, 0, 18, 1399329201, 'asdqwe'),
(99, 0, 18, 1399329207, 'ahahahaha'),
(100, 0, 18, 1399329218, 'yes - '),
(101, 0, 18, 1399329280, 'well well well'),
(102, 0, 18, 1399329292, 'I do belive it''s taking shape'),
(103, 18, 0, 1399329453, 'why thank-you hank'),
(104, 0, 18, 1399329469, 'that''s ok'),
(105, 0, 18, 1399329745, 'Hi there Babs'),
(106, 0, 18, 1399330094, 'fdsfs'),
(107, 0, 18, 1399330121, 'fsdasasdf'),
(108, 0, 18, 1399330164, 'tre'),
(109, 18, 0, 1399330173, 'why thank-you hank'),
(110, 0, 18, 1399330201, 'great'),
(111, 0, 18, 1399330226, 'better?'),
(112, 0, 18, 1399330251, 'better now?'),
(113, 0, 18, 1399330302, 'even better'),
(114, 0, 18, 1399330333, 'and now?'),
(115, 18, 0, 1399330548, 'how does this look now?'),
(116, 0, 18, 1399330568, 'good from this end, how about yours?'),
(117, 18, 0, 1399330582, 'still not quite right'),
(118, 0, 18, 1399330633, 'how about now?'),
(119, 18, 0, 1399330643, 'nope'),
(120, 0, 18, 1399330655, 'ah, oh dear'),
(121, 0, 18, 1399330722, 'did the last tweak help?'),
(122, 0, 18, 1399330829, 'hello'),
(123, 18, 0, 1399331042, 'hi'),
(124, 0, 18, 1399331066, 'so apart from the scrolling, this is looking good.'),
(125, 0, 18, 1399331102, 'do you see my name now?'),
(126, 18, 0, 1399331109, 'not yet'),
(127, 18, 0, 1399331148, 'I think I fixed it...'),
(128, 0, 18, 1399331157, 'let''s test your theory'),
(129, 18, 0, 1399331169, 'sweet! It works!'),
(130, 0, 18, 1399331182, 'just the scroliing and this is almost golden'),
(131, 0, 24, 1399331279, 'now jeff enters the ring'),
(132, 18, 0, 1399331292, 'hi jeff'),
(133, 0, 18, 1399331569, 'qwe'),
(134, 0, 18, 1399331663, 'asdasdasd'),
(135, 18, 0, 1399331702, 'burp'),
(136, 18, 0, 1399331814, 'asdasdasdassd'),
(137, 0, 18, 1399331832, 'wibble'),
(138, 0, 18, 1399331862, 'worble'),
(139, 0, 18, 1399331867, 'thats ebtterer'),
(140, 18, 0, 1399331887, 'great!'),
(141, 18, 0, 1399332038, 'does it still work hank?'),
(142, 0, 18, 1399332047, 'yup, loud and clear'),
(143, 0, 24, 1399332124, 'hi, this is jeff'),
(144, 24, 0, 1399332143, 'hi jeff?'),
(145, 0, 24, 1399332154, 'err hello.'),
(146, 24, 0, 1399332165, 'how can I help you jeff?'),
(147, 0, 18, 1399332170, 'I''m hungry'),
(148, 0, 24, 1399332182, 'want potatoes'),
(149, 0, 18, 1399332188, 'do you sell carrots'),
(150, 24, 0, 1399332198, 'We sell potatoes jeff'),
(151, 18, 0, 1399332207, 'Carrots have just come back in hank'),
(152, 0, 18, 1399332478, 'ok, I''m back'),
(153, 18, 0, 1399332487, 'that''s great hank'),
(154, 0, 18, 1399332493, 'isn''t it just'),
(155, 18, 0, 1399332498, 'yup sure is'),
(156, 0, 18, 1399332511, 'ok so we need to fill the window up '),
(157, 18, 0, 1399332516, 'yup that we do'),
(158, 0, 18, 1399332528, 'this could go on for a while more yet'),
(159, 18, 0, 1399332537, 'I dunno I reckon we;re nearly threre'),
(160, 0, 18, 1399332547, 'It works, refresh your page'),
(161, 18, 0, 1399332554, 'ok will do '),
(162, 18, 0, 1399332567, 'I''m back'),
(163, 18, 0, 1399332571, 'will have '),
(164, 18, 0, 1399332572, ' to '),
(165, 18, 0, 1399332573, 'send'),
(166, 18, 0, 1399332576, 'several'),
(167, 18, 0, 1399332579, 'messages'),
(168, 18, 0, 1399332581, 'in '),
(169, 18, 0, 1399332582, 'order'),
(170, 18, 0, 1399332583, 'to'),
(171, 18, 0, 1399332586, 'fill up'),
(172, 18, 0, 1399332592, 'to see that it''s workde'),
(173, 18, 0, 1399332602, 'oh kalookalay'),
(174, 0, 18, 1399332613, 'sweet sweet sweet'),
(175, 24, 0, 1399332639, 'you still there Jeff?'),
(176, 0, 24, 1399332647, 'yes, I''m still here'),
(177, 0, 24, 1399332652, 'hows it all going'),
(178, 0, 24, 1399332661, 'you got things sorted now'),
(179, 0, 24, 1399332672, 'not tired of having a three way conversation with yourself?'),
(180, 0, 24, 1399332689, 'not think that''s just a little ''wierd''?'),
(181, 0, 24, 1399332705, 'shucks, now I can''t sepel wrdos'),
(182, 0, 24, 1399332713, 'one more and I''ll see if it works'),
(183, 0, 24, 1399332717, 'and yes it does'),
(184, 0, 18, 1399332826, 'oh yeahs'),
(185, 0, 18, 1399332872, 'right way round?'),
(186, 0, 24, 1399332884, 'that looks more like it'),
(187, 24, 0, 1399332890, 'how about from here'),
(188, 0, 18, 1399332962, 'ok'),
(189, 18, 0, 1399332969, 'yes hank'),
(190, 24, 0, 1399332979, 'you jefff'),
(191, 0, 24, 1399332987, 'coolio'),
(192, 0, 18, 1399333095, 'So'),
(193, 18, 0, 1399333103, 'yes hank'),
(194, 0, 18, 1399339213, ''),
(195, 0, 18, 1399345040, 'Hello?'),
(196, 18, 0, 1399345046, 'Hi Hank'),
(197, 0, 18, 1399345063, 'Do you have recipes?'),
(198, 18, 0, 1399345069, 'Yes we do'),
(199, 18, 0, 1399345102, 'ok then'),
(200, 0, 18, 1399345109, 'cool'),
(201, 0, 18, 1399391697, 'imelda here'),
(202, 18, 0, 1399391706, 'Hi there imelda :-)'),
(203, 0, 18, 1399391719, 'hi there Daddy'),
(204, 18, 0, 1399391725, 'Well hello'),
(205, 0, 18, 1399391743, 'we think you\\re the best!!!!....ever!'),
(206, 18, 0, 1399391747, ':-D'),
(207, 0, 18, 1399391754, 'XXX'),
(208, 0, 18, 1399391762, 'It looks great'),
(209, 18, 0, 1399391767, '*blush*'),
(210, 0, 18, 1399391778, ' i am impressed, not that I should be'),
(211, 0, 18, 1399402926, 'ello'),
(212, 18, 0, 1399402934, 'HI Hank'),
(213, 0, 18, 1399854784, 'fdgdfg'),
(214, 18, 0, 1399854791, 'dshhf'),
(215, 24, 0, 1399854798, 'ghdgf');

-- --------------------------------------------------------

--
-- Table structure for table `contact_messages`
--

CREATE TABLE IF NOT EXISTS `contact_messages` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `email` varchar(256) COLLATE utf8_bin NOT NULL,
  `phone` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `message_text` text COLLATE utf8_bin NOT NULL,
  `received` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `replied` tinyint(1) NOT NULL,
  `replied_by` int(11) NOT NULL,
  `replied_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `contact_messages`
--

INSERT INTO `contact_messages` (`id`, `name`, `email`, `phone`, `message_text`, `received`, `replied`, `replied_by`, `replied_date`) VALUES
(0, 'nick', 'nick@thejubster.co.uk', NULL, '0', '2014-05-05 16:23:39', 0, 0, '0000-00-00 00:00:00'),
(0, 'hank', 'nick@thejubster.co.uk', NULL, 'Blah blah blah blah bkag\r\n  ', '2014-05-05 16:24:36', 0, 0, '0000-00-00 00:00:00'),
(0, 'hank', 'hank@thejubster.co.uk', NULL, 'Some message that is likely pretty boring.      \r\n  ', '2014-05-06 19:18:44', 0, 0, '0000-00-00 00:00:00'),
(0, 'hank', 'hank@thejubster.co.uk', NULL, '      \r\n  ', '2014-05-11 09:36:44', 0, 0, '0000-00-00 00:00:00'),
(0, 'hank', 'hank@thejubster.co.uk', NULL, '      \r\n  ', '2014-05-11 09:37:20', 0, 0, '0000-00-00 00:00:00'),
(0, 'manager', 'manager@thejubster.co.uk', NULL, 'dasdasd', '2014-05-11 09:38:37', 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `postcode` varchar(10) CHARACTER SET latin1 NOT NULL,
  `address` varchar(256) CHARACTER SET latin1 NOT NULL,
  `house` varchar(50) CHARACTER SET latin1 NOT NULL,
  `county` varchar(50) CHARACTER SET latin1 NOT NULL,
  `country` varchar(50) CHARACTER SET latin1 NOT NULL,
  `telephone` varchar(20) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=20 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `postcode`, `address`, `house`, `county`, `country`, `telephone`) VALUES
(3, 17, 'OO11OO', 'Rack 15\r\nLevel 2\r\nDatacentre', '42', '', 'uk', ''),
(4, 18, 'HA12NK', 'HanksSt\r\nHanksville\r\nHankshire', 'Flat B', '', 'uk', ''),
(5, 19, 'BA85PC', 'Babs Lane,\r\nBabston', 'Babs Palace', '', 'uk', ''),
(6, 20, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(7, 21, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(8, 22, 'CA5HBAG', 'Managers Mansion,\r\nRichville\r\nDoshtown', '1', '', 'uk', ''),
(9, 23, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(10, 24, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(11, 25, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(12, 26, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(13, 27, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(14, 28, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(15, 29, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(16, 30, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(17, 31, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(18, 32, 'CB9FN', 'Someplace,\r\nSometown', '23', '', 'uk', ''),
(19, 34, 'JM35Pc', 'James St,\r\nJamestown\r\nCounty James', '400', '', 'uk', '');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_actions`
--

CREATE TABLE IF NOT EXISTS `inventory_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `email_manager` tinyint(1) NOT NULL,
  `email_supplier` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`,`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=36 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `transaction_id`, `timestamp`, `total`) VALUES
(26, 18, 0, 1399108526, 10263),
(27, 18, 0, 1399798942, 6345),
(28, 22, 0, 1399804076, 495),
(29, 18, 0, 1399684104, 675),
(30, 18, 0, 1399724135, 940),
(31, 18, 0, 1399804178, 2437),
(32, 18, 0, 1399815995, 2447),
(33, 18, 0, 1399819628, 26201),
(34, 17, 0, 1399821108, 700),
(35, 18, 0, 1399822417, 6345);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=107 ;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `qty`, `price`, `discount_id`) VALUES
(57, 26, 1, 1, 110, 0),
(58, 26, 2, 4, 90, 0),
(59, 26, 3, 5, 560, 0),
(60, 26, 4, 3, 999, 0),
(61, 26, 5, 2, 1299, 0),
(62, 26, 11, 2, 699, 0),
(63, 27, 14, 1, 150, 0),
(64, 27, 16, 1, 250, 0),
(65, 27, 13, 13, 199, 0),
(66, 27, 9, 1, 699, 0),
(67, 27, 3, 1, 560, 0),
(68, 27, 6, 1, 160, 0),
(69, 27, 2, 1, 90, 0),
(70, 27, 8, 1, 440, 0),
(71, 27, 1, 1, 110, 0),
(72, 27, 5, 1, 1299, 0),
(73, 28, 17, 1, 45, 0),
(74, 29, 17, 5, 45, 0),
(75, 30, 17, 2, 45, 0),
(76, 30, 15, 1, 250, 0),
(77, 30, 14, 1, 150, 0),
(78, 31, 8, 1, 440, 0),
(79, 31, 9, 1, 699, 0),
(80, 31, 4, 1, 999, 0),
(81, 31, 10, 1, 299, 0),
(82, 32, 22, 2, 250, 0),
(83, 32, 21, 1, 199, 0),
(84, 32, 15, 1, 250, 0),
(85, 32, 12, 1, 199, 0),
(86, 32, 11, 1, 699, 0),
(87, 32, 8, 1, 440, 0),
(88, 32, 6, 1, 160, 0),
(89, 33, 1, 2, 110, 0),
(90, 33, 2, 9, 90, 0),
(91, 33, 3, 5, 560, 0),
(92, 33, 4, 3, 999, 0),
(93, 33, 5, 2, 1299, 0),
(94, 33, 11, 23, 699, 0),
(95, 33, 9, 1, 699, 0),
(96, 34, 22, 1, 250, 0),
(97, 35, 14, 1, 150, 0),
(98, 35, 16, 1, 250, 0),
(99, 35, 13, 13, 199, 0),
(100, 35, 9, 1, 699, 0),
(101, 35, 3, 1, 560, 0),
(102, 35, 6, 1, 160, 0),
(103, 35, 2, 1, 90, 0),
(104, 35, 8, 1, 440, 0),
(105, 35, 1, 1, 110, 0),
(106, 35, 5, 1, 1299, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barcode_value` varchar(30) COLLATE utf8_bin NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `brand` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `stock_qty` int(11) DEFAULT NULL,
  `reorder_qty` int(11) NOT NULL DEFAULT '0',
  `supplier` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=23 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `barcode_value`, `name`, `description`, `brand`, `category`, `price`, `stock_qty`, `reorder_qty`, `supplier`) VALUES
(1, '', 'Abe & Cloe Stilton', 'A deliciously smooth, creamy and tangy Stilton cheese. Prepared by hand in small batches on our farm.', 0, 4, 110, 10, 20, 1),
(2, '', 'Kiwi fruit', 'Sumptous kiwi fruits, grown in poly tunnels using modern, organic farming methods.', 0, 3, 90, 20, 30, 2),
(3, '', 'Mixed fruit salad', 'A prepared fruit salad showcasing the best of the current picks from our orchards.', 0, 3, 560, 5, 20, 3),
(4, '', 'Veg box', 'A selection of seasonal vegetables from the farm.', 0, 2, 999, 11, 10, 0),
(5, '', 'Italian veg basket', 'An Italian inspired medley of vegetables.', 0, 2, 1299, 22, 10, 0),
(6, '', 'Watermelon', 'Ok, so we don''t grow these ourselves, but we have found one of the best suppliers around with a carbon neutral supply chain.', 0, 3, 160, 15, 10, 4),
(7, '', 'Stir Fry Kit', 'All the ingredients for a taste of the Orient. Contains: 4 Chicken Breasts, Red Peppers Spring Onions, Pak Choi, Garlic, Rice Noodles and one of our recipe sheets.', 0, 2, 1500, 0, 10, 2),
(8, '', 'Fajita kit', 'All the ingredients for a Mexican inspired treat for all the family. Contains: Onions, Red Peppers, Tomatoes, Fresh Coriander, Garlic and one of our recipe sheets.', 0, 2, 440, 5, 10, 0),
(9, '', 'Seasonal fruit basket', 'A selection of seasonal fruit from the Abe & Cloe orchards.', 0, 3, 699, 7, 10, 0),
(10, '', 'Carrots', 'One of our mainstays here at Abe & Cloe. The humble carrot.', 0, 1, 299, 500, 200, 0),
(11, '', 'Organic free range chicken', 'Reared here on our farm, these chickens a grown in organic certified, free range environments and are fed on a healthy mixture of corn and vegetable scraps.', 0, 7, 699, 60, 20, 0),
(12, '', 'Onions', 'The onion. A store cupboard essential.', 0, 1, 199, 1000, 300, 0),
(13, '', 'Courgettes', 'A top pick from our neighbours farm. These courgettes are a great addition to a rattatouille ', 0, 1, 199, 0, 10, 1),
(14, '', 'Artichokes', 'These artichokes are grown here on our farm using certified organic soils and techniques.', 0, 1, 150, 20, 5, 0),
(15, '', 'Fresh basil', 'Fresh herbs add an extra special zing to your cooking.', 0, 6, 250, 15, 20, 0),
(16, '', 'Fresh coriander', 'Fresh herbs add an extra special zing to your cooking.', 0, 6, 250, 23, 20, 0),
(17, '', 'Apple', 'Direct from our orchards, our seasonal apple varieties are left on the trees for that little bit longer to ensure maximum taste.', 0, 3, 45, 300, 100, 0),
(18, '', 'Turkey Crown', 'Product description entered via admin interface not phpmyadmin', 0, 7, 899, 0, 10, 0),
(20, '', 'Potatoes', 'The humble spud, you guessed it - grown on our farm!', 0, 1, 5, 4000, 1000, 0),
(21, '', 'Fresh Parsley', 'Re-live the 80&#039;s cooking experience and sprinkle parsley into everything.', 0, 6, 199, 300, 50, 0),
(22, '', 'Chives', 'Fresh chives - great in a potato salad or coleslaw', 0, 6, 250, 60, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`) VALUES
(1, 'Vegetables'),
(2, 'Vegetable Boxes'),
(3, 'Fruit'),
(4, 'Dairy'),
(5, 'Prepared Boxes'),
(6, 'Herbs'),
(7, 'Meat');

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  `serves` int(11) NOT NULL,
  `prep_time` int(11) NOT NULL,
  `cook_time` int(11) NOT NULL,
  `method` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `serves`, `prep_time`, `cook_time`, `method`) VALUES
(1, 'Some Recipe', 4, 15, 30, ''),
(2, 'Chicken Fajitas', 4, 15, 20, ''),
(3, 'Crazy concoction', 4, 30, 45, 'Get ingredients\r\nMix in a bowl\r\nServe\r\nEat'),
(4, 'Chicken + Stilton Lasanga', 4, 45, 60, 'Fry Onions + Garlic\r\nAdd chicken strips and brown\r\nAdd tinned tomatoes\r\nAdd Vegetables\r\nMake cheese sauce\r\nCombine \r\nBake');

-- --------------------------------------------------------

--
-- Table structure for table `recipe_items`
--

CREATE TABLE IF NOT EXISTS `recipe_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipe_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty_required` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=26 ;

--
-- Dumping data for table `recipe_items`
--

INSERT INTO `recipe_items` (`id`, `recipe_id`, `product_id`, `qty_required`) VALUES
(1, 2, 8, 1),
(2, 2, 11, 1),
(5, 2, 16, 1),
(6, 1, 2, 3),
(7, 1, 4, 2),
(8, 1, 16, 1),
(9, 1, 14, 6),
(10, 1, 5, 4),
(11, 3, 15, 4),
(12, 3, 13, 5),
(13, 3, 11, 2),
(14, 3, 9, 1),
(15, 3, 7, 4),
(16, 3, 5, 1),
(17, 3, 3, 3),
(18, 3, 1, 2),
(19, 3, 1, 1),
(20, 3, 8, 2),
(21, 4, 1, 1),
(22, 4, 5, 1),
(23, 4, 11, 1),
(24, 4, 15, 1),
(25, 1, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `saved_baskets`
--

CREATE TABLE IF NOT EXISTS `saved_baskets` (
  `id` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `saved_baskets`
--

INSERT INTO `saved_baskets` (`id`, `user_id`, `name`) VALUES
('18x535c46797e1af6.13336200', 18, 'hanks small feast'),
('18x535d06f198d000.68097512', 18, 'Girls Night In'),
('18x535d0733685012.31655062', 18, 'lot&#039;s of food'),
('18x535d1174055b85.36892171', 18, 'FRUIT!'),
('18x535d934eea68f', 18, ''),
('18x536e5bcd12bbd', 18, 'Sooo much fooood'),
('18x536e6c3a8e46c', 18, ''),
('19x535d93580ed1c', 19, ''),
('19x535d941a9b5be', 19, ''),
('19x535d97d74c030', 19, ''),
('19x535da3fd7a583', 19, '');

-- --------------------------------------------------------

--
-- Table structure for table `saved_basket_items`
--

CREATE TABLE IF NOT EXISTS `saved_basket_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=74 ;

--
-- Dumping data for table `saved_basket_items`
--

INSERT INTO `saved_basket_items` (`id`, `list_id`, `product_id`, `qty`) VALUES
(19, '18x535c46797e1af6.13336200', 1, 1),
(20, '18x535c46797e1af6.13336200', 2, 4),
(21, '18x535c46797e1af6.13336200', 3, 5),
(22, '18x535c46797e1af6.13336200', 4, 3),
(23, '18x535c46797e1af6.13336200', 5, 2),
(24, '18x535c46797e1af6.13336200', 11, 2),
(32, '18x535d06f198d000.68097512', 1, 2),
(33, '18x535d06f198d000.68097512', 2, 9),
(34, '18x535d06f198d000.68097512', 3, 5),
(35, '18x535d06f198d000.68097512', 4, 3),
(36, '18x535d06f198d000.68097512', 5, 2),
(37, '18x535d06f198d000.68097512', 11, 23),
(38, '18x535d06f198d000.68097512', 9, 1),
(39, '18x535d0733685012.31655062', 14, 1),
(40, '18x535d0733685012.31655062', 16, 1),
(41, '18x535d0733685012.31655062', 13, 13),
(42, '18x535d0733685012.31655062', 9, 1),
(43, '18x535d0733685012.31655062', 3, 1),
(44, '18x535d0733685012.31655062', 6, 1),
(45, '18x535d0733685012.31655062', 2, 1),
(46, '18x535d0733685012.31655062', 8, 1),
(47, '18x535d0733685012.31655062', 1, 1),
(48, '18x535d0733685012.31655062', 5, 1),
(49, '18x535d1174055b85.36892171', 2, 1),
(50, '18x535d1174055b85.36892171', 3, 1),
(51, '18x535d1174055b85.36892171', 6, 2),
(52, '18x535d934eea68f', 3, 3),
(53, '19x535d93580ed1c', 1, 5),
(54, '19x535d93580ed1c', 6, 3),
(55, '19x535d93580ed1c', 7, 4),
(56, '19x535d941a9b5be', 7, 8),
(57, '19x535d941a9b5be', 10, 3),
(58, '19x535d97d74c030', 1, 8),
(59, '19x535d97d74c030', 6, 4),
(60, '19x535d97d74c030', 11, 5),
(61, '19x535da3fd7a583', 1, 10),
(62, '19x535da3fd7a583', 2, 16),
(63, '18x536e5bcd12bbd', 1, 10),
(64, '18x536e5bcd12bbd', 2, 12),
(65, '18x536e5bcd12bbd', 3, 5),
(66, '18x536e5bcd12bbd', 4, 11),
(67, '18x536e5bcd12bbd', 5, 12),
(68, '18x536e5bcd12bbd', 11, 12),
(69, '18x536e6c3a8e46c', 1, 2),
(70, '18x536e6c3a8e46c', 2, 4),
(71, '18x536e6c3a8e46c', 3, 5),
(72, '18x536e6c3a8e46c', 4, 5),
(73, '18x536e6c3a8e46c', 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `email` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` tinytext COLLATE utf8_bin NOT NULL,
  `password` varchar(5012) COLLATE utf8_bin NOT NULL,
  `email` varchar(400) COLLATE utf8_bin NOT NULL,
  `last_authenticated` int(11) NOT NULL,
  `last_active` int(11) NOT NULL,
  `group` int(11) NOT NULL,
  `joined` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=35 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `last_authenticated`, `last_active`, `group`, `joined`) VALUES
(17, 'admin', '$2y$10$YmmgLqqnxpom/VNevfdt8OcovbvX1gNIjvJPqPiP/9xVkWX9K.aby', 'nick@thejubster.co.uk', 1398353026, 1399824905, 5, 0),
(18, 'hank', '$2y$10$szZNEMaD9Ob4i9nWSflDR.Jgr/TmFjs0vQ6bfPh3MA0TpbjlaymIu', 'hank@thejubster.co.uk', 1398372320, 1399859454, 0, 0),
(19, 'babs', '$2y$10$6EJ5ksluYhgmNHoWzXOo2uU8/7Fb3Xsqsw/nvkFI7dSgF.qulvw.2', 'babs2@thejubster.co.uk', 1399819079, 1399819302, 2, 0),
(20, 'franny frandsen', '$2y$10$q51VtBWL0Pbff4b.q44y3.tnM0p/.XrBkpo3CUHndZm7q8dEEE1aO', 'pernillehbfrandsen@gmail.com', 1398505949, 0, 0, 0),
(21, 'jock', '$2y$10$/12wjngRCpJaSfRdgiDz1evdatgYYNS2/3UU0e0.LC6bxIzrGxEju', 'jock@thejubster.co.uk', 1398556588, 0, 3, 0),
(22, 'manager', '$2y$10$QcTzHjpuC/8quIVivCHzE.ST6/qrJ.EzCs2n9W5HX3GdV2RNCexq2', 'manager@thejubster.co.uk', 1398787335, 1399855734, 4, 0),
(23, 'orderpicker', '$2y$10$s034.oBXJZOmwymwycyEl.z4Rxp5UDI3u7Q5lp6hcgilnq9qkTDIi', 'orderpicker1@thejubster.co.uk', 0, 1399422866, 1, 0),
(24, 'jeff', '$2y$10$O3VhhkNkcxha5ZumNbYTcu.luIyvMABCQFfp23U93Y.AAjt/jqwza', 'jeff@thejubster.co.uk', 1399251602, 1399333161, 0, 0),
(25, 'billy', '$2y$10$Nj8rMNqhdWWllQDT2htsDOBHdy1hUmEz.I5263POYoimBKpQ66d5a', 'billy@thejubster.co.uk', 0, 0, 0, 0),
(26, 'orderpicker2', '', 'orderpicker2@thejubster.co.uk', 0, 0, 0, 0),
(30, 'scooby', '@@NOTUSED', 'scoobydoo@thejubster.co.uk', 0, 0, 4, 0),
(31, 'scrappy', '@@NOTUSED', 'scrappydoo@thejubster.co.uk', 0, 0, 1, 0),
(32, 'someone', '@@NOTUSED', 'someone@thejubster.co.uk', 0, 0, 0, 0),
(34, 'james', '$2y$10$U6i8o3xYZSgHWc9NIDyzxumtji83U5vhMD0iBL4fUGb0ZyYvdxRiu', 'james@thejubster.co.uk', 1399811322, 1399814559, 4, 1399808712);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`) VALUES
(0, 'Customer'),
(1, 'Order Picker'),
(2, 'Administrator'),
(3, 'Accounts'),
(4, 'Manager'),
(5, 'Site Admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_members`
--

CREATE TABLE IF NOT EXISTS `user_group_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user_group_members`
--

INSERT INTO `user_group_members` (`id`, `group_id`, `user_id`, `added_on`) VALUES
(1, 1, 17, '2014-04-28 01:44:03'),
(2, 6, 18, '2014-04-28 01:44:36'),
(4, 6, 19, '2014-04-28 01:45:16'),
(5, 6, 20, '2014-04-28 01:45:16'),
(6, 6, 21, '2014-04-28 01:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

CREATE TABLE IF NOT EXISTS `verifications` (
  `hash` varchar(32) COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `verifications`
--

INSERT INTO `verifications` (`hash`, `created`, `user`) VALUES
('142f6cdf7b8e5ff50ce7544d42aaa57a', '2014-05-06 23:59:36', 23),
('a54c56510d7716c5c3bdb295adbf19b5', '2014-05-05 01:03:41', 25);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
