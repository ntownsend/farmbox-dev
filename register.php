<?php

  /** Register.php
   *
   * Performs the 'logic' of user registration and prepares the relevant info for the 
   * register.html template.
   *
   */



  /*
   * If User Registration is disabled, then don't produce a registration form
   */
  
  if($conf['users']['can_register']==false){
    $page = 'default';
  }
  else{

    /* If we aren't already using an encrypted session then
     * make sure that we are.
     * @TODO The make_connection_secure function just redirects -
     * @TODO if there is no https version available the user recieves a browser error and no site controlled info
     */
    
    make_connection_secure();

   
    if(!empty($_POST)){
      extract($_POST);
      
      $username = sanitise($username);
      $password = sanitise($password);
      $email = sanitise($email);
        
      
      /* If $username contains a value then check to make sure it isn't in use
       * 
       */
       
      if(!empty($username)){
        if(!ctype_alnum){

        }
        
        if($user->Exists($username)==false){

          // Store the accepted username in the $register array
          $register['username'] = $username;
        }
        else
        {
          /* @TODO: change this feature to return a message (from lang) and the value for the form
           */
          $errors['username']=$username;
        }
  
      }
      else{
  
        $errors['username']=$username;
        $errors['user']['value'] = $username;

        /* Valid reasons
         * TODO move this and make it a function
         *
         * 0 - Required value empty
         * 1 - Required value wrong chars
         * 2 - Email invalid
         */
        $errors['user']['reason'] = 0;
      }
    
    
      // Check passwords match
      // TODO - PasswordCheck function
      
      if(
          ("$password"!=="$password2")
          ||
          (strlen($password)<($conf['auth']['password_policy']['min_characters']))
          ||
          (strlen($password)>($conf['auth']['password_policy']['max_characters']))
        ){
        
        $errors['password'] = true;
  
      }
      else{
        $register['password'] = $password;
      }
    
  
      
      // Validate email
      // TODO - are these all the required validation steps
      // TODO make this a function we can use again dur brain!

      $email = filter_var("$email",FILTER_SANITIZE_EMAIL);

      if(filter_var($email, FILTER_VALIDATE_EMAIL)){


        if($user->emailExists($email)==false){

          $register['email'] = $email;
        }else
        {
      
          $errors['email'] = 'EXISTS';
        }
        


      }
      else{
        
        $errors['email'] = "$email";
      }
    
    
      // If we have errors then we include the register form again
      // If registration has been successful then we notify the user
      
    
      if(isset($errors)){
        $page='register';
      
      }
      else{

        // create the users password hash
        $password = UserSession::createPassword($password);
        // commit the new user to the database

        $user->createNewUser($username,$password,$email);

       // $page='register_confirm';
        $page='products';
        $confirm_message = $lang['register']['email_sent'] = str_replace('##EMAIL##',$_SESSION['did_register_email'],$lang['register']['email_sent']);
        $page='products';
        display_feedback("$confirm_message","Registration Successful",'products',notify);
//        page_redirect('register_confirm');

      }
      
    
    }
      

  $username_input_class = '';
  $password_input_class ='';
  $email_input_class ='';

  
  // Set the guidance messages based on detedcted errors
  
    $symbols = array(

      'info' => "<i class='fa fa-info-circle'></i>",
      'warning' => "<i class='fa fa-warning'></i>",
      'ok' => "<i class='fa fa-check-o'></i>"

    );
  
  
    if(isset($errors['username'])){

      $username_message = $symbols['warning'].$lang['register']['username_error'] ;
      $username_input_class = 'input_error';
      $username_value = $errors['username'];

    }
    elseif(isset($register['username'])){

      $username_message = $symbols['ok'].$lang['register']['value_accepted'] ;
      $username_input_class = 'accepted_value';
      $username_value = $register['username'];

    }
    else{

      $username_message = $symbols['info'].$lang['register']['username_guidance'];
      $username_value = '';

    }
    
  
  
    if(isset($errors['password'])){

      $password_message = $symbols['warning'].$lang['register']['password_error'] ;
      $password_input_class = 'input_error';
      $password_value = '';

    }
    elseif(isset($register['password'])){

      $password_message = $symbols['ok'].$lang['register']['value_accepted'] ;
      $password_input_class = 'accepted_value';
      $password_value = $register['password'];

    }
    else{

      $password_message =  $symbols['info'].$lang['register']['password_guidance'];
      $password_message_confirm =  $symbols['info'].$lang['register']['password_confirm'];
      $password_value = '';

    }
    
  
  
    if(isset($errors['email'])){
      
      $email_message = $symbols['warning'].$lang['register']['email_error'] ;
      $email_input_class = 'input_error';
      $email_value = $errors['email'];

      if($errors['email'] == "EXISTS")
        {
          $email_value = '';
      $email_message = $lang['register']['email_error_exists'];   
        }
    }
    elseif(isset($register['email'])){
      $email_message = $symbols['ok'].$lang['register']['value_accepted'] ;
      $email_input_class = 'accepted_value';
      $email_value = $register['email'];
    }
    else{
      $email_message =  $symbols['info'].$lang['register']['email_guidance'];
      $email_value = '';
    }
  }
