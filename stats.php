<?php
/** Used during development only
 */

function getFileCount($path) {
    $size = 0;
    $lines = 0;
    $ignore = array('.','..','third-party','dev','doc','.DS_Store','.git');
    $files = scandir($path);
    foreach($files as $t) {
        if(in_array($t, $ignore)) continue;
        if (is_dir(rtrim($path, '/') . '/' . $t)) {
            $size += getFileCount(rtrim($path, '/') . '/' . $t);
            
        } else {
            $size++;
            $lines += `exec wc -l $path 2> /dev/null`;
        }   
    }
    echo $lines ." lines of code";
    echo "In ". $size . "files";
}



//Stats
// Number of files
getFileCount('./');
// Code retrieved from
// http://stackoverflow.com/a/640944

