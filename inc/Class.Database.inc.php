<?php

// http://code.tutsplus.com/tutorials/php-database-access-are-you-doing-it-correctly--net-25338

/**
*  Create a persistent database connection object 
*
*/

  function db_connect()
  {
    global $conf;
    global $lang;

    extract($conf['db']);
  
 
      $db_connection = new PDO("mysql:host=$host; dbname=$database;charset=utf8", "$username", "$password");

      /* We set the error mode for exceptions to report, even in production.
       *
       * All exceptions for the application are handled by the custom exception handler defined in Class.UI.inc.php
       * to allow for enabling disabling of this via the sites configuration, a kind of 'developer mode'
       * this also allows error messages that make sense to the end user to be displayed without revealing any
       * runtime information that we would rather not be outputting the world.
       */
      
      $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

     //$mysql_ver = $db_connection->getAttribute(constant("PDO::ATTR_SERVER_VERSION")) . "\n";

  return $db_connection;
  
  }


