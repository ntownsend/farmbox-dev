<?php
  
  
  // If the script is called directly then redirect.
  // We have to use the header function as our page_direct method is not
  // loaded at this point.
    
  if(strtoupper(basename($_SERVER['SCRIPT_NAME'])) != "INDEX.PHP")
      {
      header("Location:./?p=account");
      }
  
  
  // If we aren't logged in then we can't view this page.
  
  if(!isset($_SESSION['uid'])){
  
    page_redirect('login');
  //$page= 'login';
    }
  
  
  // If passed values then prcoess them
  
  if(!empty($_POST['a'])){

    // Process a submitted email address
  
    if($_POST['a'] =='email'){
      
          $email = filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
          
          $user->changeEmail($_SESSION['uid'],$email);

    }

    // Process an address update request
    
    elseif($_POST['a'] == 'address'){

        $elements = array('house','address','country','postcode');

        // Ensure each element is filled in. THere's not much checking we can do and
        // we have to assume that the user will be wanting to give us accurate information
        // as we are going to be shipping goods that they have paid for to this address

        foreach($elements as $element){

          if(empty($_POST["$element"])){

            $error["$element"] = 'Empty Value';
          }
          else{

          // Sanitise the data before saving it. Note comments above re checking the data.
          // A lookup for postcodes with the Royal Mail and other services could
          // be implemented here but these carry a cost

          $$element = sanitise($_POST["$element"]);

          }
      

        }

        // If no errors have been set then we can proceed to processing the data received.
          

        if(empty($errors)){

          $user->changeAddress($_SESSION['uid'],$house,$address,$country,$postcode);
        }
        else{

          display_feedback('Address information was not accepted. Please check your information and try again','Address not accepted','account');

        }
        
          
    }
  
  
  
  
  }
  
