<?php

// Set the user level for this page.

$user->authorise(1);


 if(isset($_POST['a'])){

    if($_POST['a']=='picked'){

        $orders->updateStatus($_POST['order_id']);
        
    }
  }
  // Get each order where transaction is completed

  $packable_orders = $orders->getPackableOrders();
  
  //Count Orders for the 'mini report' total at the top of the page

  $pending_orders = count($orders->getPackableOrders());
  
  // Iterate over the orders and create a form for each one

  $html = '';
  
  foreach($packable_orders as $order_id => $order_detail){

    // format the time into something pretty

    $order_placed = date('H:i:s d/m/y', $order_detail['timestamp']);

    $html .= "<div class='butt_after float_left'>
              <h2><i class='fa fa-cubes'></i> Order ". $order_detail['id'] ."</h2>
              <h6>$order_placed</h6>
              <form action='./?p=pack' method='post' name='order_".$order_detail['id']."'>
              <input type='hidden' name='order_id' value ='".$order_detail['id']."'/>
              <table>
              <tr><th>Product Code</th><th>Product Detail</th><th>qty</th><th>Picked</th></tr>
    ";

    // Get customer info for label

    $deliver_to = $user->info($order_detail['customer_id']);

    $delivery_info  = $deliver_to['username'] .'<br>';
    $delivery_info .= $deliver_to['address_details']['house'] .'<br>';
    $delivery_info .= $deliver_to['address_details']['address'] .'<br>';
    $delivery_info .= $deliver_to['address_details']['postcode'] .'<br>';
    $delivery_info .= $deliver_to['address_details']['country'] .'<br>';

    $order_items = $orders->getProducts($order_detail['id']);

      // List the order items

      // TODO - The checkboxes could trigger the enabling of the button using jquery
      // The checkboxes currently serve purely as an aide memoir for the orderpicker

      foreach($order_items as $id => $order_item){
        $product_name = $product->lookup($order_item['product_id'])['name'];

          $html .= "<tr>
                  <td>". $order_item['product_id'] ."</td>
                  <td> $product_name </td>
                  <td>". $order_item['qty'] ."</td>
                  <td><input type='checkbox' name='".$order_item['id']."'></td>

          </tr>";
  

      }
    

    $html .= "<tr><td colspan='4' class='bordered'>$delivery_info</td></tr>";
    $html .= "<tr><td colspan='4'> <button id='packed_button' name='a' value='picked' ><i class='fa fa-cubes'></i> Order Packed</button></td></tr>";
    $html .= "</table></form></div>";

  }
