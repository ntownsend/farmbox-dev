<?php

// Set the user level for this page.

$user->authorise(4);

// By default we don't show the add new user form
// but we do show the add new user button that takes us there

$showform = false;
$show_button = true;

  // TODO - This user creation mechanism is different to the one used in register.php so is written from scratch again here.
  // TODO - Amalgamate the processes into a method statement to reduce the complexity of managing these two separate elements

  if(isset($_POST['a'])){

      if($_POST['a']=='update_user_role'){

        // Update a users role and go back to the user record

        $user->updateRole(sanitise($_POST['user']),sanitise($_POST['group']));

        page_redirect('users');


      }


    }

  /*
   *  Handle events sent to the page by the new user form
   */
  
  if(isset($_POST['username'])){

  // Check username doesn't exist already
  
    if($user->Exists(sanitise($_POST['username']))){

      $error['username']['err'] = 'Username exists';

    }else{

      $username = sanitise($_POST['username']);
    
    }

  // CHeck email for validity with builtin filter_vars
  
    $email = filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
    if(filter_var($email,FILTER_VALIDATE_EMAIL)){

      // check that the now validated email isn't already in use
  
      if($user->emailExists($email)){

        $error['email']['err'] = 'Email in use';
  
      }
    
    }
    else{

      // Feedback for user on error

      $error['email']['err'] = 'There was a problem with that email address';

    }

    // Check that the group value is a number

    $group = (is_numeric($_POST['group']) ? $_POST['group'] : 0);

    // If $error is empty then no error conditions above have occured.
    // Therefore safe to commit the new user to the database

    if(empty($error)){

      // Add user, sending a link to get them to activate and create a password using the account reset feature

      $user->createNewUser($username,'@@NOTUSED',$email,false,true,$group);
      
    }
  
  }


/* Either display an individua user account record or list all user accounts
 */

  if((isset($_POST['a']))&&($_POST['a']=='add_new')){

    $show_form = true;
    $show_button = false;
    $page_title = 'Add new user';

    // Build the option elements to choose the group from

    $group_options = '';

    foreach($user->getGroups() as $key => $value){
     
        $group_options .= "<option value='".$value['id']."'>". $value['group_name'] ."</option>";

    }

  


  }elseif( (isset($_POST['view'])) && ($_POST['view']=='user') ) {

    /* Display a single user record
     *
     * This includes the order history view, presented exactly as the customer sees it to assist staff
     * help customers with order enquiries.
     * 
     * 
     */
     
    $page_title = 'User Record';
    $user_details = $user->info($_POST['user_id']);
    extract($user_details);
    $account_no = $user->makeAccountNo($id);
    $order_history = $orders->getOrderHistory($id);
    $message_history = $message->chatHistory($id);

    $show_button = false;

    // Begin building the html output for the users message history
    $message_history_formatted = "<table>";

    // Each message is displayed across two table rows with a header in the first row.
    foreach ($message_history as $key => $message_data){
        
        $message_history_formatted .= "<tr><th>sent</th> <td>". date('d/m/y H:i:s',$message_data['timestamp']) ."</td>";
        $message_history_formatted .= "<th>msg_id</th><td>". $message_data['id']."</td></tr>";
        $message_history_formatted .= "<tr><td colspan=4>". $message_data['message'] . "</td></tr>";
      }
      
    $order_history_formatted = $orders->orderHistoryTable($order_history);

    $user_lookup = $id;
      
    $message_history_formatted .= '</table>';

    
    // Create some options for the form
     
    $address_details = $userinfo['address_details'];
     
    $acc_num = str_pad($_SESSION['uid'], 9, "0", STR_PAD_LEFT);
      
    $group_options = '';
      
    foreach($user->getGroups() as $key => $value){
    
      
      $selected = ($value['id']==$user_details['group'] ? 'selected' : '');
    
  
      
      $group_options .= "<option value='".$value['id']."' $selected>". $value['group_name'] ."</option>";
  
    }
    
      
    $tdata .= "
              <form action='./?p=users' method='get'>
              <div class='field'>
                <button type='submit' name='p' value='users'><i class='fa fa-users'></i> Back to user list</button>
              </div>
              </form>
              <div class='page_top'>
              User order history is displayed exactly as the customer sees it.<br><br>
              Combined order details are available from the statistics  and order picker views.

              </div>
                <form action='./?p=users' method='post'>
                <fieldset>
                <div class='field'>
                  <label for='id'>Username:</label>
                  <input name='username' type='text' disabled value='$username' />
                </div>
                <input type='hidden' name='user' value='".$user_details['id']."' />
                <div class='field'>
                  <label for='group'>Account Type</label>
                    <select name='group'>
                      $group_options
                    </select>
                   <button type='submit' value='update_user_role' name='a'><i class='fa fa-refresh'></i> Update</button>
                </div>

                <div class='field'>
                  <label for='id'>User id:</label>
                  <input name='id' type='text' disabled value='$id' />
                </div>
                <div class='field'>
                  <label for='account_no'>Account number</label>
                  <input name='account_no' type='text' disabled value='$account_no' />
                </div>
                <div class='field'>
                  <label for='orders'>Orders</label>
                  <input name='orders' type='text' disabled value='". count($order_history) ."' />
                </div>
                <div class='field'>
                  <label for='order_history'>Order History</label>
                  <div class='account_history' name='order_history' >$order_history_formatted
                  </div>
                </div>
                ";

    // Because of the way live chat works for staff users
    //(as in they all send from the same account and all receive messages)
    // we don't display the chat history for those users.
    // A quick check for group 0 (customer) provides salvation

    if ($user_details['group'] ==0) {
      
      $tdata .= "<div class='field'>
                  <label for='messages'>Chat messages</label>
                  <input name='messages' type='text' disabled value='". count($message_history) ."' />
                </div>
                <div class='field'>
                  <label for='message_history'>Chat History</label>
                  <div class='account_history' name='message_history' >$message_history_formatted
                  </div>
                </div>";

    }


    $tdata .="  </fieldset>
                </form>
                ";


  }
  else{

  /* Display all users, tabulated with form elements to
   * allow a specific user record to be chosen with the view
   * created above.
   */

  
    $page_title = 'User Management';
  
    
    // TODO - color code user roles in output
     
  
    /* these are the sort colums we will accept
     * this is important as we are building these int the prepared pdo statement in User.inc
     * as PDO doesnt play nicely when you try and bind params on the order by condition
     */
    $order_by = array('id','username','email','group','last_authenticated','last_active');
  
  
  
    // include the view_choices bar.. this sets the values for the search based on user pagination choices
    
    include"./view_choices.php";
  
  
    //$tdata = $view_choices;

    $tdata .= "
              <table class='oddeven '>
                <tr>
                  <th>
                    Acc. No <a class='yellow' href='./?p=users&amp;sort=id&amp;dir=".$alt_dir['id']."'> <i class='fa fa-sort'></i></a>
                  </th>
                  <th>
                    Username<a href='./?p=users&amp;sort=username&amp;dir=".$alt_dir['username']."'><i class='fa fa-sort'></i></a>
                  </th>
                  <th>
                    Email <a href='./?p=users&amp;sort=email&amp;dir=".$alt_dir['email']."'><i class='fa fa-sort'></i></a>
                  </th>
                  <th>
                    User Role <a href='./?p=users&amp;sort=group&amp;dir=".$alt_dir['group']."'><i class='fa fa-sort'></i></a>
                  </th>
                  <th>
                    Last Logged In <a href='./?p=users&amp;sort=last_authenticated&amp;dir=".$alt_dir['last_authenticated']."'><i class='fa fa-sort'></i></a>
                  </th>
                  <th>
                    Last Active <a href='./?p=users&amp;sort=last_active&amp;dir=".$alt_dir['last_active']."'><i class='fa fa-sort'></i></a>
                  </th>
                  <th>
                    Saved Baskets 
                  </th>
                  <th>
                  Actions
                  </th>
                </tr>
              ";
    
    // Get all of the users from the database including the info for the pagination created in view_choices
   
    $userlist = $user->listAll($sort_by,$sort_direction,$start_row,$row_limit,$selected_category);
      
    foreach($userlist as $key => $value){
     
      $actions='';
    
      $actions= "
                <form action='./?p=users' method='post'>
                <input type='hidden' name='user_id' value='".$value['id']."'/>
                <button type='submit' name='view' value='user' class='small'><i class='fa fa-eye'></i> View</button>
                </form>

                ";


      $tdata .= "<tr>
                  <td>".
                    str_pad( $value['id'],9,0,STR_PAD_LEFT)
                ."</td>
                  <td>".
                    $value['username']
                ."</td>
                  <td>".
                    $value['email']
                ."</td>
                  <td>".
                    $value['group_name']
                ." [".
                    $value['group']
                ."]</td>
                   <td>".
                    date('d/m/Y H:i:s',$value['last_authenticated'])
                ."</td>
                  <td>".
                    date('d/m/Y H:i:s',$value['last_active'])
                ."</td>
                  <td>".
                    $shopping_basket->numSaved($value['id'])
                ."</td>
                  <td>".
                      $actions
                ."</td>
                  </tr>
                 ";
  
  
    }

    $tdata .= '</table>';
    
  }
